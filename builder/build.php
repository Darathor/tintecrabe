<?php
$colors = [
	'black' => ['black', '#fff'],
	'blue' => ['#2222ee', '#fff'],
	'green' => ['green', '#fff'],
	'red' => ['#dc143c', '#000'],
	'grey' => ['grey', '#fff'],
	'violet' => ['#9932cc', '#fff'],
	'orange' => ['#ff8c00', '#000'],
	'pink' => ['#ffc0cb', '#000'],
	'yellow' => ['yellow', '#000'],
	'white' => ['white', '#000']
];

$templates = [
	// Pawns.
	'follower',
	'phantom',
	'mayor',
	'mayor',
	'builder',
	'butcher',
	'pig',
	'waggon',
	'super',
	'messenger',
	'dwarf',
	'phantom-dwarf',
	'nomad',
	'phantom-nomad',
	'slave',
	// Tokens.
	'completed',
	'won'
];

foreach ($templates as $template)
{
	$content = file_get_contents('templates' . DIRECTORY_SEPARATOR . $template . '.svg');
	$dir = 'generated' . DIRECTORY_SEPARATOR . $template;
	if (!is_dir($dir) && !mkdir($dir) && !is_dir($dir))
	{
		throw new RuntimeException(sprintf('Directory "%s" was not created', $dir));
	}
	foreach ($colors as $key => $color)
	{
		file_put_contents($dir . DIRECTORY_SEPARATOR . $key . '.svg', str_replace(['{{COLOR_PRIMARY}}', '{{COLOR_SECONDARY}}'], $color, $content));
	}
}