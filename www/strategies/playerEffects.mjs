/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { PlayerEffect } from "../structures/PlayerEffect.mjs";
import { ArrayUtils, I18n, TimelineItemTrigger } from '../services/core.mjs';
import { Strategies, Structure, StructureEvaluator } from '../services/strategies.mjs';
import { Board } from "../services/board.mjs";

class PlayerEffectStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_player_effect_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_player_effect_' + this.code + '_description');
	}

	/**
	 * @param {Object} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		return new PlayerEffect(0, false, [], []);
	}

	/**
	 * @param {Object} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, player, game, source) {
		this.estimate(configuration, player, game, source).apply(player, game, source);
	}
}

/**
 * Default strategy: no effect.
 */
class NullPlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.playerEffects.null = new NullPlayerEffectStrategy();

/**
 * Extra turn strategy: give an extra turn to player if it is the current one.
 */
class ExtraTurnPlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'extraTurn';
	}

	/**
	 * @param {Object} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		return new PlayerEffect(0, game.currentPlayer === player, [], []);
	}
}

Strategies.playerEffects.extraTurn = new ExtraTurnPlayerEffectStrategy();

/**
 * @typedef {Object} GainPointsPlayerEffectStrategyConfiguration
 * @property {number} value
 */

/**
 * Gain points strategy: give some points to the player.
 */
class GainPointsPlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'gainPoints';
	}

	/**
	 * @param {GainPointsPlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		return new PlayerEffect(configuration.value, false, [], []);
	}
}

Strategies.playerEffects.gainPoints = new GainPointsPlayerEffectStrategy();

/**
 * @typedef {Object} GainTokensPlayerEffectStrategyConfiguration
 * @property {string[]} types
 * @property {number} quantity
 */

/**
 * Gain token strategy: give some random tokens among the given types to the player.
 */
class GainTokensPlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'gainTokens';
	}

	/**
	 * @param {GainTokensPlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let tokenCodes = game.tokensPool.getRandomTokens(configuration.quantity, configuration.types);
		return new PlayerEffect(0, false, tokenCodes, []);
	}
}

Strategies.playerEffects.gainTokens = new GainTokensPlayerEffectStrategy();

/**
 * @typedef {Object} ScoreSmallestStructurePlayerEffectStrategyConfiguration
 * @property {string} type
 * @property {string} mode
 */

/**
 * Trigger a scoring on the smallest structure strategy: give points for the smallest controlled structure of a given type as if it was the end of the game.
 */
class ScoreSmallestStructurePlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'scoreSmallestStructure';
	}

	/**
	 * @param {ScoreSmallestStructurePlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let testedFeatures = [];
		let selected = {
			box: null,
			feature: null,
			score: 0
		};

		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (ArrayUtils.contains(testedFeatures, feature) || feature.code !== configuration.type || !feature.containsPlayerPawn(player)) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(testedFeatures, structure.features);

				let evaluator = new StructureEvaluator(structure, game);
				if (ArrayUtils.contains(evaluator.majors, player)) {
					let score = evaluator.evaluateEnd(player);
					if (score && (!selected.score || score < selected.score)) {
						selected.box = box;
						selected.feature = feature;
						selected.score = score;
					}
				}
			});
		});

		if (selected.box) {
			source.trigger = new TimelineItemTrigger(
				'c.main.source_feature_prefixed', selected.box.coordinates, null, { feature: selected.feature.name }
			);
		}

		return new PlayerEffect(selected.score, false, [], []);
	}
}

Strategies.playerEffects.scoreSmallestStructure = new ScoreSmallestStructurePlayerEffectStrategy();

/**
 * @typedef {Object} FollowersInFeatureTypePlayerEffectStrategyConfiguration
 * @property {string} featureType
 * @property {number} value
 */

/**
 * Trigger followers in feature type: gain points for each follower in features of a given type.
 */
class FollowersInFeatureTypePlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'followersInFeatureType';
	}

	/**
	 * @param {FollowersInFeatureTypePlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let count = 0;

		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (feature.model.code !== configuration.featureType) {
					return;
				}

				feature.pawns.forEach((pawn) => {
					if (pawn.owner === player && pawn.model.type === 'follower') {
						count++;
					}
				});
			});
		});

		return new PlayerEffect(count * configuration.value, false, [], []);
	}
}

Strategies.playerEffects.followersInFeatureType = new FollowersInFeatureTypePlayerEffectStrategy();

/**
 * @typedef {Object} AdjacentToOccupiedFeatureEffectStrategyConfiguration
 * @property {string} occupiedType
 * @property {string} adjacentType
 * @property {number} value
 */

/**
 * Trigger adjacent to occupied feature: gain points for each feature of a given type adjacent to an occupied feature of a second given type.
 */
class AdjacentToOccupiedFeaturePlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'adjacentToOccupiedFeature';
	}

	/**
	 * @param {AdjacentToOccupiedFeatureEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let testedFeatures = [];
		let count = 0;

		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (ArrayUtils.contains(testedFeatures, feature) || feature.code !== configuration.adjacentType) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(testedFeatures, structure.features);

				let found = false;
				structure.getNeighbourStructuresByTypes([configuration.occupiedType]).forEach((neighbour) => {
					let evaluator = new StructureEvaluator(neighbour, game);
					if (evaluator.isOccupiedByPlayer(player)) {
						found = true;
					}
				});

				if (found) {
					count++;
				}
			});
		});

		return new PlayerEffect(count * configuration.value, false, [], []);
	}
}

Strategies.playerEffects.adjacentToOccupiedFeature = new AdjacentToOccupiedFeaturePlayerEffectStrategy();

/**
 * @typedef {Object} DifferentResourceTokensPlayerEffectStrategyConfiguration
 * @property {number} value
 */

/**
 * Trigger different resource tokens: gain points for each different types of resource tokens owned by the player.
 */
class DifferentResourceTokensPlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'differentResourceTokens';
	}

	/**
	 * @param {DifferentResourceTokensPlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let count = 0;

		let playerTokens = player.getTokens();
		Object.keys(playerTokens).forEach((code) => {
			if (playerTokens[code] > 0) {
				count++;
			}
		});

		return new PlayerEffect(count * configuration.value, false, [], []);
	}
}

Strategies.playerEffects.differentResourceTokens = new DifferentResourceTokensPlayerEffectStrategy();

/**
 * @typedef {Object} MaxOwnedResourceTypePlayerEffectStrategyConfiguration
 * @property {number} value
 */

/**
 * Trigger max owned resource type: gain points for each token for each token of the type that the player has the most of.
 */
class MaxOwnedResourceTypePlayerEffectStrategy extends PlayerEffectStrategy {
	constructor() {
		super();
		this.code = 'maxOwnedResourceType';
	}

	/**
	 * @param {MaxOwnedResourceTypePlayerEffectStrategyConfiguration} configuration
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	estimate(configuration, player, game, source) {
		let max = 0;

		let playerTokens = player.getTokens();
		Object.keys(playerTokens).forEach((code) => {
			max = Math.max(max, playerTokens[code]);
		});

		return new PlayerEffect(max * configuration.value, false, [], []);
	}
}

Strategies.playerEffects.maxOwnedResourceType = new MaxOwnedResourceTypePlayerEffectStrategy();