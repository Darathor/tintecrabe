/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from '../services/core.mjs';
import { Strategies } from '../services/strategies.mjs';
import { BoxFeature } from "../services/tiles.mjs";
import { game } from "../services/game.mjs";

class NeighboursStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_special_neighbour_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_special_neighbour_' + this.code + '_description');
	}

	/**
	 * @param {BoxFeature} origin
	 * @param {BoxFeature} target
	 * @param {Object} configuration
	 * @returns {BoxFeature[]}
	 */
	resolve(origin, target, configuration) {
		return [];
	}
}

/**
 * Default strategy: empty array.
 */
class NullNeighboursStrategy extends NeighboursStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.neighbours.null = new NullNeighboursStrategy();

/**
 * @typedef {Object} ExternalLinkNeighboursStrategy
 * @property {string|null} activeSinceStep
 */

/**
 * External link strategy: use the externalLink data of the feature.
 */
class ExternalLinkNeighboursStrategy extends NeighboursStrategy {
	constructor() {
		super();
		this.code = 'externalLink';
	}

	/**
	 * @param {BoxFeature} origin
	 * @param {BoxFeature} target
	 * @param {Object} configuration
	 * @returns {BoxFeature[]}
	 */
	resolve(origin, target, configuration) {
		if (!origin.isSame(target)) {
			return [];
		}

		// After playing the tile, the strategy is activated at the specified step (if specified).
		let step = configuration.activeSinceStep;
		if (step && game.isStepOrAfter(step) && (game.lastPlayedBox === origin.box || game.lastPlayedBox === target.box)) {
			return [new BoxFeature(null, null)];
		}

		return [origin.feature.resolveExternalLink()];
	}
}

Strategies.neighbours.externalLink = new ExternalLinkNeighboursStrategy();

/**
 * @typedef {Object} BridgeNeighboursStrategyConfiguration
 * @property {string} featureType
 * @property {string|null} activeSinceStep
 */

/**
 * Bridge strategy: establish a bridge between the structure and another one.
 */
class BridgeNeighboursStrategy extends NeighboursStrategy {
	constructor() {
		super();
		this.code = 'bridge';
	}

	/**
	 * @param {BoxFeature} origin
	 * @param {BoxFeature} target
	 * @param {BridgeNeighboursStrategyConfiguration} configuration
	 * @returns {BoxFeature[]}
	 */
	resolve(origin, target, configuration) {
		if (origin.isSame(target) || target.feature.code !== configuration.featureType) {
			return [];
		}

		// After playing the tile, the strategy is activated at the specified step (if specified).
		let step = configuration.activeSinceStep;
		if (step && !game.isStepOrAfter(step) && (game.lastPlayedBox === origin.box || game.lastPlayedBox === target.box)) {
			return [new BoxFeature(null, null)];
		}

		let externalLink = origin.feature.resolveExternalLink();
		if (externalLink.isEmpty()) {
			return [new BoxFeature(null, null)];
		}

		let result = [];
		externalLink.feature.getNeighboursByType(configuration.featureType).forEach((feature) => {
			result.push(new BoxFeature(externalLink.box, feature));
		});
		return result;
	}
}

Strategies.neighbours.bridge = new BridgeNeighboursStrategy();