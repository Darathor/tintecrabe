/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n } from "../services/core.mjs";
import { Strategies } from "../services/strategies.mjs";

class PlacementConstraintStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_placement_constraint_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_placement_constraint_' + this.code + '_description');
	}

	/**
	 * @param {Feature} feature
	 * @param {Tile} tile
	 * @param {Box} box
	 * @returns {boolean}
	 */
	evaluate(feature, tile, box) {
		return true;
	}
}

/**
 * Default strategy: empty array.
 */
class NullPlacementConstraintStrategy extends PlacementConstraintStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.placementConstraints.null = new NullPlacementConstraintStrategy();

/**
 * Feature neighbours occupied strategy: empty array.
 */
class FeatureNeighboursOccupiedPlacementConstraintStrategy extends PlacementConstraintStrategy {
	constructor() {
		super();
		this.code = 'featureNeighboursOccupied';
	}

	/**
	 * @param {Feature} feature
	 * @param {Tile} tile
	 * @param {Box} box
	 * @returns {boolean}
	 */
	evaluate(feature, tile, box) {
		for (let i = 0; i < 4; i++) {
			let side = tile.getSide(i);
			if (ArrayUtils.contains(side, feature)) {
				let neighbourBox = box.getNeighbour(i);
				if (!neighbourBox || neighbourBox.isEmpty) {
					return false;
				}
			}
		}
		return true;
	}
}

Strategies.placementConstraints.featureNeighboursOccupied = new FeatureNeighboursOccupiedPlacementConstraintStrategy();
