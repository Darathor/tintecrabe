/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n } from '../services/core.mjs';
import { Strategies } from '../services/strategies.mjs';
import { Zones } from "../services/board.mjs";

class ConditionStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_condition_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_condition_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return true;
	}
}

/**
 * Default strategy: always returns true.
 */
class NullConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.conditions.null = new NullConditionStrategy();

/**
 * Closed strategy: returns true when the structure is closed.
 */
class ClosedConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'closed';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return structure.isClosed();
	}
}

Strategies.conditions.closed = new ClosedConditionStrategy();

/**
 * Open strategy: returns true when the structure is not closed.
 */
class OpenConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'open';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return !structure.isClosed();
	}
}

Strategies.conditions.open = new OpenConditionStrategy();

/**
 * Owned by current player strategy: returns true if a pawn is given, and it is owned by the given player.
 */
class OwnedPawnConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'ownedPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return pawn && player && player === pawn.owner;
	}
}

Strategies.conditions.ownedPawn = new OwnedPawnConditionStrategy();

/**
 * @typedef {Object} HasPawnConditionStrategyConfiguration
 * @property {string} pawnType
 */

/**
 * Has pawn strategy: returns true if the player has a pawn of the given type in the structure.
 */
class HasPawnConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'hasPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {HasPawnConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return structure.hasPlayerPawnOfCode((pawn && pawn.owner) ? pawn.owner : player, configuration.pawnType);
	}
}

Strategies.conditions.hasPawn = new HasPawnConditionStrategy();

/**
 * @typedef {Object} PawnOwnerHasEnoughOtherFollowersConditionStrategyConfiguration
 * @property {string[]} pawnTypes
 */

/**
 * Has pawn strategy: returns true if the pawn owner hasn't more pawns of the given types than other followers.
 */
class PawnOwnerHasEnoughOtherFollowersConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'pawnOwnerHasEnoughOtherFollowers';
	}

	/**
	 * @param {Structure} structure
	 * @param {PawnOwnerHasEnoughOtherFollowersConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		if (!pawn) {
			return false;
		}

		let givenTypesCount = 0;
		let otherTypesCount = 0;
		structure.pawns.forEach((structurePawn) => {
			if (structurePawn.owner === pawn.owner && structurePawn.model.type === 'follower') {
				if (ArrayUtils.contains(configuration.pawnTypes, structurePawn.code)) {
					givenTypesCount++;
				}
				else {
					otherTypesCount++;
				}
			}
		});
		return givenTypesCount <= otherTypesCount;
	}
}

Strategies.conditions.pawnOwnerHasEnoughOtherFollowers = new PawnOwnerHasEnoughOtherFollowersConditionStrategy();

/**
 * Already present strategy: returns true if a pawn is given, and it was already present before this turn.
 */
class AlreadyPresentConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'alreadyPresent';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return pawn && !ArrayUtils.contains(game.getNewlyPlayedPawns(), pawn) && !ArrayUtils.contains(game.getNewlyPlayedNeutralPawns(), pawn);
	}
}

Strategies.conditions.alreadyPresent = new AlreadyPresentConditionStrategy();

/**
 * @typedef {Object} FeatureTypeConditionStrategyConfiguration
 * @property {string} featureType
 */

/**
 * Feature type strategy: returns true if a zone centered on the structure's initial box contains a feature of a given type.
 */
class FeatureTypeConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'featureType';
	}

	/**
	 * @param {Structure} structure
	 * @param {FeatureTypeConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		return structure.initialFeature.code === configuration.featureType;
	}
}

Strategies.conditions.featureType = new FeatureTypeConditionStrategy();

/**
 * @typedef {Object} FeatureInZoneConditionStrategyConfiguration
 * @property {string} featureType
 * @property {ZoneData} zone
 */

/**
 * Feature in zone strategy: returns true if a zone centered on the structure's initial box contains a feature of a given type.
 */
class FeatureInZoneConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'featureInZone';
	}

	/**
	 * @param {Structure} structure
	 * @param {FeatureInZoneConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		let result = false;
		Zones.getZone(configuration.zone, structure.initialBox.x, structure.initialBox.y).getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (feature.code === configuration.featureType) {
					result = true;
				}
			});
		});
		return result;
	}
}

Strategies.conditions.featureInZone = new FeatureInZoneConditionStrategy();

/**
 * @typedef {Object} PawnsInZoneConditionStrategyConfiguration
 * @property {string[]} pawnTypes
 * @property {number} min
 * @property {number} max
 * @property {ZoneData} zone
 */

/**
 * Pawns in zone strategy: returns true if a zone centered on the structure's initial box contains pawns count of a given type matching the given min and max.
 */
class PawnsInZoneConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'pawnsInZone';
	}

	/**
	 * @param {Structure} structure
	 * @param {PawnsInZoneConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		let count = 0;
		Zones.getZone(configuration.zone, structure.initialBox.x, structure.initialBox.y).getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				feature.pawns.forEach((pawn) => {
					if (ArrayUtils.contains(configuration.pawnTypes, pawn.code)) {
						count++;
					}
				});
			});
		});
		return count >= (configuration.min || 0) && count <= (configuration.max || 0);
	}
}

Strategies.conditions.pawnsInZone = new PawnsInZoneConditionStrategy();

/**
 * @typedef {Object} AdjacentConditionStrategyConfiguration
 * @property {string} featureType
 * @property {string} featureStatus
 * @property {ZoneData} zone
 */

/**
 * Adjacent strategy: returns true if the structure is adjacent an other one of a given type.
 */
class AdjacentConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'adjacent';
	}

	/**
	 * @param {Structure} structure
	 * @param {AdjacentConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		let result = false;
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach(function(neighbour) {
			if (neighbour.checkStatus(configuration.featureStatus)) {
				result = true;
			}
		});
		return result;
	}
}

Strategies.conditions.adjacent = new AdjacentConditionStrategy();

/**
 * @typedef {Object} OccupiedConditionStrategyConfiguration
 * @property {boolean} alreadyPresent
 * @property {string|null} pawnCode
 */

/**
 * Occupied strategy: returns true if the structure contains at least one pawn.
 */
class OccupiedConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'occupied';
	}

	/**
	 * @param {Structure} structure
	 * @param {OccupiedConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		if (!structure.isOccupied()) {
			return false;
		}
		if (!configuration.pawnCode && !configuration.alreadyPresent) {
			return true;
		}

		let result = false;
		structure.occupiedFeatures.forEach((feature) => {
			feature.pawns.forEach((featurePawn) => {
				if (configuration.alreadyPresent && ArrayUtils.contains(game.getNewlyPlayedPawns(), featurePawn)) {
					return;
				}
				if (!configuration.pawnCode || featurePawn.code === configuration.pawnCode) {
					result = true;
				}
			});
			feature.neutralPawns.forEach((featurePawn) => {
				if (configuration.alreadyPresent && ArrayUtils.contains(game.getNewlyPlayedNeutralPawns(), featurePawn)) {
					return;
				}
				if (!configuration.pawnCode || featurePawn.code === configuration.pawnCode) {
					result = true;
				}
			});
		});
		return result;
	}
}

Strategies.conditions.occupied = new OccupiedConditionStrategy();

/**
 * @typedef {Object} PlayedTileContainsConditionStrategyConfiguration
 * @property {string} featureType
 */

/**
 * Played tile contains strategy: returns true if the played tile contains at least one feature of the given type.
 */
class PlayedTileContainsConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'playedTileContains';
	}

	/**
	 * @param {Structure} structure
	 * @param {PlayedTileContainsConditionStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {AbstractPawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, player, game, pawn) {
		let result = false;
		game.lastPlayedBox.tile.features.forEach((feature) => {
			if (feature.code === configuration.featureType) {
				result = true;
			}
		})
		return result;
	}
}

Strategies.conditions.playedTileContains = new PlayedTileContainsConditionStrategy();