/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n, TimelineItemTrigger } from '../services/core.mjs';
import { Strategies, Structure, StructureEvaluator } from '../services/strategies.mjs';
import { Zones } from "../services/board.mjs";
import { TurnSteps } from "../services/game.mjs";

class EffectStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_effect_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_effect_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		// Do nothing by default.
	}
}

/**
 * Default strategy: no effect.
 */
class NullEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.effects.null = new NullEffectStrategy();

/**
 * Extra turn strategy: give an extra turn to the current player.
 */
class ExtraTurnEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'extraTurn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		document.dispatchEvent(new CustomEvent('giveExtraTurn', { detail: { source: source } }));
	}
}

Strategies.effects.extraTurn = new ExtraTurnEffectStrategy();

/**
 * @typedef {Object} ScorePointsEffectStrategyConfiguration
 * @property {ScorePointsEffectStrategyConfigurationStrategy} strategy
 */

/**
 * @typedef {Object} ScorePointsEffectStrategyConfigurationStrategy
 * @property {string} type
 * @property {Object} configuration
 */

/**
 * Score points strategy: gain points on a given structure.
 */
class ScorePointsEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePoints';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {ScorePointsEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		let strategy = Strategies.getEvaluation(configuration.strategy.type);
		let points = strategy.evaluate(configuration.strategy.configuration, game, structure, undefined, undefined, feature);
		if (points) {
			document.dispatchEvent(new CustomEvent('scorePoints', { detail: { points: points, source: source } }));
		}
	}
}

Strategies.effects.scorePoints = new ScorePointsEffectStrategy();

/**
 * @typedef {Object} GainTokensEffectStrategyConfiguration
 * @property {string} target 'current'|'owner'|'occupants'
 * @property {string[]} types Set an empty array for a random token without restriction.
 * @property {number} quantity
 */

/**
 * Gain tokens strategy: give some random tokens among the given types to the player.
 */
class GainTokensEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'gainTokens';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {GainTokensEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		configuration.target = configuration.target || 'current';

		let tokenCodes = game.tokensPool.getRandomTokens(configuration.quantity, configuration.types);

		let players = [];
		if (configuration.target === 'current') {
			players.push(game.currentPlayer);
		}
		else if (configuration.target === 'owner') {
			if (!pawn || !pawn.owner) {
				console.warn('[GainTokensEffectStrategy.apply] Invalid pawn for target "owner":', pawn);
				return;
			}
			players.push(pawn.owner);
		}
		else if (configuration.target === 'occupants') {
			structure.occupiedFeatures.forEach((feature) => {
				feature.pawns.forEach((pawn) => {
					if (!ArrayUtils.contains(players, pawn.owner)) {
						players.push(pawn.owner);
					}
				});
			});
		}

		players.forEach((player) => {
			player.gainTokens(tokenCodes, source);
		});
	}
}

Strategies.effects.gainTokens = new GainTokensEffectStrategy();

/**
 * Gain message strategy: give a message to the player.
 */
class GainMessageEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'gainMessage';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		game.pushSpecialStep(TurnSteps.APPLY_MESSAGE, {}, source);
	}
}

Strategies.effects.gainMessage = new GainMessageEffectStrategy();

/**
 * @typedef {Object} DrawToHandEffectStrategyConfiguration
 * @property {number} quantity
 */

/**
 * Draw to hand strategy: draw one or more tiles an put them in the player's hand.
 */
class DrawToHandEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'drawToHand';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {DrawToHandEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		document.dispatchEvent(new CustomEvent('drawToHand', { detail: { source: source, quantity: configuration.quantity || 1 } }));
	}
}

Strategies.effects.drawToHand = new DrawToHandEffectStrategy();

/**
 * @typedef {Object} RemovePawnsEffectStrategyConfiguration
 * @property {string} featureType
 * @property {ZoneData} zone
 * @property {EvaluationData[]} evaluation
 */

/**
 * Remove pawns strategy: remove pawns from a structure.
 */
class RemovePawnsEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'removePawns';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {RemovePawnsEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		Zones.getZone(configuration.zone, structure.initialX, structure.initialY).getNonEmptyBoxes().forEach((box) => {
			box.occupiedFeatures.forEach((occupiedFeature) => {
				if (configuration.featureType !== 'any' && configuration.featureType !== occupiedFeature.model.code) {
					return;
				}

				let occupiedStructure = new Structure(occupiedFeature, box);
				if (configuration.evaluation && configuration.evaluation.length) {
					let evaluator = new StructureEvaluator(occupiedStructure, game);
					evaluator.majors.forEach(function(player) {
						player.addPoints(
							evaluator.evaluate(player, configuration.evaluation),
							new TimelineItemTrigger('c.main.source_feature_with_trigger', box.coordinates, source, { feature: occupiedFeature.name })
						);
					});
				}

				occupiedStructure.removeAllPawns(game, true, true);
			});
		});
	}
}

Strategies.effects.removePawns = new RemovePawnsEffectStrategy();

/**
 * @typedef {Object} PutNeutralPawnEffectStrategyConfiguration
 * @property {string|null} pawnCode
 * @property {string|null} pawnSubType
 */

/**
 * Put neutral pawn strategy: put a random neutral pawn from the pool matching the given sub-type.
 */
class PutNeutralPawnEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'putNeutralPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {PutNeutralPawnEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		let newPawn = null;

		if (configuration.pawnCode) {
			newPawn = game.neutralPawnPool.popPawn(configuration.pawnCode);
		}
		else if (configuration.pawnSubType) {
			newPawn = game.neutralPawnPool.popPawnBySubType(configuration.pawnSubType);
		}

		if (newPawn) {
			feature.addNeutralPawn(newPawn);
			structure.initialBox.refreshPawns();
			game.lastPlayedNeutralPawns.push({ pawn: newPawn, feature: feature, box: structure.initialBox });
		}
	}
}

Strategies.effects.putNeutralPawn = new PutNeutralPawnEffectStrategy();

/**
 * @typedef {Object} TriggerAdjacentContinuationEffectStrategyConfiguration
 * @property {string|null} featureType
 */

/**
 * Trigger adjacent continuation strategy: triggers continuation effect on adjacent features.
 */
class TriggerAdjacentContinuationEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'triggerAdjacentContinuation';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {TriggerAdjacentContinuationEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		if (configuration.featureType) {
			structure.getNeighbourStructuresByTypes([configuration.featureType]).forEach((neighbour) => {
				// To be continued, the structure must already exist before this turn.
				if (neighbour.features.length === 1 && neighbour.initialBox === game.lastPlayedBox) {
					return;
				}
				neighbour.onContinued(neighbour.initialFeature, game);
			});
		}
		else {
			console.error('[TriggerAdjacentContinuationEffectStrategy] feature type is required!');
		}
	}
}

Strategies.effects.triggerAdjacentContinuation = new TriggerAdjacentContinuationEffectStrategy();

/**
 * @typedef {Object} ConvertPawnEffectStrategyConfiguration
 * @property {string} from
 * @property {string} to
 */

/**
 * Trigger convert paws strategy: convert pawns occupying on the structure.
 */
class ConvertPawnEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'convertPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {ConvertPawnEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		structure.features.forEach((feature) => {
			feature.pawns.forEach((pawn) => {
				if (pawn.code === configuration.from) {
					pawn.convert(configuration.to);
				}
			});
		});
	}
}

Strategies.effects.convertPawn = new ConvertPawnEffectStrategy();

/**
 * @typedef {Object} TradeResourceEffectStrategyConfiguration
 * @property {TradeOffer[]} offers
 */

/**
 * Trigger trade resources strategy: offer to trade ressources.
 */
class TradeResourcesEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'tradeResources';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {TradeResourceEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @param {TimelineItemTrigger} source
	 */
	apply(structure, feature, configuration, game, pawn, source) {
		game.addTradeOffers(configuration.offers, source);
	}
}

Strategies.effects.tradeResources = new TradeResourcesEffectStrategy();