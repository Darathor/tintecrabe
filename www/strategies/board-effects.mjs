/**
 * Copyright (C) 2023 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { PlayerEffect } from "../structures/PlayerEffect.mjs";
import { Alerts, ArrayUtils, Coordinates, I18n, MathUtils, Timeline, TimelineItemTrigger } from '../services/core.mjs';
import { Strategies, Structure } from '../services/strategies.mjs';
import { Board } from "../services/board.mjs";
import { BoxDirectory } from "../services/directories.mjs";
import { Sounds } from "../services/sounds.mjs";
import { Connection } from "../services/connection.mjs";

export class BoardEffectStrategy {
	/**
	 * @param {Game} game
	 * @param {string} message
	 * @param {Object} substitutions
	 * @param {boolean} dispatch
	 */
	static alert(game, message, substitutions, dispatch) {
		let options = {
			substitutions: substitutions,
			icon: 'images/icons/info.svg'
		};
		if (game.preferences.playSounds) {
			options.sound = Sounds.newAlert;
		}
		Alerts.info('c.timeline.board_divergence_vertical', options);

		if (dispatch) {
			Connection.sendMessage('BoardEffectStrategyAlert', { message: message, substitutions: substitutions });
		}
	}

	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_board_effect_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_board_effect_' + this.code + '_description');
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, source) {
		// Do nothing by default.
	}

	/**
	 * @param {TimelineItemTrigger} source
	 */
	refreshAll(source) {
		BoxDirectory.refreshAccessibilities();
		Strategies.refreshRegistrations(Board.getNonEmptyBoxes([]));

		// Get back helpers if there is no more follower in the structure.
		BoxDirectory.getInstancesArray().forEach((box) => {
			if (!box.isEmpty) {
				box.tile.features.forEach((feature) => {
					feature.pawns.forEach((pawn) => {
						if (pawn.model.type === 'helper') {
							let structure = new Structure(feature, box);
							if (!structure.hasPlayerPawnOfType(pawn.owner, 'follower')) {
								let customData = { pawn: pawn.model.name };
								Timeline.log('c.timeline.helper_alone_back', 'effect', box.coordinates, source, pawn.owner.name, customData);
								feature.removePawn(pawn);
								pawn.back();
								box.refreshPawns();
							}
						}
					});
				});
			}
		});
	}
}

/**
 * Default strategy: no effect.
 */
class NullBoardEffectStrategy extends BoardEffectStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.boardEffects.null = new NullBoardEffectStrategy();

/**
 * @typedef {Object} DivergenceBoardEffectStrategyConfiguration
 * @property {string} direction horizontal|vertical|random
 * @property {number} length
 */

/**
 * Trigger max owned resource type: gain points for each token for each token of the type that the player has the most of.
 */
class DivergenceBoardEffectStrategy extends BoardEffectStrategy {
	constructor() {
		super();
		this.code = 'divergence';
	}

	/**
	 * @param {DivergenceBoardEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 * @returns {PlayerEffect}
	 */
	apply(configuration, game, source) {
		let length = configuration.length || 1;
		let direction = configuration.direction || 'random';
		if (direction === 'random') {
			direction = ArrayUtils.randomItem(['horizontal', 'vertical']);
		}

		switch (direction) {
			case 'horizontal':
				this.doHorizontal(game, length, source);
				break;

			case 'vertical':
				this.doVertical(game, length, source);
				break;

			default:
				console.warn('[DivergenceBoardEffectStrategy.apply] Unknown direction: ' + direction);
		}

		this.refreshAll(source);
	}

	/**
	 * @param {Game} game
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 */
	doHorizontal(game, length, source) {
		let position = (MathUtils.randomInt(0, 1)) ? this.doHorizontalTop(length, source) : this.doHorizontalBottom(length, source);

		Board.initMissingBoxes();

		let customData = { length: length, position: position };
		Timeline.log('c.timeline.board_divergence_vertical', 'board', new Coordinates(0, position), source, null, customData);
		BoardEffectStrategy.alert(game, 'c.timeline.board_divergence_vertical', customData, true);
	}

	/**
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 * @returns {number}
	 */
	doHorizontalTop(length, source) {
		let position = MathUtils.randomInt(Board.realTop, 0);
		if (Board.limits.maxTop) {
			Board.limits.maxTop -= length;
		}
		for (let i = 0; i < length; i++) {
			Board.expandTop(false);
		}

		for (let i = Board.realLeft; i <= Board.realRight; i++) {
			for (let j = Board.realTop; j <= (position - length); j++) {
				BoxDirectory.move(BoxDirectory.instances[i + '_' + (j + length)], i, j);
			}
		}
		return position;
	}

	/**
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 * @returns {number}
	 */
	doHorizontalBottom(length, source) {
		let position = MathUtils.randomInt(0, Board.realBottom);
		if (Board.limits.maxBottom) {
			Board.limits.maxBottom += length;
		}
		for (let i = 0; i < length; i++) {
			Board.expandBottom(false);
		}

		for (let i = Board.realLeft; i <= Board.realRight; i++) {
			for (let j = Board.realBottom; j >= (position + length); j--) {
				BoxDirectory.move(BoxDirectory.instances[i + '_' + (j - length)], i, j);
			}
		}
		return position;
	}

	/**
	 * @param {Game} game
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 */
	doVertical(game, length, source) {
		let position = (MathUtils.randomInt(0, 1)) ? this.doVerticalLeft(length, source) : this.doVerticalRight(length, source);

		Board.initMissingBoxes();

		let customData = { length: length, position: Coordinates.formatX(position) };
		Timeline.log('c.timeline.board_divergence_horizontal', 'board', new Coordinates(position, 0), source, null, customData);
		BoardEffectStrategy.alert(game, 'c.timeline.board_divergence_horizontal', customData, true);
	}

	/**
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 * @returns {number}
	 */
	doVerticalLeft(length, source) {
		let position = MathUtils.randomInt(Board.realLeft, 0);
		if (Board.limits.maxLeft) {
			Board.limits.maxLeft -= length;
		}
		for (let i = 0; i < length; i++) {
			Board.expandLeft(false);
		}

		for (let j = Board.realTop; j <= Board.realBottom; j++) {
			for (let i = Board.realLeft; i <= (position - length); i++) {
				BoxDirectory.move(BoxDirectory.instances[(i + length) + '_' + j], i, j);
			}
		}
		return position;
	}

	/**
	 * @param {number} length
	 * @param {TimelineItemTrigger} source
	 * @returns {number}
	 */
	doVerticalRight(length, source) {
		let position = MathUtils.randomInt(0, Board.realRight);
		if (Board.limits.maxRight) {
			Board.limits.maxRight += length;
		}
		for (let i = 0; i < length; i++) {
			Board.expandRight(false);
		}

		for (let j = Board.realTop; j <= Board.realBottom; j++) {
			for (let i = Board.realRight; i >= (position + length); i--) {
				BoxDirectory.move(BoxDirectory.instances[(i - length) + '_' + j], i, j);
			}
		}
		return position;
	}
}

Strategies.boardEffects.divergence = new DivergenceBoardEffectStrategy();