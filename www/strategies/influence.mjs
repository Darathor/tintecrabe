/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n, TimelineItemTrigger } from '../services/core.mjs';
import { Strategies, Structure } from '../services/strategies.mjs';
import { Zones } from "../services/board.mjs";

class InfluenceStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_influence_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_influence_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {ZoneData} zoneData
	 */
	register(structure, zoneData) {
		let positions = Zones.getZone(zoneData, structure.initialX, structure.initialY).positions;
		Strategies.registerFeatureInfluence(structure.initialFeature, structure.initialBox, positions);
	}

	/**
	 * @param {CoordinatesData} zoneOrigin
	 * @param {ZoneData} zoneData
	 * @param {CoordinatesData} coordinates
	 */
	checkZone(zoneOrigin, zoneData, coordinates) {
		return Zones.getZone(zoneData, zoneOrigin.x, zoneOrigin.y).contains(coordinates.x, coordinates.y);
	}

	/**
	 * @param {Box} box
	 * @param {Structure} sourceStructure
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(box, sourceStructure, configuration, game, source) {
		// Do nothing by default.
	}
}

/**
 * Default strategy: do nothing.
 */
class NullInfluenceStrategy extends InfluenceStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.influence.null = new NullInfluenceStrategy();

/**
 * @typedef {Object} FeaturePlacedInfluenceStrategyConfiguration
 * @property {string=} featureType
 * @property {string=} featureCategory
 * @property {string=} featureStatus close|open|any
 * @property {boolean} oncePerTile
 * @property {Object[]} effects
 */

/**
 * Feature placed strategy: triggered when a feature of the required type/category is placed in the zone.
 */
class FeaturePlacedInfluenceStrategy extends InfluenceStrategy {
	constructor() {
		super();
		this.code = 'featurePlaced';
	}

	/**
	 * @param {Box} box
	 * @param {Structure} sourceStructure
	 * @param {FeaturePlacedInfluenceStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(box, sourceStructure, configuration, game, source) {
		let applicationCount = 0;
		Structure.getBoxesStructures([box], configuration, []).forEach(() => {
			if (configuration.oncePerTile && applicationCount) {
				return;
			}
			configuration.effects.forEach((strategyData) => {
				let strategy = Strategies.getEffect(strategyData.type);
				let effectSource = new TimelineItemTrigger(
					'c.main.source_feature',  sourceStructure.initialBox.coordinates, source, { feature: sourceStructure.initialFeature.name }
				);
				strategy.apply(sourceStructure, sourceStructure.initialFeature, strategyData.configuration || {}, game, null, effectSource);
				applicationCount++;
			});
		});
	}
}

Strategies.influence.featurePlaced = new FeaturePlacedInfluenceStrategy();

/**
 * @typedef {Object} PawnPlacedInfluenceStrategyConfiguration
 * @property {boolean} oncePerTile
 * @property {Object[]} effects
 */

/**
 * Pawn placed strategy: triggered when a pawn is placed in the zone.
 */
class PawnPlacedInfluenceStrategy extends InfluenceStrategy {
	constructor() {
		super();
		this.code = 'pawnPlaced';
	}

	/**
	 * @param {Box} box
	 * @param {Structure} sourceStructure
	 * @param {PawnPlacedInfluenceStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(box, sourceStructure, configuration, game, source) {
		let applicationCount = 0;
		game.lastPlayedPawns.forEach((playedPawn) => {
			if (playedPawn.box !== box) {
				return;
			}

			if (configuration.oncePerTile && applicationCount) {
				return;
			}
			configuration.effects.forEach((strategyData) => {
				let strategy = Strategies.getEffect(strategyData.type);
				let effectSource = new TimelineItemTrigger(
					'c.main.source_feature',  sourceStructure.initialBox.coordinates, source, { feature: sourceStructure.initialFeature.name }
				);
				strategy.apply(sourceStructure, sourceStructure.initialFeature, strategyData.configuration || {}, game, null, effectSource);
				applicationCount++;
			});
		});
	}
}

Strategies.influence.pawnPlaced = new PawnPlacedInfluenceStrategy();
