/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n, ArrayUtils } from '../services/core.mjs';
import { Zones } from '../services/board.mjs';
import { Strategies, Structure } from '../services/strategies.mjs';
import { Ranking } from "../services/ranking.mjs";

class EvaluationStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_evaluation_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_evaluation_' + this.code + '_description');
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		return 0;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @param {Structure} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @returns {number}
	 */
	evaluateBoxes(boxes, configuration, structure, player, pawns) {
		if (configuration.type === 'tiles') {
			return this.evaluateBoxesTiles(boxes, configuration);
		}
		else if (configuration.type === 'features') {
			return this.evaluateBoxesFeatures(boxes, configuration, structure);
		}
		else if (configuration.type === 'pawns') {
			return this.evaluateBoxesPawns(boxes, configuration, structure);
		}
		console.warn('[AbstractAreaEvaluationStrategy.evaluateBoxes] Unknown type:', configuration.type);
		return 0;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @returns {number}
	 */
	evaluateBoxesTiles(boxes, configuration) {
		return boxes.length * configuration.value;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @param {Structure} structure
	 * @returns {number}
	 */
	evaluateBoxesFeatures(boxes, configuration, structure) {
		let result = 0;

		Structure.getBoxesStructures(boxes, configuration, structure.features).forEach((structure) => {
			result += configuration.value;
		});

		return result;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @param {Structure} structure
	 * @returns {number}
	 */
	evaluateBoxesPawns(boxes, configuration, structure) {
		if (!configuration.pawnTypes || !configuration.pawnTypes.length) {
			console.warn('[AbstractAreaEvaluationStrategy.evaluateBoxesPawns] Missing pawnTypes');
			return 0;
		}

		let result = 0;

		boxes.forEach((box) => {
			if (!box.tile) {
				return;
			}

			box.tile.features.forEach((feature) => {
				feature.pawns.forEach((pawn) => {
					if (ArrayUtils.contains(configuration.pawnTypes, pawn.code)) {
						result += configuration.value;
					}
				});
			});
		});

		return result;
	}
}

/**
 * Default strategy: always 0 points.
 */
class NullEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.evaluation.null = new NullEvaluationStrategy();

/**
 * Fixed strategy: a fixed points number.
 * Parameters:
 *  - value: points
 */
class FixedEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'fixed';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		return configuration.value;
	}
}

Strategies.evaluation.fixed = new FixedEvaluationStrategy();

/**
 * Tiles strategy: n points for each tile in the structure.
 * Parameters:
 *  - value: points per tile
 */
class TilesEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'tiles';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		return structure ? this.evaluateBoxesTiles(structure.boxes, configuration) : 0;
	}
}

Strategies.evaluation.tiles = new TilesEvaluationStrategy();

/**
 * @typedef {Object} AdjacentEvaluationStrategyConfiguration
 * @property {number} value - points per structure.
 * @property {string} featureType - feature type.
 * @property {string} featureStatus - feature status close|open|any.
 */

/**
 * Adjacent strategy: n points for each adjacent structure of a given type.
 * Parameters:
 *  - value: points per structure.
 *  - featureType: the feature type to count.
 *  - featureStatus: feature status close|open|any.
 */
class AdjacentEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'adjacent';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!structure) {
			return 0;
		}

		let result = 0;
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach((neighbour) => {
			if (neighbour.checkStatus(configuration.featureStatus)) {
				result += configuration.value;
			}
		});
		return result;
	}
}

Strategies.evaluation.adjacent = new AdjacentEvaluationStrategy();

/**
 * @typedef {Object} TilesInAdjacentEvaluationStrategyConfiguration
 * @property {number} value - points per tile in adjacent structure.
 * @property {string} featureType - feature type.
 * @property {string} featureStatus - feature status close|open|any.
 */

/**
 * Tiles in adjacent strategy: n points for tiles in each adjacent structure of a given type.
 * Parameters:
 *  - value: points per tile in adjacent structure.
 *  - featureType: the feature type to count.
 *  - featureStatus: feature status close|open|any.
 */
class TilesInAdjacentEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'tilesInAdjacent';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!structure) {
			return 0;
		}

		let result = 0;
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach((neighbour) => {
			if (neighbour.checkStatus(configuration.featureStatus)) {
				result += this.evaluateBoxesTiles(neighbour.boxes, configuration);
			}
		});
		return result;
	}
}

Strategies.evaluation.tilesInAdjacent = new TilesInAdjacentEvaluationStrategy();

/**
 * Square strategy: n points for each element in a square centered on the feature.
 * Parameters:
 *  - length: the square tile count from the center.
 *  - type: element checking mode.
 *  - value: points per element.
 * Parameters for type:
 *  - features:
 *    - featureType: the feature type to count (required if featureCategory is omitted).
 *    - featureCategory: the feature category to count (required if featureType is omitted).
 *    - featureStatus: feature status close|open|any.
 *  - pawns:
 *    - pawnTypes: the pawn type(s) to count.
 */
class SquareEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'square';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!structure) {
			return 0;
		}

		let boxes = Zones.getSquare(structure.initialX, structure.initialY, configuration.length).getNonEmptyBoxes();
		return this.evaluateBoxes(boxes, configuration, structure, player, pawns);
	}
}

Strategies.evaluation.square = new SquareEvaluationStrategy();

/**
 * Diamond strategy: n points for each element in a "diamond" centered on the feature.
 * Parameters:
 *  - length: the diamond tile count from the center (horizontally and vertically).
 *  - type: element checking mode.
 *  - value: points per element.
 * Parameters for type:
 *  - feature:
 *    - featureType: the feature type to count (required if featureCategory is omitted).
 *    - featureCategory: the feature category to count (required if featureType is omitted).
 *    - featureStatus: feature status close|open|any.
 *  - pawns:
 *    - pawnTypes: the pawn type(s) to count.
 */
class DiamondEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'diamond';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!structure) {
			return 0;
		}

		let boxes = Zones.getDiamond(structure.initialX, structure.initialY, configuration.length).getNonEmptyBoxes();
		return this.evaluateBoxes(boxes, configuration, structure, player, pawns);
	}
}

Strategies.evaluation.diamond = new DiamondEvaluationStrategy();

/**
 * Star strategy: n points for each element in a "star" (horizontal, vertical and diagonal lines) centered on the feature.
 * Parameters:
 *  - length: the diamond tile count from the center (horizontally, vertically and diagonally).
 *  - type: element checking mode.
 *  - value: points per element.
 * Parameters for type:
 *  - feature:
 *    - featureType: the feature type to count (required if featureCategory is omitted).
 *    - featureCategory: the feature category to count (required if featureType is omitted).
 *    - featureStatus: feature status close|open|any.
 *  - pawns:
 *    - pawnTypes: the pawn type(s) to count.
 */
class StarEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'star';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!structure) {
			return 0;
		}

		let boxes = Zones.getStar(structure.initialX, structure.initialY, configuration.length).getNonEmptyBoxes();
		return this.evaluateBoxes(boxes, configuration, structure, player, pawns);
	}
}

Strategies.evaluation.star = new StarEvaluationStrategy();

/**
 * Connections strategy: score points depending on the number of connections between the last played feature and the existing ones in the structure.
 */
class ConnectionsEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'connections';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!lastFeature || !structure) {
			return 0;
		}

		let box = structure.getBoxByFeature(lastFeature);
		return configuration.thresholds[structure.getConnectionsCountByBox(box)];
	}
}

Strategies.evaluation.connections = new ConnectionsEvaluationStrategy();

/**
 * @typedef {Object} TokenRankFixedEvaluationStrategyConfiguration
 * @property {string} tokenType - token type.
 * @property {number[]} scores - the points by ranking.
 */

/**
 * Token rank fixed strategy: score points depending on the rank position of the player on a given token type.
 */
class TokenRankFixedEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'tokenRankFixed';
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Structure=} structure
	 * @param {Player=} player
	 * @param {PlayerPawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(configuration, game, structure, player, pawns, lastFeature) {
		if (!player) {
			return 0;
		}

		let ranking = Ranking.buildForTokenCode(game, configuration.tokenType);
		let position = ranking.getPlayerPosition(player);
		// If the player has no token for this type, no points gained.
		if (!position) {
			return 0; 
		}

		return configuration.scores[Math.min(position, configuration.scores.length) - 1];
	}
}

Strategies.evaluation.tokenRankFixed = new TokenRankFixedEvaluationStrategy();