/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
let Vue = window.Vue;

export let StringUtils = {
	/**
	 * @param {string} value
	 * @returns {string}
	 */
	ucfirst(value) {
		if (!value) {
			return '';
		}
		value = value.toString();
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
};

export let DateUtils = {
	timeFormat: new Intl.DateTimeFormat('fr-FR', { hour: 'numeric', minute: 'numeric', second: 'numeric' }),
	/**
	 * @param {Date} date
	 * @returns {string}
	 */
	time(date) {
		return this.timeFormat.format(date);
	},
	/**
	 * @param {Date} from
	 * @param {Date} to
	 * @returns number
	 */
	getSecondsDifference(from, to) {
		return Math.floor((to.getTime() - from.getTime()) / 1000);
	},
	/**
	 * @param {Date} from
	 * @param {Date} to
	 * @returns number
	 */
	getMinutesDifference(from, to) {
		return this.getSecondsDifference(from, to) / 60;
	}
};

export let MathUtils = {
	/**
	 * @param {number} min
	 * @param {number} max
	 * @returns {number}
	 */
	randomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}
};

export let ArrayUtils = {
	/**
	 * @param {Array} array
	 * @param {number} index
	 * @returns {number}
	 */
	nextIndex(array, index) {
		return (index + 1) < array.length ? (index + 1) : 0;
	},
	/**
	 * @param {Array} array
	 * @returns {number}
	 */
	randomIndex(array) {
		return MathUtils.randomInt(0, array.length);
	},
	/**
	 * @param {Array} array
	 * @returns {*}
	 */
	randomItem(array) {
		return array[MathUtils.randomInt(0, array.length)];
	},
	/**
	 * @param {Array} array1
	 * @param {Array} array2
	 * @returns {Array}
	 */
	merge(array1, array2) {
		for (let i = 0; i < array2.length; i++) {
			array1.push(array2[i]);
		}
		return array1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {boolean}
	 */
	contains(array, item) {
		return array.indexOf(item) > -1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {Array}
	 */
	remove(array, item) {
		let index = array.indexOf(item);
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @param {*} toReplace
	 * @param {*} newItem
	 * @returns {Array}
	 */
	replace(array, toReplace, newItem) {
		let index = array.indexOf(toReplace);
		if (index > -1) {
			array.splice(index, 1, [newItem]);
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	clear(array) {
		array.splice(0, array.length);
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	shuffle(array) {
		for (let i = array.length - 1; i > 0; i--) {
			let j = Math.floor(Math.random() * (i + 1));
			let x = array[i];
			array[i] = array[j];
			array[j] = x;
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	sortNumber(array) {
		array.sort(function(a, b) { return a - b; });
		return array;
	}
};

export let I18n = {
	LCID: 'fr_FR',
	keys: {},
	keysByLCID: {},
	registeredFilePaths: [
		{ prefix: 'c.main.', path: 'i18n/main/' },
		{ prefix: 'c.timeline.', path: 'i18n/timeline/' },
		{ prefix: 'c.setup.', path: 'i18n/setup/' }
	],
	/**
	 * @param {string} LCID
	 * @param {function} endCallback
	 */
	init(LCID, endCallback) {
		this.LCID = LCID;
		if (this.keysByLCID[LCID]) {
			this.keys = this.keysByLCID[LCID];
			console.log('Language set to ' + this.LCID + ' (from cache)');
			return;
		}

		this.keysByLCID[LCID] = {};
		let loader = new FileLoader();
		let _endCallback = () => {
			this.keys = this.keysByLCID[LCID];
			console.log('Language set to ' + this.LCID);
			endCallback();
		}
		this.registeredFilePaths.forEach((item) => {
			loader.loadFile(item.path + LCID + '.json', (data) => { this.importPackage(data, item.prefix); }, _endCallback);
		});
	},
	switchLCID() {
		this.init(this.LCID === 'fr_FR' ? 'en_US' : 'fr_FR', () => {});
	},
	/**
	 * @param {string} prefix
	 * @param {string} path
	 */
	registerFilePath(prefix, path) {
		this.registeredFilePaths.push({ prefix: prefix, path: path });
	},
	/**
	 * @param {Object} data
	 * @param {string} prefix
	 */
	importPackage(data, prefix) {
		Object.keys(data).map((key) => {
			if (!('message' in data[key])) {
				console.warn('Ignore key declaration without message:', key);
				return;
			}
			let fullKey = prefix + key;
			if (fullKey in this.keysByLCID[this.LCID]) {
				console.warn('Ignore duplicate key declaration:', key);
				return;
			}
			this.keysByLCID[this.LCID][fullKey] = data[key].message;
		});
	},
	/**
	 * @param {string} key
	 * @param {string[]=} transformers
	 * @param {Object=} substitutions
	 * @returns {string}
	 */
	trans(key, transformers, substitutions) {
		let text = this.keys[key] || key;
		if (substitutions) {
			Object.keys(substitutions).forEach((key) => {
				text = text.replace('$' + key + '$', substitutions[key]);
			});
		}
		if (transformers) {
			transformers.forEach((transformer) => {
				if (transformer === 'etc') {
					text += '…';
				}
				else if (transformer === 'lab') {
					text += this.LCID === 'fr_FR' ? ' :' : ':';
				}
				else if (transformer === 'ucf') {
					text = text[0].toUpperCase() + text.slice(1);
				}
			});
		}
		return text;
	}
};

/**
 * @typedef {Object} CoordinatesData
 * @property {number} x
 * @property {number} y
 */

export class Coordinates {
	/**
	 * @param {number|string} x
	 * @param {number} y
	 */
	constructor(x, y) {
		this.x = parseX(x);
		this.y = y;
	}

	/**
	 * @param {CoordinatesData|null} data
	 * @return {Coordinates|null}
	 */
	static fromData(data) {
		return (data && data.hasOwnProperty('x') && data.hasOwnProperty('y')) ? (new Coordinates(data.x, data.y)) : null;
	}

	/**
	 * @returns {CoordinatesData}
	 */
	toJSON() {
		return { x: this.x, y: this.y };
	}

	/**
	 * @param {Coordinates|CoordinatesData} value
	 * @returns {string}
	 */
	static format(value) {
		return '[' + Coordinates.formatX(value.x) + ';' + value.y + ']';
	}

	/**
	 * @param {number} x
	 * @returns {string}
	 */
	static formatX = (x) => {
		if (x < 0) {
			return '-' + Coordinates.formatX(-x);
		}
		else if (x > 26) {
			return String.fromCharCode(65 + (x % 26)) + Coordinates.formatX(Math.floor(x / 26));
		}
		return String.fromCharCode(65 + x);
	}
}

/**
 * @param {number|string} x
 * @returns {number}
 */
let parseX = (x) => {
	if (Number.isInteger(x)) {
		return x;
	}

	let cleanValue = 0;
	if (typeof x === 'string' || x instanceof String) {
		let power = 0;
		[...x].reverse().forEach((char) => {
			if (char === '-') {
				cleanValue = -cleanValue;
			}
			else {
				cleanValue += Math.pow(26, power) * convertCharCode(char.charCodeAt(0));
				power++;
			}
		});
	}
	return cleanValue;
}

/**
 * @param {number} charCode
 */
let convertCharCode = (charCode) => {
	if (charCode >= 65 && charCode <= 90) {
		return charCode - 65;
	}
	if (charCode >= 97 && charCode <= 122) {
		return charCode - 97;
	}
	console.warn('[convertCharCode] Unexpected char code:', charCode);
	return 0;
}

/**
 * @typedef {Object} TimelineItemTriggerData
 * @property {string} key
 * @property {CoordinatesData|null} coordinates
 * @property {TimelineItemTriggerData|null} trigger
 * @property {Object|null} custom
 */

export class TimelineItemTrigger {
	/**
	 * @param {string} key
	 * @param {Coordinates|null=} coordinates
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {Object|null=} customData Serializable custom data.
	 */
	constructor(key, coordinates, trigger, customData) {
		this.key = key;
		this.coordinates = coordinates;
		this.trigger = trigger;
		this.customData = customData;
	}

	/**
	 * @param {TimelineItemTriggerData|null} data
	 * @return {TimelineItemTrigger|null}
	 */
	static fromData(data) {
		if (!data || !data.hasOwnProperty('key')) {
			return null;
		}
		let coordinates = data.coordinates ? Coordinates.fromData(data.coordinates) : null;
		let trigger = data.trigger ? TimelineItemTrigger.fromData(data.trigger) : null;
		return new TimelineItemTrigger(data.key, coordinates, trigger, data.custom);
	}

	/**
	 * @returns {TimelineItemTriggerData}
	 */
	toJSON() {
		return {
			key: this.key,
			coordinates: this.coordinates ? this.coordinates.toJSON() : null,
			trigger: this.trigger ? this.trigger.toJSON() : null,
			custom: this.customData
		};
	}

	/**
	 * @returns {Object}
	 */
	get substitutions() {
		let substitutions = this.customData ? JSON.parse(JSON.stringify(this.customData)) : {};

		if (this.coordinates) {
			substitutions.coordinates = Coordinates.format(this.coordinates);
		}

		if (this.trigger) {
			substitutions.trigger = I18n.trans(this.trigger.key, [], this.trigger.substitutions);
		}
		else {
			substitutions.trigger = substitutions.trigger || '';
		}

		if (substitutions.tokenKey) {
			substitutions.token = I18n.trans(substitutions.tokenKey);
		}

		if (substitutions.pawnKey) {
			substitutions.pawn = I18n.trans(substitutions.pawnKey);
		}

		return substitutions;
	}

	/**
	 * @returns {string}
	 */
	get label() {
		return I18n.trans(this.key, [], this.substitutions);
	}

	/**
	 * @returns {string|null}
	 */
	get messageCode() {
		if (this.customData && this.customData.messageCode) {
			return this.customData.messageCode;
		}
		return null;
	}

	/**
	 * @returns {string|null}
	 */
	get boardEventCode() {
		if (this.customData && this.customData.boardEventCode) {
			return this.customData.boardEventCode;
		}
		return null;
	}

	/**
	 * @returns {boolean}
	 */
	get clickable() {
		return !!(this.coordinates || this.messageCode || this.boardEventCode);
	}

	/**
	 * @returns {string|null}
	 */
	get tooltip() {
		if (this.coordinates) {
			return I18n.trans('c.main.center_display_on_coordinates');
		}
		else if (this.messageCode) {
			return I18n.trans('c.main.show_message');
		}
		else if (this.boardEventCode) {
			return I18n.trans('c.main.show_board_event');
		}
		return null;
	}

	onClick() {
		if (this.coordinates) {
			document.dispatchEvent(new CustomEvent('centerOnCoordinates', { detail: { coordinates: this.coordinates } }));
		}
		else if (this.messageCode) {
			document.dispatchEvent(new CustomEvent('showMessageModal', { detail: { messageCode: this.messageCode } }));
		}
		else if (this.boardEventCode) {
			document.dispatchEvent(new CustomEvent('showBoardEventModal', { detail: { boardEventCode: this.boardEventCode } }));
		}
	}
}

/**
 * @typedef {Object} TimelineItemData
 * @property {string} message
 * @property {string} date
 * @property {string} category
 * @property {CoordinatesData|null} coordinates
 * @property {TimelineItemTriggerData|null} trigger
 * @property {string|null} player
 * @property {Object|null} custom
 */

export class TimelineItem {
	/**
	 * @param {string} message
	 * @param {Date} date
	 * @param {string|null=} category
	 * @param {Coordinates|null=} coordinates
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {string|null=} playerName
	 * @param {Object|null=} customData Serializable custom data.
	 */
	constructor(message, date, category, coordinates, trigger, playerName, customData) {
		this.message = message;
		this.date = date;
		this.coordinates = coordinates;
		this.trigger = trigger;
		this.playerName = playerName;
		this.customData = customData;
		this.category = category || 'default';

		if (!this.coordinates && this.trigger && this.trigger.coordinates) {
			this.coordinates = this.trigger.coordinates;
		}
	}

	/**
	 * @returns {Object}
	 */
	get substitutions() {
		let substitutions = this.customData ? JSON.parse(JSON.stringify(this.customData)) : {};

		if (this.coordinates) {
			substitutions.coordinates = Coordinates.format(this.coordinates);
		}

		if (this.playerName) {
			substitutions.player = this.playerName;
		}

		if (substitutions.tokenKey) {
			substitutions.token = I18n.trans(substitutions.tokenKey);
		}

		if (substitutions.pawnKey) {
			substitutions.pawn = I18n.trans(substitutions.pawnKey);
		}

		return substitutions;
	}

	/**
	 * @param {TimelineItemData} data
	 * @return
	 */
	static fromData(data) {
		return new TimelineItem(
			data.message, new Date(data.date), data.category,
			Coordinates.fromData(data.coordinates), TimelineItemTrigger.fromData(data.trigger),
			data.player, data.custom
		);
	}

	/**
	 * @returns {TimelineItemData}
	 */
	get data() {
		return {
			message: this.message,
			date: this.date.toJSON(),
			category: this.category,
			coordinates: this.coordinates ? this.coordinates.toJSON() : null,
			trigger: this.trigger ? this.trigger.toJSON() : null,
			player: this.playerName,
			custom: this.customData
		};
	}

	/**
	 * @returns {string|null}
	 */
	get messageCode() {
		if (this.customData && this.customData.messageCode) {
			return this.customData.messageCode;
		}
		else if (this.trigger && this.trigger.customData && this.trigger.customData.messageCode) {
			return this.trigger.customData.messageCode;
		}
		return null;
	}

	/**
	 * @returns {boolean}
	 */
	get clickable() {
		return !!(this.coordinates || this.messageCode);
	}

	/**
	 * @returns {string|null}
	 */
	get tooltip() {
		if (this.messageCode) {
			return I18n.trans('c.main.show_message');
		}
		else if (this.coordinates) {
			return I18n.trans('c.main.center_display_on_coordinates');
		}
		return null;
	}

	onClick() {
		if (this.messageCode) {
			document.dispatchEvent(new CustomEvent('showMessageModal', { detail: { messageCode: this.messageCode } }));
		}
		else if (this.coordinates) {
			document.dispatchEvent(new CustomEvent('centerOnCoordinates', { detail: { coordinates: this.coordinates } }));
		}
	}
}

/**
 * @typedef {Object} TimelineData
 * @property {TimelineItemData[]} items
 */

export let Timeline = {
	/** @type {TimelineItem[]} */
	items: [],
	/**
	 * @param {string} messageKey
	 * @param {string|null=} category
	 * @param {Coordinates|null=} coordinates
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {string|null=} playerName
	 * @param {Object|null=} customData
	 */
	log(messageKey, category, coordinates, trigger, playerName, customData) {
		this.items.unshift(new TimelineItem(messageKey, new Date(), category, coordinates, trigger, playerName, customData));
	},
	/**
	 * @param {string} categoryName
	 */
	clearCategory(categoryName) {
		let toRemove = [];
		this.items.forEach((item) => {
			if (item.category === categoryName) {
				toRemove.push(item);
			}
		});
		toRemove.forEach((item) => {
			ArrayUtils.remove(this.items, item);
		});
	},
	/**
	 * @returns {TimelineData}
	 */
	get data() {
		let data = {
			items: []
		};

		this.items.forEach((item) => {
			data.items.push(item.data);
		});

		return data;
	},
	/**
	 * @param {TimelineData} data
	 */
	set data(data) {
		this.items = [];

		data.items.forEach((item) => {
			this.items.push(TimelineItem.fromData(item));
		});
	}
};

export class Alert {
	//region static stuff
	static INFO = 'info';
	static SUCCESS = 'success';
	static WARNING = 'warning';
	static DANGER = 'danger';
	//endregion

	/**
	 * @param {string} id
	 * @param {string} level
	 * @param {string} message The message's i18n key.
	 * @param {Object=} substitutions
	 * @param {string=} icon
	 */
	constructor(id, level, message, substitutions, icon) {
		this.id = id;
		this.level = level;
		this.message = message;
		this.substitutions = substitutions;
		this.icon = icon;
	}
}

export let Alerts = {
	/** @var Object[] */
	items: [],
	/** @var number */
	defaultTimeout: 5000,

	/**
	 * @param {string} message
	 * @param {Object} options
	 */
	success(message, options = {}) {
		options.level = Alert.SUCCESS;
		this.add(message, options);
	},
	/**
	 * @param {string} message
	 * @param {Object} options
	 */
	info(message, options = {}) {
		options.level = Alert.INFO;
		this.add(message, options);
	},
	/**
	 * @param {string} message
	 * @param {Object} options
	 */
	warning(message, options = {}) {
		options.level = Alert.WARNING;
		this.add(message, options);
	},
	/**
	 * @param {string} message
	 * @param {Object} options
	 */
	danger(message, options = {}) {
		options.level = Alert.DANGER;
		this.add(message, options);
	},
	/**
	 * @param {string} message
	 * @param {Object} options
	 */
	add(message, { level, substitutions, timeout, sound, icon }) {
		let item = new Alert(`${Date.now()}-${Math.random()}`, level, message, substitutions, icon);
		this.items.push(item);
		setTimeout(() => this.remove(item), timeout || this.defaultTimeout);
		if (sound) {
			sound.play();
		}
	},
	/**
	 * @param {Object} item
	 */
	remove(item) {
		let i = this.items.indexOf(item);
		if (i >= 0) {
			this.items.splice(i, 1);
		}
	}
};

export let LocalStorage = {
	/**
	 * @param {string} key
	 * @returns {boolean}
	 */
	isset(key) {
		return this.get(key) !== null;
	},
	/**
	 * @param {string} key
	 * @param {string} value
	 */
	set(key, value) {
		localStorage.setItem('tc.' + key, value)
	},
	/**
	 * @param {string} key
	 * @param {Object} value
	 */
	setObject(key, value) {
		this.set(key, JSON.stringify(value));
	},
	/**
	 * @param {string} key
	 * @returns {string|null}
	 */
	get(key) {
		return localStorage.getItem('tc.' + key);
	},
	/**
	 * @param {string} key
	 * @returns {Object|null}
	 */
	getObject(key) {
		let value = this.get(key);
		return value ? JSON.parse(value) : null;
	}
};

export class FileLoader {
	constructor() {
		this.requests = 0;
	}

	/**
	 * @param {string} filePath
	 * @param {function} fileCallback
	 * @param {function} endCallback
	 */
	loadFile(filePath, fileCallback, endCallback) {
		this.requests++;
		fetch(filePath).then((response) => {
			if (response.ok) {
				response.json().then((data) => {
					fileCallback(data);
					this.requests--;
					if (!this.requests) {
						endCallback();
					}
				});
			}
			else {
				console.warn('filePath not found:', filePath);
				this.requests--;
				if (!this.requests) {
					endCallback();
				}
			}
		});
	}
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
// Taken from https://davidwalsh.name/function-debounce
export function debounce(func, wait, immediate) {
	let timeout;
	return function() {
		let context = this, args = arguments;
		let later = function() {
			timeout = null;
			if (!immediate) {
				func.apply(context, args);
			}
		};
		let callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait || 500);
		if (callNow) {
			func.apply(context, args);
		}
	};
}

//region Filters.

Vue.filter('ucfirst', (value) => {
	return StringUtils.ucfirst(value);
});

Vue.filter('time', (value) => {
	return DateUtils.time(value);
});

Vue.filter('trans', (value, transformers, substitutions) => {
	return I18n.trans(value, transformers, substitutions);
});

Vue.filter('coordinates', (value) => {
	return Coordinates.format(value);
});

//endregion