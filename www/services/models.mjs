/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @typedef {Object} ZoneData
 * @property {string} type
 * @property {number} size
 */

/**
 * @typedef {Object} ConditionData
 * @property {string} type
 * @property {boolean} not
 * @property {Object} configuration
 */

/**
 * @typedef {Object} EffectData
 * @property {string} type
 * @property {ConditionData[]} conditions
 * @property {Object} configuration
 */

/**
 * @typedef {Object} CompletionData
 * @property {string} type
 * @property {Object} configuration
 */

/**
 * @typedef {Object} EvaluationData
 * @property {string} type
 * @property {ConditionData[]} conditions
 * @property {Object} configuration
 */

export let Models = {
	/** @type {string[]} */
	extensionCodes: [],
	/** @type {object} */
	extensions: {},
	/** @type {Extension[]} */
	extensionsArray: [],
	/** @type {object} */
	features: {},
	/** @type {FeatureModel[]} */
	featuresArray: [],
	/** @type {object} */
	pawns: {},
	/** @type {PlayerPawnModel[]} */
	pawnsArray: [],
	/** @type {object} */
	neutralPawns: {},
	/** @type {NeutralPawnModel[]} */
	neutralPawnsArray: [],
	/** @type {object} */
	tokens: {},
	/** @type {TokenModel[]} */
	tokensArray: [],
	/** @type {object} */
	tiles: {},
	/** @type {TileModel[]} */
	tilesArray: [],
	/** @type {object} */
	messages: {},
	/** @type {MessageModel[]} */
	messagesArray: [],
	/** @type {object} */
	boardEvents: {},
	/** @type {BoardEventModel[]} */
	boardEventsArray: [],
	/**
	 * @param {string} code
	 * @returns {FeatureModel|null}
	 */
	getFeatureByCode(code) {
		if (this.features[code]) {
			return this.features[code];
		}
		console.error('[Models.getFeatureByCode] Unknown feature model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {PlayerPawnModel|null}
	 */
	getPawnByCode(code) {
		if (this.pawns[code]) {
			return this.pawns[code];
		}
		console.error('[Models.getPawnByCode] Unknown pawn model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {NeutralPawnModel|null}
	 */
	getNeutralPawnByCode(code) {
		if (this.neutralPawns[code]) {
			return this.neutralPawns[code];
		}
		console.error('[Models.getNeutralPawnByCode] Unknown neutral pawn model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {TokenModel|null}
	 */
	getTokenByCode(code) {
		if (this.tokens[code]) {
			return this.tokens[code];
		}
		console.error('[Models.getTokenByCode] Unknown token model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {TileModel|null}
	 */
	getTileByCode(code) {
		if (this.tiles[code]) {
			return this.tiles[code];
		}
		console.error('[Models.getTileByCode] Unknown tile model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {TileModel|null}
	 */
	getMessageByCode(code) {
		if (this.messages[code]) {
			return this.messages[code];
		}
		console.error('[Models.getMessageByCode] Unknown message model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {TileModel|null}
	 */
	getBoardEventByCode(code) {
		if (this.boardEvents[code]) {
			return this.boardEvents[code];
		}
		console.error('[Models.getBoardEventByCode] Unknown board event model', code);
		return null;
	}
};

/**
 * Set a tile to show the detail modal.
 * @type {Object}
 */
Models.Details = {
	tile: null,
	message: null,
	boardEvent: null
};

//region Event listeners on game events.
document.addEventListener('showMessageModal', (event) => {
	let messageCode = event.detail.messageCode;
	if (messageCode) {
		Models.Details.message = Models.getMessageByCode(messageCode);
	}
});

document.addEventListener('showBoardEventModal', (event) => {
	let code = event.detail.boardEventCode;
	if (code) {
		Models.Details.boardEvent = Models.getBoardEventByCode(code);
	}
});
//endregion