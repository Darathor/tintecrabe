/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

let document = window.document;
import { BoxDirectory, PlayerDirectory } from "./directories.mjs";
import { Player } from "./players.mjs";
import { Strategies } from "./strategies.mjs";
import { Models } from "./models.mjs";
import { Tile } from "./tiles.mjs";
import { Alerts, ArrayUtils, Coordinates, Timeline } from "./core.mjs";
import { Sounds } from "./sounds.mjs";
import { Preferences } from "./preferences.mjs";

/**
 * @typedef {Object} SerializedPlacementSuggestion
 * @property {CoordinatesData} coordinates
 * @property {SerializedTile} tile
 * @property {string} authorName
 */

export class PlacementSuggestion {
	//region Static stuff.
	static instances = [];

	/**
	 * @param {PlacementSuggestion} newSuggestion
	 */
	static add(newSuggestion) {
		PlacementSuggestion.instances.forEach((suggestion) => {
			if (suggestion.author === newSuggestion.author || suggestion.box === newSuggestion.box) {
				PlacementSuggestion.remove(suggestion);
			}
		});
		PlacementSuggestion.instances.push(newSuggestion);
		newSuggestion.box.placementSuggestion = newSuggestion;
		Timeline.log('c.timeline.tile_placement_suggestion', 'suggestion', newSuggestion.box.coordinates, null, newSuggestion.author.name);
	}

	/**
	 * @param {PlacementSuggestion} suggestion
	 */
	static remove(suggestion) {
		ArrayUtils.remove(PlacementSuggestion.instances, suggestion);
		suggestion.box.placementSuggestion = null;
	}

	static clear() {
		PlacementSuggestion.instances.forEach((suggestion) => {
			suggestion.box.placementSuggestion = null;
		});
		PlacementSuggestion.instances.length = 0;
	}

	/**
	 * @param {SerializedPlacementSuggestion} data
	 * @returns {PlacementSuggestion}
	 */
	static fromData(data) {
		return new PlacementSuggestion(
			new Coordinates(data.coordinates.x, data.coordinates.y), Tile.fromData(data.tile), PlayerDirectory.getByName(data.authorName)
		);
	}

	//endregion

	/**
	 * @param {Coordinates} coordinates
	 * @param {Tile} tile
	 * @param {Player} author
	 */
	constructor(coordinates, tile, author) {
		this.coordinates = coordinates;
		this.tile = Tile.fromData(tile.data);
		this.author = author;
	}

	/**
	 * @returns {Box}
	 */
	get box() {
		return BoxDirectory.getByCoordinates(this.coordinates);
	}

	/**
	 * @returns {string}
	 */
	get cssColor() {
		return this.author.color.cssCode;
	}

	/**
	 * @returns {SerializedPlacementSuggestion}
	 */
	get data() {
		return {
			coordinates: this.coordinates.toJSON(),
			tile: this.tile.data,
			authorName: this.author.name
		};
	}
}

/**
 * @typedef {Object} SerializedBox
 * @property {CoordinatesData} coordinates
 * @property {SerializedTile|null} tile
 */

export class Box {
	/** @type {Tile|null} */
	tile;
	/** @type {boolean} */
	isEmpty;
	/** @type {string} */
	cssClasses;
	/** @type {Feature[]} */
	availableFeatures = [];
	/** @type {Feature[]} */
	occupiedFeatures = [];
	/** @type {PlacementSuggestion|null} */
	internalPlacementSuggestion;

	/**
	 * @param {number} x
	 * @param {number} y
	 */
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.tile = null;
		this.isAccessible = false;
		this.hasPreview = false;
		this.isPreviewValid = false;
		this.setEmpty();
		this.setAccessible(false);
	}

	/**
	 * @returns {PlacementSuggestion|null}
	 */
	get placementSuggestion() {
		return this.internalPlacementSuggestion;
	}

	/**
	 * @param {PlacementSuggestion|null} suggestion
	 */
	set placementSuggestion(suggestion) {
		this.internalPlacementSuggestion = suggestion;
		this.refreshCssClasses();
	}

	/**
	 * @returns {Coordinates}
	 */
	get coordinates() {
		return new Coordinates(this.x, this.y);
	}

	/**
	 * @returns {string}
	 */
	get elementId() {
		return 'box_' + this.x + '_' + this.y;
	}

	scrollIntoView() {
		let element = document.getElementById(this.elementId);
		if (element) {
			element.scrollIntoView({ block: 'center', inline: 'center', behavior: 'smooth' });
		}
	}

	/**
	 * @returns {Box[]}
	 */
	get neighbours() {
		let neighbours = [];
		let coordinates = [[this.x - 1, this.y], [this.x + 1, this.y], [this.x, this.y - 1], [this.x, this.y + 1]];
		for (let i = 0; i < coordinates.length; i++) {
			let neighbour = BoxDirectory.get(Board.getRealX(coordinates[i][0]), Board.getRealY(coordinates[i][1]));
			if (neighbour) {
				neighbours.push(neighbour);
			}
		}
		return neighbours;
	}

	/**
	 * @param {string|number} direction
	 * @returns {Box|null}
	 */
	getNeighbour(direction) {
		if (direction === 'top' || direction === 0) {
			return BoxDirectory.get(Board.getRealX(this.x), Board.getRealY(this.y - 1));
		}
		else if (direction === 'right' || direction === 1) {
			return BoxDirectory.get(Board.getRealX(this.x + 1), Board.getRealY(this.y));
		}
		else if (direction === 'bottom' || direction === 2) {
			return BoxDirectory.get(Board.getRealX(this.x), Board.getRealY(this.y + 1));
		}
		else if (direction === 'left' || direction === 3) {
			return BoxDirectory.get(Board.getRealX(this.x - 1), Board.getRealY(this.y));
		}
		console.error('[Box::getNeighbour] Invalid direction:', direction);
		return null;
	}

	/**
	 * @param {string|number} direction
	 * @param {number} index
	 * @returns {Feature|null}
	 */
	getRelatedFeature(direction, index) {
		let neighbour = this.getNeighbour(direction);
		if (!neighbour || neighbour.isEmpty) {
			return null;
		}
		return neighbour.tile.getOppositeFeature(direction, index);
	}

	/**
	 * @param {Tile} tile
	 */
	setTile(tile) {
		this.removePreview();
		this.tile = tile;
		this.setFull();
	}

	removeTile() {
		this.removePreview();
		this.tile = null;
		this.setEmpty();
	}

	setEmpty() {
		this.isEmpty = true;
		this.availableFeatures = [];
		this.occupiedFeatures = [];
		this.refreshCssClasses();
	}

	setFull() {
		this.isEmpty = false;
		this.refreshCssClasses();
	}

	refreshCssClasses() {
		let classes = ['box'];
		classes.push(this.isEmpty ? 'empty' : 'full');
		if (this.isAccessible) {
			classes.push('accessible');
		}
		if (this.hasPreview) {
			classes.push('preview');
			classes.push(this.isPreviewValid ? 'preview-valid' : 'preview-invalid');
		}
		else if (this.placementSuggestion) {
			classes.push('suggestion');
		}
		this.cssClasses = classes.join(' ');
	}

	/**
	 * @param {boolean} value
	 */
	setAccessible(value) {
		if (value) {
			this.isAccessible = true;
			this.refreshCssClasses();
		}
		else {
			this.isAccessible = false;
			this.refreshCssClasses();
		}
	}

	/**
	 * @param {Tile} tile
	 * @param {boolean} valid
	 */
	setPreview(tile, valid) {
		this.hasPreview = true;
		this.previewTile = tile;
		this.isPreviewValid = valid;
		this.refreshCssClasses();
	}

	/**
	 * @param {boolean} valid
	 */
	refreshPreview(valid) {
		this.isPreviewValid = valid;
		this.refreshCssClasses();
	}

	removePreview() {
		if (this.hasPreview) {
			this.hasPreview = false;
			this.previewTile = null;
			this.refreshCssClasses();
		}
	}

	/**
	 * @param {boolean=} refreshNeighbours
	 */
	refreshAccessibility(refreshNeighbours) {
		let neighbours = (this.isEmpty || refreshNeighbours) ? this.neighbours : [];

		this.isAccessible = false;
		if (this.isEmpty) {
			for (let i = 0; i < neighbours.length; i++) {
				if (neighbours[i].tile) {
					this.isAccessible = true;
				}
			}
		}
		this.refreshCssClasses();

		// Update neighbours accessibility.
		if (refreshNeighbours) {
			for (let i = 0; i < neighbours.length; i++) {
				neighbours[i].refreshAccessibility();
			}
		}
	}

	/**
	 * @param {Player} currentPlayer
	 * @param {Game} game
	 */
	showFeatures(currentPlayer, game) {
		this.availableFeatures = [];
		this.tile.features.forEach((feature) => {
			if (feature.isPlayable(currentPlayer, this, game)) {
				this.availableFeatures.push(feature);
			}
		});
	}

	hideFeatures() {
		this.availableFeatures = [];
	}

	refreshPawns() {
		this.occupiedFeatures = [];
		this.tile.features.forEach((feature) => {
			if (!feature.isFree) {
				this.occupiedFeatures.push(feature);
			}
		});
	}

	/**
	 * @param {Tile} tile
	 */
	onEnter(tile) {
		// Accessible empty box case.
		if (this.isEmpty && this.isAccessible && tile) {
			this.setPreview(tile, this.canBePlaced(tile));
		}
	}

	onLeave() {
		if (this.hasPreview) {
			this.removePreview();
		}
	}

	/**
	 * @param {Tile} tile
	 * @returns {boolean}
	 */
	canBePlaced(tile) {
		if (!tile) {
			return false;
		}
		let coordinates = [[this.x - 1, this.y, 'left'], [this.x + 1, this.y, 'right'], [this.x, this.y - 1, 'top'], [this.x, this.y + 1, 'bottom']];
		for (let i = 0; i < coordinates.length; i++) {
			let data = coordinates[i];
			let neighbour = BoxDirectory.get(Board.getRealX(data[0]), Board.getRealY(data[1]));
			if (neighbour && neighbour.tile && !tile.isCompatible(neighbour.tile, data[2])) {
				return false;
			}
		}

		let conditionsVerified = true;
		tile.features.forEach((feature) => {
			feature.model.placementConstraints.forEach((constraintData) => {
				let constraint = Strategies.getPlacementConstraints(constraintData.type);
				if (!constraint.evaluate(feature, tile, this)) {
					conditionsVerified = false;
				}
			});
		});

		return conditionsVerified;
	}

	/**
	 * @returns {Array[]}
	 */
	get neighbourFeatures() {
		if (this.isEmpty) {
			return [];
		}
		let neighbourFeatures = [];
		let coordinates = [[this.x - 1, this.y, 'right'], [this.x + 1, this.y, 'left'], [this.x, this.y - 1, 'bottom'], [this.x, this.y + 1, 'top']];
		coordinates.forEach((data) => {
			let neighbour = BoxDirectory.get(Board.getRealX(data[0]), Board.getRealY(data[1]));
			if (neighbour && neighbour.tile) {
				neighbour.tile.getSide(data[2]).forEach((sideFeature) => {
					neighbourFeatures.push([sideFeature, neighbour]);
				});
			}
		});
		return neighbourFeatures;
	}

	/**
	 * @returns {SerializedBox}
	 */
	get data() {
		return {
			coordinates: this.coordinates.toJSON(),
			tile: this.tile ? this.tile.data : null
		};
	}

	/**
	 * @param {SerializedBox} data
	 */
	set data(data) {
		if (this.x !== data.coordinates.x || this.y !== data.coordinates.y) {
			console.error('[Box.set data] Invalid box coordinates!', this, data);
		}

		if (this.tile) {
			this.tile.free();
			this.removeTile();
		}

		if (data.tile) {
			let tile = new Tile(Models.getTileByCode(data.tile.code));
			tile.data = data.tile;
			this.setTile(tile);
			this.refreshPawns();
		}
	}

	//region DOM event listeners.
	onMouseEnter() {
		document.dispatchEvent(new CustomEvent('enterBox', { detail: { box: this } }));
	}

	onMouseLeave() {
		document.dispatchEvent(new CustomEvent('leaveBox', { detail: { box: this } }));
	}

	onPreviewRotateLeftClick() {
		this.previewTile.rotateLeft();
		this.refreshPreview(this.canBePlaced(this.previewTile));
	}

	onPreviewRotateRightClick() {
		this.previewTile.rotateRight();
		this.refreshPreview(this.canBePlaced(this.previewTile));
	}

	onPreviewSetOrientation(orientation) {
		this.previewTile.setOrientation(orientation);
		this.refreshPreview(this.canBePlaced(this.previewTile));
	}

	/**
	 * @param {boolean} ctrlPressed
	 */
	onPlayPreviewTile(ctrlPressed) {
		if (this.canBePlaced(this.previewTile)) {
			document.dispatchEvent(new CustomEvent('playCurrentTile', { detail: { box: this, ctrlPressed: !!ctrlPressed } }));
		}
	}

	/**
	 * @param {Feature} feature
	 */
	onPlayPawnOnFeature(feature) {
		document.dispatchEvent(new CustomEvent('playCurrentPawn', { detail: { box: this, feature: feature } }));
	}

	//endregion
}

/**
 * @typedef {Object} SerializedBoard
 * @property {string} horizontalType
 * @property {string} verticalType
 * @property {SerializedBox[]} boxes
 * @property {BoardLimits} limits
 * @property {CoordinatesData=} _lastPlayedBox
 */

/**
 * @typedef {Object} BoardLimits
 * @property {number} top
 * @property {number|null} maxTop
 * @property {number} right
 * @property {number|null} maxRight
 * @property {number} bottom
 * @property {number|null} maxBottom
 * @property {number} left
 * @property {number|null} maxLeft
 */

export let Board = Vue.observable({
	horizontalType: 'infinite',
	verticalType: 'infinite',
	limits: {
		top: 0,
		maxTop: null,
		right: 0,
		maxRight: null,
		bottom: 0,
		maxBottom: null,
		left: 0,
		maxLeft: null
	},
	rows: [0],
	columns: [0],
	/** @type {Box|null} */
	highlightedBox: null,
	/** @type {number|null} */
	highlightedBoxTimeout: null,

	/**
	 * @returns {string}
	 */
	get limitsHash() {
		return Board.limits.top + ';' + Board.limits.bottom + ';' + Board.limits.left + ';' + Board.limits.right;
	},
	/**
	 * @returns {number}
	 */
	get realTop() {
		return !this.loopsVertically() ?  this.limits.top : Math.max(this.limits.maxTop, this.limits.top);
	},
	/**
	 * @returns {number}
	 */
	get realBottom() {
		return !this.loopsVertically() ?  this.limits.bottom : Math.min(this.limits.maxBottom, this.limits.bottom);
	},
	/**
	 * @returns {number}
	 */
	get realLeft() {
		return !this.loopsHorizontally() ?  this.limits.left : Math.max(this.limits.maxLeft, this.limits.left);
	},
	/**
	 * @returns {number}
	 */
	get realRight() {
		return !this.loopsHorizontally() ?  this.limits.right : Math.min(this.limits.maxRight, this.limits.right);
	},

	/**
	 * @returns {Box}
	 */
	initFirstBox() {
		return this.initBox(0, 0);
	},
	/**
	 * @param {string} code
	 * @returns {Box[]}
	 */
	getBoxesByTileCode(code) {
		let result = [];
		this.getNonEmptyBoxes().forEach((box) => {
			if (box.tile.code === code) {
				result.push(box);
			}
		});
		return result;
	},
	/**
	 * @param {string} horizontalType
	 * @param {number|null} horizontalSize
	 * @param {string} verticalType
	 * @param {number|null} verticalSize
	 */
	initLimits(horizontalType, horizontalSize, verticalType, verticalSize) {
		this.horizontalType = horizontalType;
		let horizontalLimit = (horizontalType !== 'infinite') ? (horizontalSize || 10) : null;
		if (horizontalLimit) {
			this.limits.maxLeft = -horizontalLimit;
			this.limits.maxRight = horizontalLimit;
		}

		this.verticalType = verticalType;
		let verticalLimit = (verticalType !== 'infinite') ? (verticalSize || 10) : null;
		if (verticalLimit) {
			this.limits.maxTop = -verticalLimit;
			this.limits.maxBottom = verticalLimit;
		}
	},
	/**
	 * @param {number} horizontalSize
	 * @param {number} verticalSize
	 */
	initBoxes(horizontalSize, verticalSize) {
		for (let i = this.limits.left; i > -horizontalSize; i--) {
			this.expandLeft(true);
		}
		for (let i = this.limits.right; i < horizontalSize; i++) {
			this.expandRight(true);
		}
		for (let i = this.limits.top; i > -verticalSize; i--) {
			this.expandTop(true);
		}
		for (let i = this.limits.bottom; i < verticalSize; i++) {
			this.expandBottom(true);
		}
	},
	/**
	 * @returns {boolean}
	 */
	loopsHorizontally() {
		return this.horizontalType === 'finite-without-edges';
	},
	/**
	 * @returns {boolean}
	 */
	loopsVertically() {
		return this.verticalType === 'finite-without-edges';
	},
	getRealCoordinate(value, min, max) {
		let length = 1 + max - min;
		value -= min;
		while (value < 0) {
			value += length;
		}
		value %= length;
		value += min;
		return value;
	},
	/**
	 * @param {number} x
	 * @returns {number}
	 */
	getRealX(x) {
		if (!this.loopsHorizontally()) {
			return x;
		}
		return this.getRealCoordinate(x, this.limits.maxLeft, this.limits.maxRight);
	},
	/**
	 * @param {number} y
	 * @returns {number}
	 */
	getRealY(y) {
		if (!this.loopsVertically()) {
			return y;
		}
		return this.getRealCoordinate(y, this.limits.maxTop, this.limits.maxBottom);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box|null}
	 */
	getRealBox(x, y) {
		return BoxDirectory.get(this.getRealX(x), this.getRealY(y));
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {string}
	 */
	getLoopClass(x, y) {
		// If this is the real position of the box, no loop class.
		if (this.getRealX(x) === x && this.getRealY(y) === y) {
			return '';
		}

		// Else the loop class number depends on the distance to the "real" board limits.
		let distance = 0;
		if (this.loopsHorizontally()) {
			if (x < this.limits.maxLeft) {
				distance = Math.max(distance, this.limits.maxLeft - x);
			}
			if (x > this.limits.maxRight) {
				distance = Math.max(distance, x - this.limits.maxRight);
			}
		}
		if (this.loopsVertically()) {
			if (y < this.limits.maxTop) {
				distance = Math.max(distance, this.limits.maxTop - y);
			}
			if (y > this.limits.maxBottom) {
				distance = Math.max(distance, y - this.limits.maxBottom);
			}
		}
		return 'loop-' + distance;
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {boolean}
	 */
	isValidCoordinates(x, y) {
		x = this.getRealX(x);
		y = this.getRealY(y);
		return x >= this.limits.maxLeft && x <= this.limits.maxRight && y >= this.limits.maxTop && y <= this.limits.maxBottom;
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box}
	 */
	initBox(x, y) {
		x = this.getRealX(x);
		y = this.getRealY(y);
		let box = BoxDirectory.get(x, y);
		if (box) {
			// The box already exists.
			return box;
		}

		// The box doesn't exist, let's create it.
		box = new Box(x, y);
		BoxDirectory.add(box);

		// Set cell classes.
		box.refreshAccessibility();
		return box;
	},
	initMissingBoxes() {
		for (let i = this.realLeft; i <= this.realRight; i++) {
			for (let j = this.realTop; j <= this.realBottom; j++) {
				Board.initBox(i, j);
			}
		}
	},
	/**
	 * @returns {boolean}
	 */
	canExpandTop() {
		return !this.limits.maxTop || this.limits.top > (this.limits.maxTop - (this.loopsVertically() ? 3 : 0));
	},
	/**
	 * @param {boolean} initBoxes
	 * @returns {boolean}
	 */
	expandTop(initBoxes) {
		if (!this.canExpandTop()) {
			return false;
		}

		this.limits.top--;
		this.rows.splice(0, 0, this.limits.top);
		if (initBoxes) {
			for (let x = this.realLeft; x <= this.realRight; x++) {
				this.initBox(x, this.realTop);
			}
		}
		return true;
	},
	/**
	 * @returns {boolean}
	 */
	canExpandBottom() {
		return !this.limits.maxBottom || this.limits.bottom < (this.limits.maxBottom + (this.loopsVertically() ? 3 : 0));
	},
	/**
	 * @param {boolean} initBoxes
	 * @returns {boolean}
	 */
	expandBottom(initBoxes) {
		if (!this.canExpandBottom()) {
			return false;
		}

		this.limits.bottom++;
		this.rows.push(this.limits.bottom);
		if (initBoxes) {
			for (let x = this.realLeft; x <= this.realRight; x++) {
				this.initBox(x, this.realBottom);
			}
		}
		return true;
	},
	/**
	 * @returns {boolean}
	 */
	canExpandLeft() {
		return !this.limits.maxLeft || this.limits.left > (this.limits.maxLeft - (this.loopsHorizontally() ? 3 : 0));
	},
	/**
	 * @param {boolean} initBoxes
	 * @returns {boolean}
	 */
	expandLeft(initBoxes) {
		if (!this.canExpandLeft()) {
			return false;
		}

		this.limits.left--;
		this.columns.splice(0, 0, this.limits.left);
		if (initBoxes) {
			for (let y = this.realTop; y <= this.realBottom; y++) {
				this.initBox(this.realLeft, y);
			}
		}
		return true;
	},
	/**
	 * @returns {boolean}
	 */
	canExpandRight() {
		return !this.limits.maxRight || this.limits.right < (this.limits.maxRight + (this.loopsHorizontally() ? 3 : 0));
	},
	/**
	 * @param {boolean} initBoxes
	 * @returns {boolean}
	 */
	expandRight(initBoxes) {
		if (!this.canExpandRight()) {
			return false;
		}

		this.limits.right++;
		this.columns.push(this.limits.right);
		if (initBoxes) {
			for (let y = this.realTop; y <= this.realBottom; y++) {
				this.initBox(this.realRight, y);
			}
		}
		return true;
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 */
	expand(x, y) {
		for (let i = 0; i < 3; i++) {
			if (x === this.limits.left + i) {
				this.expandLeft(true);
			}
			if (x === this.limits.right - i) {
				this.expandRight(true);
			}
			if (y === this.limits.top + i) {
				this.expandTop(true);
			}
			if (y === this.limits.bottom - i) {
				this.expandBottom(true);
			}
		}
	},
	/**
	 * @param {Box} box
	 * @param {Tile} tile
	 */
	putTile(box, tile) {
		box.setTile(tile);
		box.refreshAccessibility(true);

		// Expand board.
		this.expand(box.x, box.y);
	},
	/**
	 * @param {Box} box
	 */
	removeTile(box) {
		box.removeTile();
		box.refreshAccessibility(true);
	},
	/**
	 * @param {Box[]=} ignoredBoxes
	 * @returns {Box[]}
	 */
	getEmptyBoxes(ignoredBoxes) {
		if (!ignoredBoxes) {
			ignoredBoxes = [];
		}
		let boxes = [];
		for (let i = this.realLeft; i <= this.realRight; i++) {
			for (let j = this.realTop; j <= this.realBottom; j++) {
				let box = BoxDirectory.get(i, j);
				if (box && box.isEmpty && !ArrayUtils.contains(ignoredBoxes, box)) {
					boxes.push(box);
				}
			}
		}
		return boxes;
	},
	/**
	 * @param {{x: number, y: number}[]} [ignoredCoordinates=[]]
	 * @returns {Box[]}
	 */
	getNonEmptyBoxes(ignoredCoordinates) {
		if (!ignoredCoordinates) {
			ignoredCoordinates = [];
		}

		let boxes = [];
		for (let i = this.realLeft; i <= this.realRight; i++) {
			for (let j = this.realTop; j <= this.realBottom; j++) {
				let box = BoxDirectory.get(i, j);
				if (!box) {
					continue;
				}

				let found = false;
				ignoredCoordinates.forEach((item) => {
					if (item.x === box.x && item.y === box.y) {
						found = true;
					}
				});

				if (!box.isEmpty && !found) {
					boxes.push(box);
				}
			}
		}
		return boxes;
	},
	/**
	 * @param {Coordinates} coordinates
	 */
	centerOnCoordinates(coordinates) {
		let box = Board.getRealBox(coordinates.x, coordinates.y);
		if (box) {
			box.scrollIntoView();
			this.highlightedBox = box;

			if (this.highlightedBoxTimeout) {
				clearTimeout(this.highlightedBoxTimeout);
			}

			this.highlightedBoxTimeout = setTimeout(() => {
				this.highlightedBox = null;
			}, 1000);
		}
		else {
			let options = {
				icon: 'images/icons/warning.svg'
			};
			if (Preferences.getInstance().playSounds) {
				options.sound = Sounds.newAlert;
			}
			Alerts.warning('c.main.alert_outside_board', options);
		}
	},

	/**
	 * @returns {SerializedBoard|null}
	 */
	get data() {
		let data = {
			horizontalType: this.horizontalType,
			verticalType: this.verticalType,
			limits: this.limits,
			boxes: []
		};

		this.getNonEmptyBoxes().forEach((box) => {
			data.boxes.push(box.data);
		});

		return data;
	},
	/**
	 * @param {SerializedBoard|null} data
	 */
	set data(data) {
		if (!data) {
			return;
		}

		// Clear the boxes.
		this.getNonEmptyBoxes().forEach((box) => {
			box.tile.free();
			box.removeTile();
		});

		// Limits.
		if (!BoxDirectory.get(0, 0)) {
			this.initFirstBox();
		}

		this.horizontalType = data.horizontalType || 'infinite';
		this.verticalType = data.verticalType || 'infinite';

		this.limits.maxTop = data.limits.maxTop || null;
		while (this.canExpandTop() && this.limits.top > data.limits.top) {
			this.expandTop(true);
		}
		this.limits.maxRight = data.limits.maxRight || null;
		while (this.canExpandRight() && this.limits.right < data.limits.right) {
			this.expandRight(true);
		}
		this.limits.maxBottom = data.limits.maxBottom || null;
		while (this.canExpandBottom() && this.limits.bottom < data.limits.bottom) {
			this.expandBottom(true);
		}
		this.limits.maxLeft = data.limits.maxLeft || null;
		while (this.canExpandLeft() && this.limits.left > data.limits.left) {
			this.expandLeft(true);
		}

		// Add the tiles.
		data.boxes.forEach((boxData) => {
			let box = BoxDirectory.getByCoordinates(boxData.coordinates);
			if (box) {
				box.data = boxData;
			}
			else {
				console.error('[Board.set data] Box not found:', boxData, this.limits);
			}
		});

		// Refresh the accessibility.
		this.getEmptyBoxes().forEach((box) => {
			box.refreshAccessibility();
		});

		// Register incomplete features.
		Strategies.refreshRegistrations(this.getNonEmptyBoxes(data._lastPlayedBox ? [data._lastPlayedBox] : []));
	},
	_debug_checkMissingBoxes() {
		for (let j = this.realTop; j <= this.realBottom; j++) {
			for (let i = this.realLeft; i <= this.realRight; i++) {
				if (!BoxDirectory.instances[i + '_' + j]) {
					console.warn('missing', i + '_' + j);
				}
			}
		}
	}
});

export class Position {
	/**
	 * @param {number} x
	 * @param {number} y
	 */
	constructor(x, y) {
		/** @type {number} */
		this.x = Board.getRealX(x);
		/** @type {number} */
		this.y = Board.getRealY(y);
	}

	/**
	 * @returns {Box|null}
	 */
	get box() {
		return BoxDirectory.getByCoordinates(this);
	}

	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {boolean}
	 */
	is(x, y) {
		return this.x === Board.getRealX(x) && this.y === Board.getRealY(y);
	}
}

export class BoardZone {
	/**
	 * @param {Position[]} positions
	 */
	constructor(positions) {
		/** @type {Position[]} */
		this.positions = [];
		/** @type {Object} */
		this.coordinates = {};

		positions.forEach((position) => {
			let x = position.x + '';
			let y = position.y + '';
			if (!(x in this.coordinates)) {
				this.coordinates[x] = {};
			}
			if (!(y in this.coordinates[x])) { // No doubles.
				this.coordinates[x][y] = true;
				this.positions.push(position);
			}
		});
	}

	/**
	 * @returns {Box[]}
	 */
	getNonEmptyBoxes() {
		let boxes = [];
		for (let i = 0; i < this.positions.length; i++) {
			let box = this.positions[i].box;
			if (box && !box.isEmpty) {
				boxes.push(box);
			}
		}
		return boxes;
	}

	/**
	 * @returns {boolean}
	 */
	isFull() {
		for (let i = 0; i < this.positions.length; i++) {
			let box = this.positions[i].box;
			if (!box || box.isEmpty) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {boolean}
	 */
	contains(x, y) {
		return this.coordinates[x + ''] && this.coordinates[x + ''][y + ''] === true;
	}
}

export let Zones = {
	/**
	 * @param {ZoneData} data
	 * @param x
	 * @param y
	 */
	getZone(data, x, y) {
		switch (data.type) {
			case 'square':
				return this.getSquare(x, y, data.size);
			case 'diamond':
				return this.getDiamond(x, y, data.size);
			case 'cross':
				return this.getCross(x, y, data.size);
			case 'ix':
				return this.getIx(x, y, data.size);
			case 'star':
				return this.getStar(x, y, data.size);
			default:
				console.warn('[Zones.getZone] Unknown zone type:', data.type);
		}
		return new BoardZone([]);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getSquare(x, y, length) {
		let positions = [];
		for (let i = -length; i <= length; i++) {
			for (let j = -length; j <= length; j++) {
				positions.push(new Position(x + i, y + j));
			}
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getDiamond(x, y, length) {
		let positions = [];
		for (let i = -length; i <= length; i++) {
			let absI = Math.abs(i);
			for (let j = absI - length; j <= length - absI; j++) {
				positions.push(new Position(x - i, y - j));
			}
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getCross(x, y, length) {
		let positions = [];
		// Horizontally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y));
		}
		// Vertically.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x, y + i));
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getIx(x, y, length) {
		let positions = [];
		// Diagonally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y + i));
			positions.push(new Position(x + i, y - i));
			positions.push(new Position(x - i, y + i));
			positions.push(new Position(x - i, y - i));
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getStar(x, y, length) {
		let positions = [];
		// Horizontally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y));
		}
		// Vertically.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x, y + i));
		}
		// Diagonally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y + i));
			positions.push(new Position(x + i, y - i));
			positions.push(new Position(x - i, y + i));
			positions.push(new Position(x - i, y - i));
		}
		return new BoardZone(positions);
	}
};