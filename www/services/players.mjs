/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, Timeline, I18n, TimelineItemTrigger } from './core.mjs';
import { Models } from './models.mjs';
import { Structure } from './strategies.mjs';
import { PlayerPawn } from './pawns.mjs';
import { PlayerDirectory } from './directories.mjs';
import { Tile } from './tiles.mjs';

export class Color {
	/**
	 * @returns {Color|null}
	 */
	static getByCode(code) {
		for (let key in Colors) {
			if (Colors[key].code === code) {
				return Colors[key];
			}
		}
		return null;
	}

	/**
	 * @param {string} code
	 * @param {string} cssCode
	 * @param {string} cssSecondary
	 */
	constructor(code, cssCode, cssSecondary) {
		this.code = code;
		this.cssCode = cssCode
		this.cssSecondary = cssSecondary;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('color_' + this.code);
	}

	/**
	 * @param {Player[]} players
	 * @param {Player=} ignorePlayer
	 */
	isAvailable(players, ignorePlayer) {
		let available = true;
		players.forEach((player) => {
			if (player.color === this && ignorePlayer !== player) {
				available = false;
			}
		});
		return available;
	}
}

export let Colors = {
	black: new Color('black', 'black', '#fff'),
	blue: new Color('blue', '#22e', '#fff'),
	green: new Color('green', 'green', '#fff'),
	red: new Color('red', '#dc143c', '#000'),
	grey: new Color('grey', 'grey', '#000'),
	violet: new Color('violet', '#9932cc', '#fff'),
	orange: new Color('orange', '#ff8c00', '#000'),
	pink: new Color('pink', '#ffc0cb', '#000'),
	white: new Color('white', 'white', '#000'),
	yellow: new Color('yellow', 'yellow', '#000')
};

/**
 * @typedef {Object} SerializedPlayer
 * @property {string} name
 * @property {string} color - The color code.
 * @property {string} peerId
 * @property {number} points
 * @property {string[]} pawns - The pawns codes.
 * @property {object} tokens - The token quantities by code.
 * @property {string[]} tiles - The hand tiles codes.
 */

export class Player {
	//region Static stuff.
	/**
	 * @param {SerializedPlayer} data
	 * @returns {Player}
	 */
	static fromData(data) {
		let player = new Player();
		player.data = data;
		return player;
	}

	//endregion

	/**
	 * @param {string=} name
	 * @param {Color=} color
	 * @param {string=} peerId
	 */
	constructor(name, color, peerId) {
		this.setNameAndPeerId(name, peerId);
		this.color = color;
		this.pawns = {};
		this.tokens = {};
		Models.tokensArray.forEach((model) => {
			this.tokens[model.code] = 0;
		});
		/** @type {Tile[]} */
		this.tiles = [];
		this.points = 0;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		delete this.color;

		Object.keys(this.pawns).forEach((key) => {
			/** @var pawns Pawn[] */
			let pawns = this.pawns[key];
			pawns.forEach((pawn) => {
				pawn.free();
			});
			ArrayUtils.clear(this.pawns[key]);
		});
		this.pawns = {};

		this.tiles.forEach((tile) => {
			tile.free();
		});
		this.tiles = [];

		PlayerDirectory.remove(this);
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return this._name;
	}

	/**
	 * @param {string} name
	 */
	set name(name) {
		PlayerDirectory.remove(this);
		this._name = name;
		PlayerDirectory.add(this);
	}

	/**
	 * @returns {string}
	 */
	get peerId() {
		return this._peerId;
	}

	/**
	 * @param {string|null} peerId
	 */
	set peerId(peerId) {
		PlayerDirectory.remove(this);
		this._peerId = peerId;
		PlayerDirectory.add(this);
	}

	/**
	 * @param {string} name
	 * @param {string|null} peerId
	 */
	setNameAndPeerId(name, peerId) {
		PlayerDirectory.remove(this);
		this._name = name;
		this._peerId = peerId;
		PlayerDirectory.add(this);
	}

	/**
	 * @param {PlayerPawn} pawn
	 */
	addPawn(pawn) {
		if (!this.pawns[pawn.model.code]) {
			this.pawns[pawn.model.code] = [];
		}
		this.pawns[pawn.model.code].push(pawn);
	}

	/**
	 * @param {string} code
	 * @param {number} count
	 */
	addPawns(code, count) {
		let model = Models.getPawnByCode(code);
		for (let i = 0; i < count; i++) {
			this.addPawn(new PlayerPawn(model, this));
		}
	}

	/**
	 * @param {string} code
	 * @return {PlayerPawn}
	 */
	popPawn(code) {
		return this.pawns[code].splice(0, 1)[0];
	}

	/**
	 * @param {string} code
	 * @returns {PlayerPawn[]}
	 */
	getPawnsByCode(code) {
		return this.pawns[code] || [];
	}

	/**
	 * @param {string[]} codes
	 * @param {TimelineItemTrigger} source
	 */
	gainPawns(codes, source) {
		if (codes.length) {
			codes.forEach((code) => {
				let pawn = new PlayerPawn(Models.getPawnByCode(code), this)
				this.addPawn(pawn);
				Timeline.log('c.timeline.player_gains_pawn', 'pawn-gain', null, source, this.name, { pawnKey: pawn.model.nameKey });
			});
		}
	}

	/**
	 * @param {string[]} codes
	 * @param {TimelineItemTrigger} source
	 */
	loosePawns(codes, source) {
		if (codes.length) {
			codes.forEach((code) => {
				let pawn = this.popPawn(code);
				if (pawn) {
					Timeline.log('c.timeline.player_looses_pawn', 'pawn-loose', null, source, this.name, { pawnKey: pawn.model.nameKey });
				}
			});
		}
	}

	/**
	 * @param {string} code
	 */
	addToken(code) {
		this.tokens[code]++;
	}

	/**
	 * @param {string} code
	 */
	removeToken(code) {
		this.tokens[code]--;
	}

	/**
	 * @param {string[]} codes
	 * @param {TimelineItemTrigger} source
	 */
	gainTokens(codes, source) {
		if (codes.length) {
			codes.forEach((code) => {
				this.addToken(code);
				let token = Models.getTokenByCode(code);
				Timeline.log('c.timeline.player_gains_token', 'token-gain', null, source, this.name, { tokenKey: token.nameKey });
			});
		}
	}

	/**
	 * @param {string[]} codes
	 * @param {TimelineItemTrigger} source
	 */
	looseTokens(codes, source) {
		if (codes.length) {
			codes.forEach((code) => {
				this.removeToken(code);
				let token = Models.getTokenByCode(code);
				Timeline.log('c.timeline.player_looses_token', 'token-loose', null, source, this.name, { tokenKey: token.nameKey });
			});
		}
	}

	/**
	 * @param {string} code
	 * @returns {number}
	 */
	getTokenCount(code) {
		return this.tokens[code] || 0;
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	hasToken(code) {
		return this.getTokenCount(code) > 0;
	}

	/**
	 * @returns {object} The token quantities by code.
	 */
	getTokens() {
		return this.tokens;
	}

	/**
	 * @param {PlayerPawnModel} model
	 * @param {Game} game
	 * @returns {boolean}
	 */
	hasPlayablePawnByModel(model, game) {
		let playerPawns = this.getPawnsByCode(model.code);
		if (!playerPawns.length) {
			return false;
		}

		if (game.canPlayMainAction()) {
			return true;
		}

		if (!model.secondCompatible || game.lastPlayedPawns.length > 1) {
			return false;
		}

		if (!game.lastGotBackPawns.length) {
			return true;
		}

		let found = false;
		playerPawns.forEach((pawn) => {
			let available = true;
			game.lastGotBackPawns.forEach((gotBackPawn) => {
				if (gotBackPawn.pawn === pawn) {
					available = false;
				}
			});
			if (available) {
				found = true;
			}
		});
		return found;
	}

	/**
	 * @param {Game} game
	 * @returns {PlayerPawn[]}
	 */
	getDifferentAvailablePawns(game) {
		let pawns = [];
		Models.pawnsArray.forEach((model) => {
			let code = model.code;
			if (this.hasPlayablePawnByModel(model, game)) {
				pawns.push(this.pawns[code][0]);
			}
		});
		return pawns;
	}

	/**
	 * @param {string} type
	 * @param {Game} game
	 * @returns {PlayerPawn[]}
	 */
	getDifferentAvailablePawnsOfType(type, game) {
		let pawns = [];
		this.getDifferentAvailablePawns(game).forEach((pawn) => {
			if (pawn.model.type === type) {
				pawns.push(pawn);
			}
		});
		return pawns;
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 * @returns {PlayerPawn[]}
	 */
	getPlayablePawns(structure, feature, game) {
		if (!feature.isFree) {
			return [];
		}

		let pawns = [];
		if (structure.isOccupied()) {
			if (structure.hasPlayerPawnOfType(this, 'follower')) {
				this.getDifferentAvailablePawnsOfType('helper', game).forEach((pawn) => {
					if (pawn.isPlayable(structure, feature, game)) {
						pawns.push(pawn);
					}
				});
			}
		}
		else {
			this.getDifferentAvailablePawnsOfType('follower', game).forEach((pawn) => {
				if (pawn.isPlayable(structure, feature, game)) {
					pawns.push(pawn);
				}
			});
		}

		this.getDifferentAvailablePawnsOfType('special', game).forEach((pawn) => {
			if (pawn.isPlayable(structure, feature, game)) {
				pawns.push(pawn);
			}
		});

		return pawns;
	}

	/**
	 * @param {Tile} tile
	 */
	addTile(tile) {
		this.tiles.push(tile);
	}

	/**
	 * @param {Tile} tile
	 */
	removeTile(tile) {
		return ArrayUtils.remove(this.tiles, tile);
	}

	/**
	 * @param {Tile} tile
	 * @param {TimelineItemTrigger} source
	 */
	gainTile(tile, source) {
		this.addTile(tile);
		Timeline.log('c.timeline.player_gets_new_tile_for_hand', 'tile-gain', null, source, this.name);
	}

	/**
	 * @param {Tile} tile
	 * @param {TimelineItemTrigger} source
	 */
	looseTile(tile, source) {
		this.removeTile(tile);
		Timeline.log('c.timeline.player_discards_tile_from_hand', 'tile-loose', null, source, this.name);
	}

	/**
	 * @param {number} points
	 * @param {TimelineItemTrigger} source
	 */
	addPoints(points, source) {
		points = Math.ceil(points);
		this.points += points;
		if (points < -1) {
			Timeline.log('c.timeline.player_looses_points', 'point-loose', null, source, this.name, { points: points, abs_points: Math.abs(points) });
		}
		else if (points === -1) {
			Timeline.log('c.timeline.player_looses_point', 'point-loose', null, source, this.name, { points: points, abs_points: Math.abs(points) });
		}
		else if (points === 0) {
			Timeline.log('c.timeline.player_scores_no_point', 'point-none', null, source, this.name, { points: points });
		}
		else if (points === 1) {
			Timeline.log('c.timeline.player_scores_point', 'point-score', null, source, this.name, { points: points });
		}
		else if (points > 1) {
			Timeline.log('c.timeline.player_scores_points', 'point-score', null, source, this.name, { points: points });
		}
	}

	/**
	 * @return {SerializedPlayer}
	 */
	get data() {
		let data = {
			name: this.name,
			color: this.color.code,
			peerId: this.peerId,
			points: this.points,
			tokens: this.tokens,
			pawns: [],
			tiles: []
		};

		Object.keys(this.pawns).forEach((pawnModelCode) => {
			if (this.pawns[pawnModelCode]) {
				this.pawns[pawnModelCode].forEach((pawn) => {
					data.pawns.push(pawn.code);
				});
			}
		});

		this.tiles.forEach((tile) => {
			data.tiles.push(tile.code);
		});

		return data;
	}

	/**
	 * @param {SerializedPlayer} data
	 */
	set data(data) {
		this.setNameAndPeerId(data.name, data.peerId);

		this.color = Colors[data.color];
		this.points = data.points;
		this.tokens = data.tokens;

		this.pawns = {};
		data.pawns.forEach((code) => {
			this.addPawn(new PlayerPawn(Models.getPawnByCode(code), this));
		});

		Models.tokensArray.forEach((model) => {
			this.tokens[model.code] = data.tokens[model.code] || 0;
		});

		this.tiles = [];
		data.tiles.forEach((code) => {
			this.tiles.push(new Tile(Models.getTileByCode(code)))
		});
	}
}