/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

import { Models } from "./models.mjs";
import { Strategies } from "./strategies.mjs";
import { ArrayUtils, TimelineItemTrigger } from "./core.mjs";
import { PawnDirectory, PlayerDirectory } from "./directories.mjs";

export class AbstractPawn {
	static lastId = 0;

	/**
	 * @param {string} type
	 * @param {Player|null} [owner]
	 */
	constructor(type, owner) {
		if (this.constructor === AbstractPawn) {
			throw new TypeError('Abstract class "AbstractPawn" cannot be instantiated directly!');
		}

		/** @type {number} */
		this.id = NeutralPawn.lastId++;
		/** @type {string} */
		this.type = type;
		/** @type {string|null} */
		this.ownerName = owner ? owner.name : null;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		throw new Error('You must implement this method!');
	}

	get owner() {
		return this.ownerName ? PlayerDirectory.getByName(this.ownerName) : null;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		throw new Error('You must implement this method!');
	}

	/**
	 * @returns {string}
	 */
	get code() {
		throw new Error('You must implement this method!');
	}

	/**
	 * @returns {string}
	 */
	get src() {
		throw new Error('You must implement this method!');
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 * @returns never
	 */
	onContinued(structure, feature, game) {
		throw new Error('You must implement this method!');
	}
}

/**
 * @typedef {Object} SerializedPawn
 * @property {string} code
 * @property {string} ownerName
 */

export class PlayerPawn extends AbstractPawn {
	/**
	 * @param {PlayerPawnModel} model
	 * @param {Player|null} [owner]
	 */
	constructor(model, owner) {
		super('player', owner);
		PawnDirectory.add(this);

		this._code = model.code;

		if (owner) {
			/** @type {Color} */
			this.color = owner.color;
			this._src = model.getSrc(this.color.code);
		}
		else {
			console.error('Pawn without owner!');
			// TODO
		}
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		PawnDirectory.remove(this);
		delete this.color;
	}

	/**
	 * @param {SerializedPawn} data
	 * @returns {PlayerPawn}
	 */
	static fromData(data) {
		return new PlayerPawn(Models.getPawnByCode(data.code), PlayerDirectory.getByName(data.ownerName));
	}

	/**
	 * @returns {SerializedPawn}
	 */
	get data() {
		return {
			code: this.code,
			ownerName: this.owner.name
		};
	}

	/**
	 * @returns {string}
	 */
	get name() {
		let name = this.model.name;
		if (this.owner) {
			name += ' (' + this.owner.name + ')';
		}
		return name;
	}

	/**
	 * @returns {string}
	 */
	get code() {
		return this._code
	}

	/**
	 * @returns {PlayerPawnModel}
	 */
	get model() {
		let model = Models.getPawnByCode(this._code);
		if (!model) {
			console.error('[PlayerPawn.model] Unknown code:', this._code);
		}
		return model;
	}

	/**
	 * @returns {string}
	 */
	get src() {
		return this._src;
	}

	convert(code) {
		this._code = code;
		this._src = this.model.getSrc(this.color.code);
	}

	back() {
		this.owner.addPawn(this);
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 * @returns never
	 */
	onContinued(structure, feature, game) {
		this.model.continuation.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], structure, game.currentPlayer, game, this)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = structure.getBoxByPawn(this);
				let source = new TimelineItemTrigger('c.main.source_pawn', box.coordinates, null, { pawnKey: this.model.nameKey });
				strategy.apply(structure, feature, strategyData.configuration || {}, game, this, source);
			}
		});
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 * @returns never
	 */
	onMessageObtained(structure, feature, game) {
		this.model.messageObtained.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], structure, game.currentPlayer, game, this)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = structure.getBoxByPawn(this);
				let source = new TimelineItemTrigger('c.main.source_pawn', box.coordinates, null, { pawnKey: this.model.nameKey });
				strategy.apply(structure, feature, strategyData.configuration || {}, game, this, source);
			}
		});
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 */
	isPlayable(structure, feature, game) {
		if (!ArrayUtils.contains(feature.model.pawns.allowed || [], this.code)) {
			return false;
		}

		if (!Strategies.checkCondition(this.model.placement.conditions || [], structure, game.currentPlayer, game, this)) {
			return false;
		}

		return Strategies.checkCondition(feature.model.pawns.conditions || [], structure, game.currentPlayer, game, this);
	}

	/**
	 * @returns {boolean}
	 */
	hasCompletion() {
		return !!(this.model.completion && this.model.completion.length);
	}

	/**
	 * @returns {boolean}
	 */
	hasCompletionEvaluation() {
		return !!(this.model.evaluationCompleted && this.model.evaluationCompleted.length);
	}

	/**
	 * @returns {boolean}
	 */
	hasEndEvaluation() {
		return !!(this.model.evaluationEnd && this.model.evaluationEnd.length);
	}
}

/**
 * @typedef {Object} SerializedNeutralPawn
 * @property {string} code
 */

export class NeutralPawn extends AbstractPawn {
	/**
	 * @param {NeutralPawnModel} model
	 */
	constructor(model) {
		super('neutral');

		this._code = model.code;
		this._src = model.src;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
	}

	/**
	 * @param {SerializedNeutralPawn} data
	 * @returns {NeutralPawn}
	 */
	static fromData(data) {
		return new NeutralPawn(Models.getNeutralPawnByCode(data.code));
	}

	/**
	 * @returns {SerializedNeutralPawn}
	 */
	get data() {
		return {
			code: this.code
		};
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return this.model.name;
	}

	/**
	 * @returns {string}
	 */
	get code() {
		return this._code
	}

	/**
	 * @returns {NeutralPawnModel}
	 */
	get model() {
		let model = Models.getNeutralPawnByCode(this._code);
		if (!model) {
			console.error('[NeutralPawn.model] Unknown code:', this._code);
		}
		return model;
	}

	/**
	 * @returns {string}
	 */
	get src() {
		return this._src
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Game} game
	 * @returns never
	 */
	onContinued(structure, feature, game) {
		this.model.continuation.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], structure, game.currentPlayer, game, this)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = structure.getBoxByNeutralPawn(this);
				let source = new TimelineItemTrigger('c.main.source_pawn', box.coordinates, null, { pawnKey: this.model.nameKey });
				strategy.apply(structure, feature, strategyData.configuration || {}, game, this, source);
			}
		});
	}
}