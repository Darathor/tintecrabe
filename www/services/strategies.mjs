/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, TimelineItemTrigger } from './core.mjs';
import { DynamicParameter } from './models/dynamic-parameter.mjs';
import { BoxDirectory, FeatureDirectory, PawnDirectory } from "./directories.mjs";
import { PlayerEffect } from "../structures/PlayerEffect.mjs";

/**
 * @typedef {Object} RegisteredFeature
 * @property {Feature} feature
 * @property {Box} box
 */

export let Strategies = {
	conditions: {},
	effects: {},
	boardEffects: {},
	playerEffects: {},
	placementConstraints: {},
	neighbours: {},
	influence: {},
	completion: {},
	evaluation: {},
	registeredFeatures: {},
	registeredFeaturesNeighbours: {},
	registeredFeaturesInfluences: {},
	registeredPawns: {},
	/**
	 * @param {string} code
	 * @returns {ConditionStrategy}
	 */
	getCondition(code) {
		let strategy = this.conditions[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getCondition] Unknown strategy:', code);
		return this.conditions['null'];
	},
	/**
	 * @param {string} code
	 * @returns {EffectStrategy}
	 */
	getEffect(code) {
		let strategy = this.effects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEffect] Unknown strategy:', code);
		return this.effects['null'];
	},
	/**
	 * @param {string} code
	 * @returns {BoardEffectStrategy}
	 */
	getBoardEffect(code) {
		let strategy = this.boardEffects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getBoardEffect] Unknown strategy:', code);
		return this.boardEffects['null'];
	},
	/**
	 * @param {string} code
	 * @returns {PlayerEffectStrategy}
	 */
	getPlayerEffect(code) {
		let strategy = this.playerEffects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getPlayerEffect] Unknown strategy:', code);
		return this.playerEffects['null'];
	},
	/**
	 * @param {string} code
	 * @returns {PlacementConstraintStrategy}
	 */
	getPlacementConstraints(code) {
		let strategy = this.placementConstraints[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getPlacementConstraints] Unknown strategy:', code);
		return this.conditions['null'];
	},
	/**
	 * @param {string} code
	 * @returns {NeighboursStrategy}
	 */
	getNeighbours(code) {
		let strategy = this.neighbours[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getNeighbours] Unknown strategy:', code);
		return this.neighbours['null'];
	},
	/**
	 * @param {string} code
	 * @returns {InfluenceStrategy}
	 */
	getInfluence(code) {
		let strategy = this.influence[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getInfluence] Unknown strategy:', code);
		return this.influence['null'];
	},
	/**
	 * @param {string} code
	 * @returns {CompletionStrategy}
	 */
	getCompletion(code) {
		let strategy = this.completion[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getCompletion] Unknown strategy:', code);
		return this.completion['null'];
	},
	/**
	 * @param {string} code
	 * @returns {EvaluationStrategy}
	 */
	getEvaluation(code) {
		let strategy = this.evaluation[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEvaluation] Unknown strategy:', code);
		return this.evaluation['null'];
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Position[]} positions
	 */
	registerFeature(feature, box, positions) {
		positions.forEach((position) => {
			let key = position.x + 'x' + position.y;
			if (!this.registeredFeatures[key]) {
				this.registeredFeatures[key] = [];
			}
			this.registeredFeatures[key].push([feature.id, box.coordinates]);
		});
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Array}
	 */
	getRegisteredFeatures(x, y) {
		let registeredFeatures = [];
		let key = x + 'x' + y;
		if (this.registeredFeatures[key]) {
			this.registeredFeatures[key].forEach((neighboursData) => {
				registeredFeatures.push([FeatureDirectory.get(neighboursData[0]), BoxDirectory.getByCoordinates(neighboursData[1])]);
			});
		}
		return registeredFeatures;
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Position[]} positions
	 */
	registerFeatureInfluence(feature, box, positions) {
		positions.forEach((position) => {
			let key = position.x + 'x' + position.y;
			if (!this.registeredFeaturesInfluences[key]) {
				this.registeredFeaturesInfluences[key] = [];
			}
			this.registeredFeaturesInfluences[key].push([feature.id, box.coordinates]);
		});
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {RegisteredFeature[]}
	 */
	getRegisteredInfluence(x, y) {
		let registeredFeatures = [];
		let key = x + 'x' + y;
		if (this.registeredFeaturesInfluences[key]) {
			this.registeredFeaturesInfluences[key].forEach((influenceData) => {
				registeredFeatures.push({ feature: FeatureDirectory.get(influenceData[0]), box: BoxDirectory.getByCoordinates(influenceData[1]) });
			});
		}
		return registeredFeatures;
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Feature} neighbour
	 */
	registerFeatureNeighbour(feature, box, neighbour) {
		if (!this.registeredFeaturesNeighbours['f' + neighbour.id]) {
			this.registeredFeaturesNeighbours['f' + neighbour.id] = [];
		}
		this.registeredFeaturesNeighbours['f' + neighbour.id].push([feature.id, box.coordinates]);
	},
	/**
	 * @param {Feature} feature
	 * @returns {Array}
	 */
	getRegisteredNeighbours(feature) {
		let neighbours = [];
		let neighboursData = this.registeredFeaturesNeighbours['f' + feature.id] || [];
		neighboursData.forEach((neighbourData) => {
			neighbours.push([FeatureDirectory.get(neighbourData[0]), BoxDirectory.getByCoordinates(neighbourData[1])]);
		});
		return neighbours;
	},
	/**
	 * @param {PlayerPawn} pawn
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Position[]} positions
	 */
	registerPawn(pawn, feature, box, positions) {
		positions.forEach((position) => {
			let key = position.x + 'x' + position.y;
			if (!this.registeredPawns[key]) {
				this.registeredPawns[key] = [];
			}
			this.registeredPawns[key].push([pawn.id, feature.id, box.coordinates]);
		});
	},
	/**
	 * @param {PlayerPawn} pawn
	 */
	unRegisterPawn(pawn) {
		Object.keys(this.registeredPawns).forEach((key) => {
			this.registeredPawns[key].forEach((registeredPawn) => {
				if (registeredPawn[0] === pawn.id) {
					ArrayUtils.remove(this.registeredPawns[key], registeredPawn);
				}
			});
		});
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {PlayedPawn[]}
	 */
	getRegisteredPawns(x, y) {
		let registeredPawns = [];
		let key = x + 'x' + y;
		if (this.registeredPawns[key]) {
			this.registeredPawns[key].forEach((neighboursData) => {
				registeredPawns.push({
					pawn: PawnDirectory.get(neighboursData[0]),
					feature: FeatureDirectory.get(neighboursData[1]),
					box: BoxDirectory.getByCoordinates(neighboursData[2])
				});
			});
		}
		return registeredPawns;
	},
	/**
	 * @param {ConditionData[]} conditionsData
	 * @param {Structure} structure
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @returns {boolean}
	 */
	checkCondition(conditionsData, structure, player, game, pawn) {
		let conditionsVerified = true;
		conditionsData.forEach((conditionData) => {
			let condition = Strategies.getCondition(conditionData.type);
			let result = condition.evaluate(structure, conditionData.configuration || {}, player, game, pawn);
			if ((result && conditionData.not) || (!result && !conditionData.not)) {
				conditionsVerified = false;
			}
		});
		return conditionsVerified;
	},
	/**
	 * @param {Box[]} nonEmptyBoxes
	 */
	refreshRegistrations(nonEmptyBoxes) {
		this.registeredFeatures = {};
		this.registeredFeaturesNeighbours = {};
		this.registeredFeaturesInfluences = {};
		this.registeredPawns = {};

		Structure.getBoxesStructures(nonEmptyBoxes, {}, []).forEach((structure) => {
			if (!structure.isCompleted()) {
				structure.register();
			}

			structure.initialFeature.model.influence.forEach((influenceData) => {
				Strategies.getInfluence(influenceData.type).register(structure, influenceData.zone);
			});
		});

		PawnEvaluator.getBoxesPawnEvaluators(nonEmptyBoxes).forEach((evaluator) => {
			evaluator.register();
		});
	}
};

/**
 * @typedef {Object} BoxStructuresConfiguration
 * @property {string=} featureType
 * @property {string=} featureCategory
 * @property {string=} featureStatus close|open|any
 */

export class Structure {
	// region static stuff.
	/**
	 * @param {Box[]} boxes
	 * @param {BoxStructuresConfiguration} configuration
	 * @param {Feature[]} excludedFeatures
	 * @returns {Structure[]}
	 */
	static getBoxesStructures(boxes, configuration, excludedFeatures) {
		let alreadyTestedFeatures = [];
		ArrayUtils.merge(alreadyTestedFeatures, excludedFeatures);

		let structures = [];

		boxes.forEach((box) => {
			if (!box.tile) {
				return;
			}

			box.tile.features.forEach((feature) => {
				if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
					return;
				}
				if (configuration.featureType && feature.code !== configuration.featureType) {
					return;
				}
				if (configuration.featureCategory && !ArrayUtils.contains(feature.model.categories, configuration.featureCategory)) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(alreadyTestedFeatures, structure.features);

				if (configuration.featureStatus && !structure.checkStatus(configuration.featureStatus)) {
					return;
				}

				structures.push(structure);
			});
		});

		return structures;
	}

	// endregion

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 */
	constructor(feature, box) {
		/** @type {Feature} */
		this.initialFeature = feature;
		/** @type {Box} */
		this.initialBox = box;
		/** @type {Feature[]} */
		this.features = [];
		/** @type {Box[]} */
		this.boxes = [];
		/** @type {PlayerPawn[]} */
		this.pawns = [];
		/** @type {NeutralPawn[]} */
		this.neutralPawns = [];
		/** @type {Feature[]} */
		this.featuresByPawnIndex = [];
		/** @type {Feature[]} */
		this.featuresByNeutralPawnIndex = [];
		/** @type {Feature[][]} */
		this.featuresByBoxIndex = [];
		/** @type {Box[]} */
		this.boxesByPawnIndex = [];
		/** @type {Box[]} */
		this.boxesByNeutralPawnIndex = [];
		/** @type {Box[]} */
		this.boxesByFeatureIndex = [];
		/** @type {Feature[]} */
		this.openFeatures = [];
		/** @type {Feature[]} */
		this.occupiedFeatures = [];

		this.addFeature(feature, box);
	}

	/**
	 * @returns {number}
	 */
	get initialX() {
		return this.initialBox.x;
	}

	/**
	 * @returns {number}
	 */
	get initialY() {
		return this.initialBox.y;
	}

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 */
	addFeature(feature, box) {
		if (ArrayUtils.contains(this.features, feature)) {
			return;
		}

		if (!ArrayUtils.contains(this.boxes, box)) {
			this.boxes.push(box);
			this.featuresByBoxIndex.push([]);
		}
		this.featuresByBoxIndex[this.boxes.indexOf(box)].push(feature);

		this.features.push(feature);
		this.boxesByFeatureIndex.push(box);

		if (feature.pawns.length || feature.neutralPawns.length) {
			let occupied = false;
			feature.pawns.forEach((pawn) => {
				if (pawn.model.type === 'follower') {
					occupied = true;
				}
			});
			if (occupied) {
				this.occupiedFeatures.push(feature);
			}

			feature.pawns.forEach((pawn) => {
				this.pawns.push(pawn);
				let pawnIndex = this.pawns.indexOf(pawn);
				this.featuresByPawnIndex[pawnIndex] = feature;
				this.boxesByPawnIndex[pawnIndex] = box;
			});
			feature.neutralPawns.forEach((pawn) => {
				this.neutralPawns.push(pawn);
				let pawnIndex = this.neutralPawns.indexOf(pawn);
				this.featuresByNeutralPawnIndex[pawnIndex] = feature;
				this.boxesByNeutralPawnIndex[pawnIndex] = box;
			});
		}

		let open = false;

		// Add adjacent features from neighbour tiles.
		for (let i = 0; i < 4; i++) {
			let neighbour = box.getNeighbour(i);
			for (let j = 0; j < 3; j++) {
				if (box.tile.getSideFeature(i, j) === feature) {
					if (!neighbour || neighbour.isEmpty) {
						open = true;
						continue;
					}
					let neighbourFeature = box.getRelatedFeature(i, j);
					// Add the next feature only if it has the same type.
					if (neighbourFeature.code === feature.code) {
						this.addFeature(neighbourFeature, neighbour);
					}
				}
			}
		}

		// Add adjacent features from special neighbour strategies.
		feature.getSpecialNeighbours(box, feature, box).forEach((specialNeighbour) => {
			if (specialNeighbour.isEmpty()) {
				open = true;
				return;
			}
			this.addFeature(specialNeighbour.feature, specialNeighbour.box);
		});
		feature.neighbours.forEach((neighbour) => {
			neighbour.getSpecialNeighbours(box, feature, box).forEach((specialNeighbour) => {
				if (specialNeighbour.isEmpty()) {
					open = true;
					return;
				}
				this.addFeature(specialNeighbour.feature, specialNeighbour.box);
			});
		});

		if (open) {
			this.openFeatures.push(feature);
		}
	}

	/**
	 * @param {Feature} feature
	 * @returns {Box}
	 */
	getBoxByFeature(feature) {
		return this.boxesByFeatureIndex[this.features.indexOf(feature)];
	}

	/**
	 * @param {PlayerPawn} pawn
	 * @returns {Box}
	 */
	getBoxByPawn(pawn) {
		return this.boxesByPawnIndex[this.pawns.indexOf(pawn)];
	}

	/**
	 * @param {NeutralPawn} pawn
	 * @returns {Box}
	 */
	getBoxByNeutralPawn(pawn) {
		return this.boxesByNeutralPawnIndex[this.neutralPawns.indexOf(pawn)];
	}

	/**
	 * @param {PlayerPawn} pawn
	 * @returns {Feature}
	 */
	getFeatureByPawn(pawn) {
		return this.featuresByPawnIndex[this.pawns.indexOf(pawn)];
	}

	/**
	 * @param {NeutralPawn} pawn
	 * @returns {Feature}
	 */
	getFeatureByNeutralPawn(pawn) {
		return this.featuresByNeutralPawnIndex[this.neutralPawns.indexOf(pawn)];
	}

	/**
	 * @param {Box} box
	 * @returns {Feature[]}
	 */
	getFeaturesByBox(box) {
		let features = [];
		box.tile.features.forEach((feature) => {
			if (ArrayUtils.contains(this.features, feature)) {
				features.push(feature);
			}
		});
		return features;
	}

	/**
	 * @returns {boolean}
	 */
	isClosed() {
		return this.openFeatures.length === 0;
	}

	/**
	 * @returns {boolean}
	 */
	isOccupied() {
		return this.occupiedFeatures.length > 0;
	}

	/**
	 * @returns {CompletionData[]}
	 */
	get completion() {
		return DynamicParameter.applyDynamicConfiguration(this.initialFeature.model.completion, this.initialFeature.dynamicParameters);
	}

	/**
	 * @returns {EvaluationData[]}
	 */
	get evaluationCompleted() {
		return DynamicParameter.applyDynamicConfiguration(this.initialFeature.model.evaluationCompleted, this.initialFeature.dynamicParameters);
	}

	/**
	 * @returns {EvaluationData[]}
	 */
	get evaluationEnd() {
		return DynamicParameter.applyDynamicConfiguration(this.initialFeature.model.evaluationEnd, this.initialFeature.dynamicParameters);
	}

	/**
	 * @returns {boolean}
	 */
	isCompleted() {
		let result = true;
		this.completion.forEach((data) => {
			let strategy = Strategies.getCompletion(data.type || 'null');
			if (!strategy.evaluate(this, data.configuration || {})) {
				result = false;
			}
		});

		return result;
	}

	register() {
		this.completion.forEach((data) => {
			let strategy = Strategies.getCompletion(data.type || 'null');
			strategy.register(this, data.configuration || {});
		});
	}

	/**
	 * @param {Feature} feature
	 * @param {Game} game
	 */
	onContinued(feature, game) {
		// Apply feature's effects.
		feature.model.continuation.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], this, game.currentPlayer, game)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = this.getBoxByFeature(feature);
				let source = this.getFeatureTimelineTrigger(box.coordinates, feature);
				strategy.apply(this, feature, strategyData.configuration || {}, game, null, source);
			}
		});

		// Apply pawns effects.
		this.pawns.forEach((pawn) => {
			pawn.onContinued(this, feature, game);
		});

		// Apply neutral pawns effects.
		this.neutralPawns.forEach((pawn) => {
			pawn.onContinued(this, feature, game);
		});
	}

	/**
	 * @param {Feature} feature
	 * @param {Game} game
	 */
	onCompleted(feature, game) {
		// Apply feature's effects.
		feature.model.completionEffects.forEach((effectData) => {
			if (Strategies.checkCondition(effectData.conditions || [], this, null, game)) {
				let strategy = Strategies.getEffect(effectData.type);
				let source = this.getFeatureTimelineTrigger(this.initialBox.coordinates, this.initialFeature);
				strategy.apply(this, feature, effectData.configuration, game, null, source);
			}
		});

		// Add points.
		let evaluator = new StructureEvaluator(this, game);
		evaluator.majors.forEach((player) => {
			player.addPoints(
				evaluator.evaluateCompleted(player),
				this.getFeatureTimelineTrigger(this.initialBox.coordinates, this.initialFeature)
			);
		});

		// Check neighbours.
		this.features.forEach((feature) => {
			Strategies.getRegisteredNeighbours(feature).forEach((neighbour) => {
				(new Structure(neighbour[0], neighbour[1])).checkCompletion(neighbour[0], game);
			});
		});

		// Remove all pawn.
		this.removeAllPawns(game, true, true);
	}

	onEndGame(game) {
		let evaluator = new StructureEvaluator(this, game);

		// Add points.
		evaluator.majors.forEach((player) => {
			player.addPoints(
				evaluator.evaluateEnd(player),
				this.getFeatureTimelineTrigger(this.initialBox.coordinates, this.initialFeature)
			);
		});

		// Remove all pawn.
		this.removeAllPawns(game, true, true);
	}

	getFeatureTimelineTrigger(coordinates, feature) {
		return new TimelineItemTrigger('c.main.source_feature', coordinates, null, { feature: feature.name });
	}

	/**
	 * @param {Feature} feature
	 * @param {Game} game
	 */
	checkCompletion(feature, game) {
		if (this.isCompleted()) {
			this.onCompleted(feature, game);
		}
	}

	/**
	 * @param {Player} player
	 * @param {string} type
	 * @returns {boolean}
	 */
	hasPlayerPawnOfType(player, type) {
		return this.getPlayerPawnsOfType(player, type).length > 0;
	}

	/**
	 * @param {Player} player
	 * @param {string} type
	 * @returns {PlayerPawn[]}
	 */
	getPlayerPawnsOfType(player, type) {
		let pawns = [];
		this.pawns.forEach((pawn) => {
			if (pawn.owner === player && pawn.model.type === type) {
				pawns.push(pawn);
			}
		});
		return pawns;
	}

	/**
	 * @param {Player} player
	 * @param {string} code
	 * @returns {boolean}
	 */
	hasPlayerPawnOfCode(player, code) {
		let has = false;
		this.pawns.forEach((pawn) => {
			if (pawn.owner === player && pawn.model.code === code) {
				has = true;
			}
		});
		return has;
	}

	/**
	 * @param {Game} game
	 * @param {boolean} exceptSpecial
	 * @param {boolean} exceptNeural
	 */
	removeAllPawns(game, exceptSpecial, exceptNeural) {
		// Remove pawns.
		for (let i = 0; i < this.features.length; i++) {
			let feature = this.features[i];

			// Player pawns.
			for (let j = 0; j < feature.pawns.length; j++) {
				let pawn = feature.pawns[j];
				if (!exceptSpecial || pawn.model.type !== 'special') {
					pawn.back();
					feature.removePawn(pawn);
				}
			}

			// Neutral pawns.
			if (!exceptNeural) {
				for (let j = 0; j < feature.neutralPawns.length; j++) {
					game.neutralPawnPool.addPawn(feature.neutralPawns[j]);
				}
				feature.removeNeutralPawns();
			}
		}

		// Refresh display.
		for (let i = 0; i < this.boxes.length; i++) {
			this.boxes[i].refreshPawns();
		}
	}

	/**
	 * @param {string} status close|open|any
	 * @returns {boolean}
	 */
	checkStatus(status) {
		if (status === 'any') {
			return true;
		}
		else if (status === 'closed') {
			if (this.isClosed()) {
				return true;
			}
		}
		else if (status === 'open') {
			if (!this.isClosed()) {
				return true;
			}
		}
		else {
			console.warn('[Structure.checkStatus] Unknown featureStatus:', status);
		}
		return false;
	}

	/**
	 * @param {string[]} types
	 * @returns {Structure[]}
	 */
	getNeighbourStructuresByTypes(types) {
		let neighbourStructures = [];
		let alreadyTestedFeatures = [this.features];
		for (let i = 0; i < this.features.length; i++) {
			let feature = this.features[i];
			let box = this.getBoxByFeature(feature);
			let neighbours = feature.getNeighboursByTypes(types);
			for (let j = 0; j < neighbours.length; j++) {
				if (!ArrayUtils.contains(alreadyTestedFeatures, neighbours[j])) {
					let neighbourStructure = new Structure(neighbours[j], box);
					ArrayUtils.merge(alreadyTestedFeatures, neighbourStructure.features);
					neighbourStructures.push(neighbourStructure);
				}
			}
		}
		return neighbourStructures;
	}

	/**
	 * @param {Box} box
	 * @returns {number}
	 */
	getConnectionsCountByBox(box) {
		let count = 0;
		box.tile.getSideDirectionsByFeatures(this.getFeaturesByBox(box)).forEach((sideDirection) => {
			let neighbour = box.getNeighbour(sideDirection);
			if (neighbour && !neighbour.isEmpty) {
				count++;
			}
		});
		return count;
	}

	/**
	 * @param {string} code
	 * @param {NeutralPawn[]} excluded
	 * @returns {boolean}
	 */
	containsNeutralPawn(code, excluded) {
		let result = false;
		this.features.forEach((feature) => {
			feature.neutralPawns.forEach((pawn) => {
				if (pawn.code === code && !ArrayUtils.contains(excluded, pawn)) {
					result = true;
				}
			})
		});
		return result;
	}
}

export class StructureEvaluator {
	/**
	 * @param {Structure} structure
	 * @param {Game} game
	 */
	constructor(structure, game) {
		this.structure = structure;
		this.data = {};
		this.majors = [];
		this.game = game;

		let maxScore = 0;
		structure.pawns.forEach((pawn) => {
			let player = pawn.owner;
			let playerName = player.name;
			if (!this.data.hasOwnProperty(playerName)) {
				this.data[playerName] = {
					pawns: [],
					score: 0,
					major: false
				};
			}
			this.data[playerName].pawns.push(pawn);

			pawn.model.majority.forEach((majorityData) => {
				if (Strategies.checkCondition(majorityData.conditions || [], structure, game.currentPlayer, game, pawn)) {
					let strategy = Strategies.getEvaluation(majorityData.type);
					this.data[playerName].score += strategy.evaluate(majorityData.configuration, game, structure, player, [pawn]);
					if (this.data[playerName].score) {
						if (this.data[playerName].score > maxScore) {
							this.majors = [player];
							maxScore = this.data[playerName].score;
						}
						else if (this.data[playerName].score === maxScore) {
							this.majors.push(player);
						}
					}
				}
			});
		});

		this.majors.forEach((player) => {
			this.data[player.name].major = true;
		});
	}

	/**
	 * @param player
	 * @returns {number}
	 */
	evaluateCompleted(player) {
		return this.evaluate(player, this.structure.evaluationCompleted);
	}

	/**
	 * @param player
	 * @returns {number}
	 */
	evaluateEnd(player) {
		return this.evaluate(player, this.structure.evaluationEnd);
	}

	/**
	 * @param {Player} player
	 * @param {EvaluationData[]} evaluationData
	 * @returns {number}
	 */
	evaluate(player, evaluationData) {
		let result = 0;
		evaluationData.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], this.structure, player, this.game)) {
				let strategy = Strategies.getEvaluation(strategyData.type);
				result += Math.ceil(strategy.evaluate(strategyData.configuration, this.game, this.structure, player, this.data[player.name].pawns));
			}
		});
		return result;
	}

	/**
	 * @param {Player} player
	 * @returns {boolean}
	 */
	isOccupiedByPlayer(player) {
		return this.data[player.name] && this.data[player.name].pawns.length > 0;
	}
}

export class PawnEvaluator {
	// region static stuff.
	/**
	 * @param {Box[]} boxes
	 * @returns {PawnEvaluator[]}
	 */
	static getBoxesPawnEvaluators(boxes) {
		let evaluators = [];

		boxes.forEach((box) => {
			if (!box.tile) {
				return;
			}

			box.tile.features.forEach((feature) => {
				feature.pawns.forEach((pawn) => {
					evaluators.push(new PawnEvaluator(pawn, feature, box));
				});
			});
		});

		return evaluators;
	}

	// endregion
	/**
	 * @param {PlayerPawn} pawn
	 * @param {Feature} feature
	 * @param {Box} box
	 */
	constructor(pawn, feature, box) {
		this.pawn = pawn;
		this.feature = feature;
		this.box = box;
		this.structure = new Structure(feature, box);
	}

	/**
	 * @param {Game} game
	 * @returns {number}
	 */
	evaluateCompleted(game) {
		return this.evaluate(this.pawn.model.evaluationCompleted, game);
	}

	/**
	 * @param {Game} game
	 * @returns {number}
	 */
	evaluateEnd(game) {
		return this.evaluate(this.pawn.model.evaluationEnd, game);
	}

	/**
	 * @param {EvaluationData[]} evaluationData
	 * @param {Game} game
	 * @returns {number}
	 */
	evaluate(evaluationData, game) {
		let result = 0;
		let player = this.pawn.owner;
		evaluationData.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], this.structure, player, game)) {
				let strategy = Strategies.getEvaluation(strategyData.type);
				result += Math.ceil(strategy.evaluate(strategyData.configuration, game, this.structure, player, [this.pawn]));
			}
		});
		return result;
	}

	/**
	 * @returns {boolean}
	 */
	isCompleted() {
		let result = true;
		this.pawn.model.completion.forEach((data) => {
			let strategy = Strategies.getCompletion(data.type || 'null');
			// For now, the available strategies are only zone ones, so it works with the structure.
			if (!strategy.evaluate(this.structure, data.configuration || {})) {
				result = false;
			}
		});

		return result;
	}

	register() {
		this.pawn.model.completion.forEach((data) => {
			let strategy = Strategies.getCompletion(data.type || 'null');
			strategy.registerPawn(this.pawn, this.structure, data.configuration || {});
		});
	}

	/**
	 * @param {Game} game
	 */
	onCompleted(game) {
		// Add points.
		this.pawn.owner.addPoints(
			this.evaluateCompleted(game),
			this.getPawnTimelineTrigger(this.box.coordinates)
		);

		// Remove pawn.
		this.pawn.back();
		this.structure.initialFeature.removePawn(this.pawn);
	}

	/**
	 * @param {Game} game
	 */
	onEndGame(game) {
		// Add points.
		this.pawn.owner.addPoints(
			this.evaluateEnd(game),
			this.getPawnTimelineTrigger(this.box.coordinates)
		);

		// Remove pawn.
		this.pawn.back();
		this.structure.initialFeature.removePawn(this.pawn);
	}

	/**
	 * @param {Coordinates} coordinates
	 */
	getPawnTimelineTrigger(coordinates) {
		return new TimelineItemTrigger('c.main.source_pawn', coordinates, null, { pawn: this.pawn.name });
	}
}

export class MessageEvaluator {
	/**
	 * @param {MessageModel} model
	 * @param {Game} game
	 */
	constructor(model, game) {
		this.model = model;
		this.game = game;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		delete this.model;
		delete this.game;
	}

	accept() {
		this.model.effects.forEach((effectData) => {
			let strategy = Strategies.getPlayerEffect(effectData.type);
			let source = new TimelineItemTrigger('c.timeline.accepted_message', null, null, { messageCode: this.model.code });
			strategy.apply(effectData.configuration || {}, this.game.currentPlayer, this.game, source);
		});
	}

	/**
	 * @returns {PlayerEffect}
	 */
	estimate() {
		let effect = PlayerEffect.getNewEmpty();
		this.model.effects.forEach((effectData) => {
			let strategy = Strategies.getPlayerEffect(effectData.type);
			let source = new TimelineItemTrigger('c.timeline.accepted_message', null, null, { messageCode: this.model.code });
			effect.merge(strategy.estimate(effectData.configuration || {}, this.game.currentPlayer, this.game, source));
		});
		return effect;
	}

	refuse() {
		this.game.currentPlayer.addPoints(2, new TimelineItemTrigger('c.timeline.refused_message', null, null, { messageCode: this.model.code }));
	}
}

export class TokenEvaluator {
	/**
	 * @param {TokenModel} model
	 * @param {Game} game
	 */
	constructor(model, game) {
		this.model = model;
		this.game = game;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		delete this.model;
		delete this.game;
	}

	/**
	 * @param player
	 * @returns {number}
	 */
	evaluateEnd(player) {
		this.model.scoreEnd.forEach((strategyData) => {
			if (player.hasToken(this.model.code) && Strategies.checkCondition(strategyData.conditions || [], null, player, this.game)) {
				let strategy = Strategies.getEvaluation(strategyData.type);
				let source = new TimelineItemTrigger('c.timeline.possessed_token', null, null, { tokenKey: this.model.nameKey });
				player.addPoints(strategy.evaluate(strategyData.configuration, this.game, null, player, []), source);
			}
		});
	}
}