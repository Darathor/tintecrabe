/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils, MathUtils } from './core.mjs';
import { FeatureDirectory, PlayerDirectory } from './directories.mjs';
import { Models } from './models.mjs';
import { DynamicParameter } from './models/dynamic-parameter.mjs';
import { Structure } from './strategies.mjs';
import { Strategies } from "./strategies.mjs";
import { Board } from "./board.mjs";
import { PlayerPawn, NeutralPawn } from "./pawns.mjs";

export class BoxFeature {
	/**
	 * @param {Box|null} box
	 * @param {Feature|null} feature
	 */
	constructor(box, feature) {
		this.box = box;
		this.feature = feature;
	}

	/**
	 * @returns {boolean}
	 */
	isEmpty() {
		return this.box === null || this.feature === null;
	}

	/**
	 * @param {BoxFeature} target
	 * @returns {boolean}
	 */
	isSame(target) {
		return this.box === target.box && this.feature === target.feature;
	}
}

export class Feature {
	/** @type {number} */
	id; // initialized by FeatureDirectory.add().

	/**
	 * @param {string} code
	 * @param {number} x
	 * @param {number} y
	 * @param {Array} dynamicParameters
	 */
	constructor(code, x, y, dynamicParameters) {
		FeatureDirectory.add(this);
		/** @type {string} */
		this.code = code;
		/** @type {number} */
		this.x = x;
		/** @type {number} */
		this.y = y;
		/** @type {PlayerPawn[]} */
		this.pawns = [];
		/** @type {NeutralPawn[]} */
		this.neutralPawns = [];
		/** @type {boolean} */
		this.isFree = true;
		/** @type {Feature[]} */
		this.neighbours = [];

		this.dynamicParameters = dynamicParameters;
		if (this.model.externalLink) {
			this.externalLink = {
				tileCode: dynamicParameters['neighbourTileCode'],
				featureIndex: dynamicParameters['neighbourFeatureIndex']
			}
		}
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		FeatureDirectory.remove(this);
		ArrayUtils.clear(this.pawns);
		ArrayUtils.clear(this.neutralPawns);
		ArrayUtils.clear(this.neighbours);
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return this.model.name;
	}

	/**
	 * @returns {FeatureModel}
	 */
	get model() {
		let model = Models.features[this.code];
		if (!model) {
			console.error('[Feature.model] Unknown code:', this.code);
		}
		return model;
	}

	/**
	 * @returns {FeatureInfluenceData[]}
	 */
	get influence() {
		return DynamicParameter.applyDynamicConfiguration(this.model.influence, this.dynamicParameters);
	}

	/**
	 * @param {number} rotation
	 * @returns {number}
	 */
	getX(rotation) {
		if (rotation === 0) {
			return this.x;
		}
		if (rotation === 90) {
			return 100 - this.y;
		}
		if (rotation === 180) {
			return 100 - this.x;
		}
		if (rotation === 270) {
			// noinspection JSSuspiciousNameCombination
			return this.y;
		}
	}

	/**
	 * @param {number} rotation
	 * @returns {number}
	 */
	getY(rotation) {
		if (rotation === 0) {
			return this.y;
		}
		if (rotation === 90) {
			// noinspection JSSuspiciousNameCombination
			return this.x;
		}
		if (rotation === 180) {
			return 100 - this.y;
		}
		if (rotation === 270) {
			return 100 - this.x;
		}
	}

	/**
	 * @param {Feature} feature
	 * @returns {boolean}
	 */
	isCompatibleWith(feature) {
		if (this.code === feature.code) {
			return true;
		}
		let ok = false;
		this.model.compatibleWith.forEach((code) => {
			if (code === '*' || code === feature.code) {
				ok = true;
			}
		});
		feature.model.compatibleWith.forEach((code) => {
			if (code === '*' || code === this.code) {
				ok = true;
			}
		});
		return ok;
	}

	/**
	 * @param {Feature} neighbour
	 */
	addNeighbour(neighbour) {
		this.neighbours.push(neighbour);
	}

	/**
	 * @param {string} code
	 * @returns {Feature[]}
	 */
	getNeighboursByType(code) {
		let result = [];
		for (let i = 0; i < this.neighbours.length; i++) {
			if (this.neighbours[i].code === code) {
				result.push(this.neighbours[i]);
			}
		}
		return result;
	}

	/**
	 * @param {string[]} codes
	 * @returns {Feature[]}
	 */
	getNeighboursByTypes(codes) {
		let result = [];
		for (let i = 0; i < this.neighbours.length; i++) {
			if (ArrayUtils.contains(codes, this.neighbours[i].code)) {
				result.push(this.neighbours[i]);
			}
		}
		return result;
	}

	/**
	 * @param {Box} originBox
	 * @param {Feature} targetFeature
	 * @param {Box} targetBox
	 * @returns {BoxFeature[]}
	 */
	getSpecialNeighbours(originBox, targetFeature, targetBox) {
		let result = [];
		let target = new BoxFeature(targetBox, targetFeature);
		DynamicParameter.applyDynamicConfiguration(this.model.specialNeighbours, this.dynamicParameters).forEach((strategyData) => {
			let strategy = Strategies.getNeighbours(strategyData.type);
			ArrayUtils.merge(result, strategy.resolve(new BoxFeature(originBox, this), target, strategyData.configuration || []));
		});
		return result;
	}

	/**
	 * @returns {BoxFeature}
	 */
	resolveExternalLink() {
		let externalLink = this.externalLink;
		if (!externalLink) {
			console.warn('[Feature.resolveExternalLink] This feature has no external link:', this.code);
			return new BoxFeature(null, null);
		}

		let linkedBoxes = Board.getBoxesByTileCode(externalLink.tileCode);
		if (linkedBoxes.length > 1) {
			console.error('[Feature.resolveExternalLink] Target tile should be unique:', externalLink.tileCode);
		}

		if (linkedBoxes.length > 0) {
			let linkedFeature = linkedBoxes[0].tile.features[externalLink.featureIndex];
			if (!linkedFeature) {
				console.error('[Feature.resolveExternalLink] Target tile does not contains the feature:', externalLink.featureIndex);
				return new BoxFeature(null, null);
			}
			return new BoxFeature(linkedBoxes[0], linkedFeature);
		}
		return new BoxFeature(null, null);
	}

	/**
	 * @param {Player} player
	 * @param {Box} box
	 * @param {Game} game
	 * @returns {boolean}
	 */
	isPlayable(player, box, game) {
		return player.getPlayablePawns(new Structure(this, box), this, game).length > 0;
	}

	/**
	 * @param {PlayerPawn} pawn
	 */
	addPawn(pawn) {
		this.pawns.push(pawn);
		this.refreshIsFree();
	}

	/**
	 * @param {NeutralPawn} pawn
	 */
	addNeutralPawn(pawn) {
		this.neutralPawns.push(pawn);
		this.refreshIsFree();
	}

	/**
	 * @param {PlayerPawn} pawn
	 */
	removePawn(pawn) {
		Strategies.unRegisterPawn(pawn);
		this.pawns.splice(this.pawns.indexOf(pawn), 1);
		this.refreshIsFree();
	}

	/**
	 * @param {NeutralPawn} pawn
	 */
	removeNeutralPawn(pawn) {
		this.pawns.splice(this.neutralPawns.indexOf(pawn), 1);
		this.refreshIsFree();
	}

	removePawns() {
		this.pawns.forEach((pawn) => {
			Strategies.unRegisterPawn(pawn);
		});
		ArrayUtils.clear(this.pawns);
		this.refreshIsFree();
	}

	removeNeutralPawns() {
		ArrayUtils.clear(this.neutralPawns);
		this.refreshIsFree();
	}

	removeAllPawns() {
		this.pawns.forEach((pawn) => {
			Strategies.unRegisterPawn(pawn);
		});
		ArrayUtils.clear(this.pawns);
		ArrayUtils.clear(this.neutralPawns);
		this.refreshIsFree();
	}

	refreshIsFree() {
		this.isFree = this.pawns.length === 0 && this.neutralPawns.length === 0;
	}

	/**
	 * @param {Player} player
	 * @returns {boolean}
	 */
	containsPlayerPawn(player) {
		let result = false;
		this.pawns.forEach((pawn) => {
			if (pawn.owner === player) {
				result = true;
			}
		});
		return result;
	}
}

/**
 * @typedef {Object} SerializedTile
 * @property {string} code
 * @property {number} rotation
 * @property {SerializedFeaturePawn[]} pawns
 * @property {SerializedFeatureNeutralPawn[]} neutralPawns
 */

/**
 * @typedef {SerializedPawn} SerializedFeaturePawn
 * @property {number} featureIndex
 */

/**
 * @typedef {SerializedPawn} SerializedFeatureNeutralPawn
 * @property {number} featureIndex
 * @property {string} code
 */

export class Tile {
	//region Static stuff.
	/**
	 * @param {SerializedTile} data
	 * @returns {Tile}
	 */
	static fromData(data) {
		let tile = new Tile(Models.tiles[data.code]);
		tile.data = data;
		return tile;
	}

	//endregion

	/**
	 * @param {TileModel} model
	 */
	constructor(model) {
		/** @type {TileModel} */
		this.model = model;
		/** @type {string} */
		this.code = model.code;
		/** @type {string} */
		this.src = model.src;
		/** @type {Feature[]} */
		this.features = [];
		/** @type {Feature[][]} */
		this.sides = [];
		/** @type {number} */
		this.rotation = 0;

		for (let i = 0; i < model.featuresData.length; i++) {
			let featureData = model.featuresData[i];
			this.features.push(new Feature(featureData[0], featureData[1], featureData[2], featureData[4]));
		}
		// Set features neighbours.
		for (let i = 0; i < model.featuresData.length; i++) {
			let neighbours = model.featuresData[i][3];
			for (let j = 0; j < neighbours.length; j++) {
				this.features[i].addNeighbour(this.features[neighbours[j]]);
			}
		}
		// Set sides.
		for (let i = 0; i < model.sides.length; i++) {
			let side = model.sides[i];
			this.sides.push([this.features[side[0]], this.features[side[1]], this.features[side[2]]]);
		}
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		delete this.model;
		this.features.forEach((feature) => {
			feature.free();
		});
		ArrayUtils.clear(this.features);
		ArrayUtils.clear(this.sides);
	}

	rotateLeft() {
		this.rotation = (this.rotation <= 0) ? 270 : (this.rotation - 90);
	}

	rotateRight() {
		this.rotation = (this.rotation >= 270) ? 0 : (this.rotation + 90);
	}

	/**
	 * @param {string} orientation 'up', 'right', 'down' or 'left'
	 */
	setOrientation(orientation) {
		if (orientation === 'up') {
			this.rotation = 0;
		}
		else if (orientation === 'right') {
			this.rotation = 90;
		}
		else if (orientation === 'down') {
			this.rotation = 180;
		}
		else if (orientation === 'left') {
			this.rotation = 270;
		}
	}

	setRandomOrientation() {
		this.rotation = MathUtils.randomInt(0, 3) * 90;
	}

	/**
	 * @param {string|number} direction top|right|bottom|left
	 * @returns {Feature[]}
	 */
	getSide(direction) {
		let index = 4 - (this.rotation / 90);
		if (direction === 'top' || direction === 0) {
			index += 0;
		}
		else if (direction === 'right' || direction === 1) {
			index += 1;
		}
		else if (direction === 'bottom' || direction === 2) {
			index += 2;
		}
		else if (direction === 'left' || direction === 3) {
			index += 3;
		}
		return this.sides[index % 4];
	}

	/**
	 * @param {string|number} direction top|right|bottom|left
	 * @param {number} index
	 * @returns {Feature}
	 */
	getSideFeature(direction, index) {
		return this.getSide(direction)[index];
	}

	/**
	 * @param {Feature[]} features
	 * @returns {number[]}
	 */
	getSideDirectionsByFeatures(features) {
		let sideDirections = [];
		for (let sideDirection = 0; sideDirection < 4; sideDirection++) {
			this.getSide(sideDirection).forEach((sideFeature) => {
				if (ArrayUtils.contains(features, sideFeature) && !ArrayUtils.contains(sideDirections, sideDirection)) {
					sideDirections.push(sideDirection);
				}
			});
		}
		return sideDirections;
	}

	/**
	 * @param {string|number} direction
	 * @returns {number}
	 */
	getOppositeDirection(direction) {
		if (direction === 'top' || direction === 0) {
			return 2;
		}
		else if (direction === 'right' || direction === 1) {
			return 3;
		}
		else if (direction === 'bottom' || direction === 2) {
			return 0;
		}
		else if (direction === 'left' || direction === 3) {
			return 1;
		}
	}

	/**
	 * @param {number} index
	 * @returns {number}
	 */
	getOppositeIndex(index) {
		return 2 - index;
	}

	/**
	 * @param {string} direction top|right|bottom|left
	 * @param {number} index
	 * @returns {Feature}
	 */
	getOppositeFeature(direction, index) {
		return this.getSideFeature(this.getOppositeDirection(direction), this.getOppositeIndex(index));
	}

	/**
	 * @param {Tile} tile
	 * @param {string|number} position top|right|bottom|left
	 * @returns {boolean}
	 */
	isCompatible(tile, position) {
		let side1 = this.getSide(position);
		let side2 = tile.getSide(this.getOppositeDirection(position));
		return side1[0].isCompatibleWith(side2[2]) && side1[1].isCompatibleWith(side2[1]) && side1[2].isCompatibleWith(side2[0]);
	}

	/**
	 * @returns {string}
	 */
	get tooltip() {
		return '<img src="' + this.src + '" class="tile rotate-' + this.rotation + '" alt="' + this.code + '" />';
	}

	/**
	 * @returns {SerializedTile}
	 */
	get data() {
		let data = {
			code: this.code,
			rotation: this.rotation,
			pawns: [],
			neutralPawns: []
		};

		this.features.forEach((feature, index) => {
			feature.pawns.forEach((pawn) => {
				let pawnData = pawn.data;
				pawnData.featureIndex = index;
				data.pawns.push(pawnData);
			});
			feature.neutralPawns.forEach((pawn) => {
				let pawnData = pawn.data;
				pawnData.featureIndex = index;
				data.neutralPawns.push(pawnData);
			});
		});

		return data;
	}

	/**
	 * @param {SerializedTile} data
	 */
	set data(data) {
		if (!data) {
			return;
		}

		if (data.code !== this.code) {
			console.warn('[Tile.set data] code is different! ' + data.code + ' != ' + this.code);
			return;
		}

		this.rotation = data.rotation;

		data.pawns.forEach((pawnData) => {
			this.features[pawnData.featureIndex].addPawn(
				new PlayerPawn(Models.getPawnByCode(pawnData.code), PlayerDirectory.getByName(pawnData.ownerName))
			);
		});

		if (data.neutralPawns) {
			data.neutralPawns.forEach((pawnData) => {
				this.features[pawnData.featureIndex].addNeutralPawn(new NeutralPawn(Models.getNeutralPawnByCode(pawnData.code)));
			});
		}
	}
}