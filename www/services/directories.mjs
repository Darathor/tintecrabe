/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils } from "./core.mjs";

export class FeatureDirectory {
	/** @type {Feature[]} */
	static instances = [];

	/**
	 * @param {number} id
	 * @returns {Feature|null}
	 */
	static get(id) {
		return this.instances[id] || null;
	}

	/**
	 * @param {Feature} feature
	 */
	static add(feature) {
		this.instances.push(feature);
		feature.id = this.instances.indexOf(feature);
	}

	/**
	 * @param {Feature} feature
	 */
	static remove(feature) {
		ArrayUtils.remove(this.instances, feature);
	}

	static clear() {
		this.instances.forEach((feature) => {
			feature.free();
		});
		ArrayUtils.clear(this.instances);
	}
}

export class BoxDirectory {
	/** @type {Object} */
	static instances = {};

	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box|null}
	 */
	static get(x, y) {
		return this.instances[x + '_' + y] || null;
	}

	/**
	 * @returns {Box[]}
	 */
	static getInstancesArray() {
		let instances = [];
		Object.keys(BoxDirectory.instances).forEach((key) => {
			instances.push(BoxDirectory.instances[key]);
		});
		return instances;
	}

	/**
	 * @param {CoordinatesData} coordinates
	 * @returns {Box|null}
	 */
	static getByCoordinates(coordinates) {
		return this.get(coordinates.x, coordinates.y);
	}

	/**
	 * @param {Box} box
	 */
	static add(box) {
		this.instances[box.x + '_' + box.y] = box;
	}

	/**
	 * @param {Box} box
	 */
	static remove(box) {
		delete this.instances[box.x + '_' + box.y];
	}

	/**
	 * @param {Box} box
	 * @param {number} i
	 * @param {number} j
	 */
	static move(box, i, j) {
		delete BoxDirectory.instances[box.x + '_' + box.y];
		box.x = i;
		box.y = j;
		box.refreshAccessibility(true);
		BoxDirectory.instances[i + '_' + j] = box;
	}

	static refreshAccessibilities() {
		Object.keys(BoxDirectory.instances).forEach((key) => {
			BoxDirectory.instances[key].refreshAccessibility();
		});
	}
}

export class PlayerDirectory {
	/** @type {Object} */
	static byName = {};

	/** @type {Object} */
	static byPeerId = {};

	/**
	 * @param {string} name
	 * @returns {Player|null}
	 */
	static getByName(name) {
		return this.byName[name] || null;
	}

	/**
	 * @param {string} peerId
	 * @returns {Player|null}
	 */
	static getByPeerId(peerId) {
		return this.byPeerId[peerId] || null;
	}

	/**
	 * @param {Player} player
	 */
	static add(player) {
		if (player.name) {
			this.byName[player.name] = player;
		}
		if (player.peerId) {
			this.byPeerId[player.peerId] = player;
		}
	}

	/**
	 * @param {Player} player
	 */
	static remove(player) {
		if (player.name) {
			delete this.byName[player.name];
		}
		if (player.peerId) {
			delete this.byPeerId[player.peerId];
		}
	}
}

export class PawnDirectory {
	/** @type {PlayerPawn[]} */
	static instances = [];

	/**
	 * @param {number} id
	 * @returns {PlayerPawn|null}
	 */
	static get(id) {
		return this.instances[id] || null;
	}

	/**
	 * @param {PlayerPawn} pawn
	 */
	static add(pawn) {
		this.instances.push(pawn);
		pawn.id = this.instances.indexOf(pawn);
	}

	/**
	 * @param {PlayerPawn} pawn
	 */
	static remove(pawn) {
		ArrayUtils.remove(this.instances, pawn);
	}

	static clear() {
		this.instances.forEach((pawn) => {
			pawn.free();
		});
		ArrayUtils.clear(this.instances);
	}
}