/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class Sound {
	constructor(url) {
		this.url = url;
		this.audio = new Audio(url);
	}

	/**
	 * @returns {Promise<void>}
	 */
	play() {
		return this.audio.play();
	}
}

export let Sounds = {
	startTurn: new Sound('../sounds/start-turn.mp3'),
	startGame: new Sound('../sounds/start-game.mp3'),
	endGame: new Sound('../sounds/end-game.mp3'),
	newAlert: new Sound('../sounds/new-alert.mp3'),
	messageReceived: new Sound('../sounds/message-received.mp3')
}