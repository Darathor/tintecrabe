/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { LocalStorage } from "./core.mjs";

export class Preferences {
	static storageKey = 'preferences';

	/**
	 * @returns {Preferences}
	 */
	static getInstance() {
		if (!Preferences.instance) {
			Preferences.instance = new Preferences();
		}
		return Preferences.instance;
	}

	/** @type {string} - 'large' or 'small' */
	_setupTileSize = 'large';

	/** @type {boolean} */
	_setupHideDisabledTiles = false;

	/** @type {number} - 1, 2 or 4 */
	_zoomLevel = 2;

	/** @type {boolean} */
	_collapseLeftSidebar = false;

	/** @type {boolean} */
	_collapseRightSidebar = false;

	/** @type {boolean} */
	_collapseAsideGameAdmin = false;

	/** @type {boolean} */
	_collapseAsidePreferences = false;

	/** @type {boolean} */
	_collapseAsideGameDebug = true;

	/** @type {boolean} */
	_collapseAsideTimeline = false;

	/** @type {boolean} */
	_showCoordinatesOnInaccessible = false;

	/** @type {boolean} */
	_activateTooltipOnTileHover = true;

	/** @type {boolean} */
	_playSounds = true;

	constructor() {
		let data = LocalStorage.getObject(Preferences.storageKey);
		if (data) {
			Object.keys(data).forEach((key) => {
				this[key] = data[key];
			});
		}
	}

	save() {
		LocalStorage.setObject(Preferences.storageKey, {
			_setupTileSize: this._setupTileSize,
			_setupHideDisabledTiles: this._setupHideDisabledTiles,
			_zoomLevel: this._zoomLevel,
			_collapseLeftSidebar: this._collapseLeftSidebar,
			_collapseRightSidebar: this._collapseRightSidebar,
			_collapseAsideGameAdmin: this._collapseAsideGameAdmin,
			_collapseAsidePreferences: this._collapseAsidePreferences,
			_collapseAsideGameDebug: this._collapseAsideGameDebug,
			_collapseAsideTimeline: this._collapseAsideTimeline,
			_showCoordinatesOnInaccessible: this._showCoordinatesOnInaccessible,
			_activateTooltipOnTileHover: this._activateTooltipOnTileHover,
			_playSounds: this._playSounds
		});
	}

	/**
	 * @returns {string}
	 */
	get setupTileSize() {
		return this._setupTileSize === 'small' ? 'small' : 'large';
	}

	/**
	 * @param {string} value - 'large' or 'small'
	 */
	set setupTileSize(value) {
		this._setupTileSize = value === 'small' ? 'small' : 'large';
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get setupHideDisabledTiles() {
		return this._setupHideDisabledTiles;
	}

	/**
	 * @param {boolean} value
	 */
	set setupHideDisabledTiles(value) {
		this._setupHideDisabledTiles = !!value;
		this.save();
	}

	/**
	 * @returns {number}
	 */
	get zoomLevel() {
		return this._zoomLevel;
	}

	/**
	 * @param {number} value - 1, 2 or 4
	 */
	set zoomLevel(value) {
		this._zoomLevel = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseLeftSidebar() {
		return this._collapseLeftSidebar;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseLeftSidebar(value) {
		this._collapseLeftSidebar = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseRightSidebar() {
		return this._collapseRightSidebar;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseRightSidebar(value) {
		this._collapseRightSidebar = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideGameAdmin() {
		return this._collapseAsideGameAdmin;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideGameAdmin(value) {
		this._collapseAsideGameAdmin = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsidePreferences() {
		return this._collapseAsidePreferences;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsidePreferences(value) {
		this._collapseAsidePreferences = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideGameDebug() {
		return this._collapseAsideGameDebug;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideGameDebug(value) {
		this._collapseAsideGameDebug = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideTimeline() {
		return this._collapseAsideTimeline;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideTimeline(value) {
		this._collapseAsideTimeline = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get showCoordinatesOnInaccessible() {
		return this._showCoordinatesOnInaccessible;
	}

	/**
	 * @param {boolean} value
	 */
	set showCoordinatesOnInaccessible(value) {
		this._showCoordinatesOnInaccessible = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get activateTooltipOnTileHover() {
		return this._activateTooltipOnTileHover;
	}

	/**
	 * @param {boolean} value
	 */
	set activateTooltipOnTileHover(value) {
		this._activateTooltipOnTileHover = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get playSounds() {
		return this._playSounds;
	}

	/**
	 * @param {boolean} value
	 */
	set playSounds(value) {
		this._playSounds = value;
		this.save();
	}
}