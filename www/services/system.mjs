/**
 * Copyright (C) 2021 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from "./connection.mjs";
import { I18n } from "./core.mjs";

export class System {
	/** @type {VueRouter} */
	static router;

	static ping() {
		if (!Connection.instance) {
			console.error('[System.ping] Not connected!');
			return;
		}

		Connection.instance.ping();
	}

	static reload() {
		if (!Connection.instance) {
			console.error('[System.reload] Not connected!');
			return;
		}
		if (Connection.isServer()) {
			console.error('[System.reload] Cannot reload server!');
			return;
		}

		console.log('[System.reload] START reload');
		System.router.push('/reconnect/' + Connection.instance.server.peer + '/' + Connection.id);
		window.location.reload();
	}

	/**
	 * @param {Object} data
	 */
	static requestReconnection(data) {
		Connection.startClient(data.serverId).then(() => {
			Connection.sendMessage('RequestReconnection', { previousId: data.selfId });
		});
	}
}