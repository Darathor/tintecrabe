/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils, LocalStorage, I18n, Alerts } from './core.mjs';
import { Models } from './models.mjs';
import { Player, Color, Colors } from './players.mjs';
import { Connection } from './connection.mjs';
import { System } from "./system.mjs";
import { Sounds } from "./sounds.mjs";
import { Preferences } from "./preferences.mjs";

let document = window.document;

/**
 * @typedef {Object} FeatureData
 * @property {string} code
 * @property {FeatureModel} model
 * @property {boolean} enabled
 * @property {TileData[]} tiles
 * @property {TileData[]} startingHand
 * @property {TileData[]} additionalStartTiles
 * @property {MessageData[]} messages
 */

/**
 * @typedef {Object} ExtensionData
 * @property {string} code
 * @property {Extension} extension
 * @property {TileData[]} tiles
 * @property {TileData[]} startingHand
 * @property {TileData[]} additionalStartTiles
 * @property {MessageData[]} messages
 * @property {BoardEventData[]} boardEvents
 */

/**
 * @typedef {Object} TileData
 * @property {string} code
 * @property {TileModel} tile
 * @property {boolean} enabled
 * @property {boolean} selected
 * @property {number} quantity
 */

/**
 * @typedef {Object} MessageData
 * @property {string} code
 * @property {MessageModel} message
 * @property {boolean} enabled
 * @property {boolean} selected
 * @property {number} quantity
 */

/**
 * @typedef {Object} BoardEventData
 * @property {string} code
 * @property {BoardEventModel} event
 * @property {boolean} enabled
 * @property {boolean} selected
 * @property {number} quantity
 */

let increment = 1;

export let Setup = {
	/** @type {boolean} */
	initialized: false,
	/** @type {boolean} */
	waitingForServerInit: false,
	/** @type {Player[]} */
	players: [],
	/** @type {Object[]} */
	pawns: [],
	/** @type {Object[]} */
	neutralPawns: [],
	/** @type {Object[]} */
	tokens: [],
	/** @type {FeatureData[]} */
	features: [],
	/** @type {string} */
	poolType: 'selection',
	/** @type {number} */
	poolSize: 50,
	/** @type {boolean} */
	activateStartingHand: false,
	/** @type {boolean} */
	activateMessages: true,
	/** @type {number} */
	messagePeriod: 5,
	/** @type {string} */
	messagePoolType: 'selection',
	/** @type {number} */
	messagePoolSize: 10,
	/** @type {boolean} */
	activateBoardEvents: true,
	/** @type {number} */
	boardEventPeriod: 10,
	/** @type {string} */
	boardEventPoolType: 'selection',
	/** @type {number} */
	boardEventPoolSize: 10,
	/** @type {ExtensionData[]} */
	selection: [],
	/** @type {TileModel[]} */
	startTileOptions: [],
	/** @type {TileModel|null} */
	startTile: null,
	start: {
		multiple: false,
		additionalType: 'selection',
		additionalCount: 1,
		horizontalSize: 5,
		verticalSize: 5,
		minSpacing: 1,
	},
	board: {
		horizontalType: 'infinite',
		horizontalSize: 10,
		verticalType: 'infinite',
		verticalSize: 10,
	},
	/** @type {OptionalRules} */
	optionalRules: {
		extraTurnWhenFillHole: true,
		allowToGetBackPawn: true,
		allowPoolDetailView: true,
		allowAddTileToHand: true,
		showMessagesEstimation: true,
	},
	collapse: {
		board: true,
		neutralPawns: true,
		tokens: true
	},
	init(onlyDefinitions) {
		if (this.initialized) {
			return;
		}
		this.initPawns();
		this.initNeutralPawns();
		this.initTokens();
		this.initFeatures();
		this.initSelection();
		if (!onlyDefinitions) {
			this.addPlayer(Connection.id);
			if (Connection.id) {
				this.loadPlayerData();
			}
		}
		else {
			this.waitingForServerInit = true;
		}
		this.initialized = true;
	},
	/**
	 * @param {string=} peerId
	 */
	addPlayer(peerId) {
		let name = I18n.trans('c.setup.player') + ' ' + increment++;
		let color = null;
		for (let key in Colors) {
			if (Colors.hasOwnProperty(key) && Colors[key].isAvailable(this.players)) {
				color = Colors[key];
				break;
			}
		}
		if (name && color) {
			this.players.push(new Player(name, color, peerId));
		}
	},
	/**
	 * @param {Player} player
	 */
	removePlayer(player) {
		ArrayUtils.remove(this.players, player);
		if (player.peerId && Connection.instance.mode === 'server') {
			Connection.sendMessage('RemovePlayer', player.data);
			// If the connection is closed immediately, the message is not sent.
			setTimeout(() => {
				Connection.instance.closeConnection(player.peerId);
			}, 100);
		}
	},
	/**
	 * @param {string} peerId
	 * @returns {Player|null}
	 */
	getPlayerByPeerId(peerId) {
		let result = null;
		this.players.forEach((player) => {
			if (player.peerId === peerId) {
				result = player;
			}
		});
		return result;
	},
	initPawns() {
		Models.pawnsArray.forEach((pawnModel) => {
			let pawn = this.getPawn(pawnModel.code);
			if (!pawn) {
				this.pawns.push({ code: pawnModel.code, count: pawnModel.defaultCount });
			}
		});
	},
	initNeutralPawns() {
		Models.neutralPawnsArray.forEach((pawnModel) => {
			let pawn = this.getNeutralPawn(pawnModel.code);
			if (!pawn) {
				this.neutralPawns.push({ code: pawnModel.code, count: pawnModel.defaultCount });
			}
		});
	},
	initTokens() {
		Models.tokensArray.forEach((tokenModel) => {
			let token = this.getPawn(tokenModel.code);
			if (!token) {
				this.tokens.push({ code: tokenModel.code, count: tokenModel.defaultCount });
			}
		});
	},
	initFeatures() {
		Models.featuresArray.forEach((featureModel) => {
			this.features.push({
				model: featureModel, code: featureModel.code, enabled: true,
				tiles: [], startingHand: [], additionalStartTiles: [], messages: []
			});
		});
	},
	/**
	 * @param {string} code
	 * @param {number} value
	 */
	setPawnCount(code, value) {
		let pawn = this.getPawn(code);
		if (pawn) {
			pawn.count = value;
		}
	},
	/**
	 * @param {string} code
	 * @param {number} value
	 */
	setNeutralPawnCount(code, value) {
		let pawn = this.getNeutralPawn(code);
		if (pawn) {
			pawn.count = value;
		}
	},
	/**
	 * @param {string} code
	 * @param {number} value
	 */
	setTokenCount(code, value) {
		let token = this.getToken(code);
		if (token) {
			token.count = value;
		}
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getPawn(code) {
		let result = null;
		this.pawns.forEach((pawn) => {
			if (pawn.code === code) {
				result = pawn;
			}
		});
		return result;
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getNeutralPawn(code) {
		let result = null;
		this.neutralPawns.forEach((pawn) => {
			if (pawn.code === code) {
				result = pawn;
			}
		});
		return result;
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getToken(code) {
		let result = null;
		this.tokens.forEach((token) => {
			if (token.code === code) {
				result = token;
			}
		});
		return result;
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getFeature(code) {
		let result = null;
		this.features.forEach((feature) => {
			if (feature.code === code) {
				result = feature;
			}
		});
		return result;
	},
	initSelection() {
		Models.extensionsArray.forEach((extension) => {
			let item = {
				code: extension.code,
				extension: extension,
				tiles: [],
				startingHand: [],
				additionalStartTiles: [],
				messages: [],
				boardEvents: []
			};

			extension.tiles.forEach((tileModel) => {
				let tileData = {
					code: tileModel.code,
					tile: tileModel,
					enabled: true,
					selected: true,
					quantity: tileModel.defaultQuantity
				};
				item.tiles.push(tileData);

				let startingHandTileData = {
					code: tileModel.code,
					tile: tileModel,
					enabled: true,
					selected: !!tileModel.startingHand,
					quantity: Math.max(1, tileModel.startingHand)
				}
				item.startingHand.push(startingHandTileData);

				if (tileModel.start) {
					this.startTileOptions.push(tileModel);
				}

				let additionalStartTileData = null;
				if (tileModel.additionalStart) {
					additionalStartTileData = {
						code: tileModel.code,
						tile: tileModel,
						enabled: true,
						selected: false,
						quantity: 1
					};
					item.additionalStartTiles.push(additionalStartTileData);
				}

				this.features.forEach((featureItem) => {
					if (tileModel.containsFeature(featureItem.code) || tileModel.refersFeature(featureItem.code)) {
						featureItem.tiles.push(tileData);
						featureItem.startingHand.push(startingHandTileData);
						if (additionalStartTileData) {
							// noinspection JSCheckFunctionSignatures
							featureItem.additionalStartTiles.push(additionalStartTileData);
						}
					}
				});
			});

			extension.messages.forEach((messageModel) => {
				let messageData = {
					code: messageModel.code,
					message: messageModel,
					enabled: true,
					selected: true,
					quantity: messageModel.defaultQuantity
				};
				item.messages.push(messageData);

				this.features.forEach((featureItem) => {
					if (messageModel.refersFeature(featureItem.code)) {
						featureItem.messages.push(messageData);
					}
				});
			});

			extension.boardEvents.forEach((boardEventModel) => {
				let eventData = {
					code: boardEventModel.code,
					event: boardEventModel,
					enabled: true,
					selected: true,
					quantity: boardEventModel.defaultQuantity
				};
				item.boardEvents.push(eventData);
			});

			this.selection.push(item);
		});
		this.startTile = this.startTileOptions[0];
	},
	refreshFeature(feature) {
		let refreshTiles = (tilesData) => {
			tilesData.forEach((tileData) => {
				let enabled = true;
				this.features.forEach((featureItem) => {
					if (tileData.tile.containsFeature(featureItem.code) || tileData.tile.refersFeature(featureItem.code)) {
						enabled = enabled && featureItem.enabled;
					}
				});
				tileData.enabled = enabled;
				if (!enabled) {
					tileData.selected = false;
				}
			});
		};

		refreshTiles(feature.tiles);
		refreshTiles(feature.startingHand);
		refreshTiles(feature.additionalStartTiles);
		feature.messages.forEach((messageData) => {
			let enabled = true;
			this.features.forEach((featureItem) => {
				if (messageData.message.refersFeature(featureItem.code)) {
					enabled = enabled && featureItem.enabled;
				}
			});
			messageData.enabled = enabled;
			if (!enabled) {
				messageData.selected = false;
			}
		});
	},
	//region Tile pool selection.
	selectAllTiles() {
		this.selection.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				tileItem.selected = true;
			});
		});
	},
	deselectAllTiles() {
		this.selection.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				tileItem.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionTiles(extension) {
		extension.tiles.forEach((tileItem) => {
			tileItem.selected = true;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionTiles(extension) {
		extension.tiles.forEach((tileItem) => {
			tileItem.selected = false;
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	selectTilesWithFeature(featureModel) {
		this.selection.forEach((extension) => {
			extension.tiles.forEach((item) => {
				if (item.tile.containsFeature(featureModel.code) || item.tile.refersFeature(featureModel.code)) {
					item.selected = item.enabled;
				}
			});
			extension.messages.forEach((item) => {
				if (item.message.refersFeature(featureModel.code)) {
					item.selected = item.enabled;
				}
			});
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	deselectTilesWithFeature(featureModel) {
		this.selection.forEach((extension) => {
			extension.tiles.forEach((item) => {
				if (item.tile.containsFeature(featureModel.code) || item.tile.refersFeature(featureModel.code)) {
					item.selected = false;
				}
			});
			extension.messages.forEach((item) => {
				if (item.message.refersFeature(featureModel.code)) {
					item.selected = false;
				}
			});
		});
	},
	//endregion
	//region Stating hand selection
	selectStartingHandAllTiles() {
		this.selection.forEach((extension) => {
			extension.startingHand.forEach((item) => {
				item.selected = item.enabled;
			});
		});
	},
	deselectStartingHandAllTiles() {
		this.selection.forEach((extension) => {
			extension.startingHand.forEach((item) => {
				item.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectStartingHandExtensionTiles(extension) {
		extension.startingHand.forEach((item) => {
			item.selected = item.enabled;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectStartingHandExtensionTiles(extension) {
		extension.startingHand.forEach((item) => {
			item.selected = false;
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	selectStartingHandTilesWithFeature(featureModel) {
		this.selection.forEach((extension) => {
			extension.startingHand.forEach((item) => {
				if (item.tile.containsFeature(featureModel.code) || item.tile.refersFeature(featureModel.code)) {
					item.selected = item.enabled;
				}
			});
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	deselectStartingHandTilesWithFeature(featureModel) {
		this.selection.forEach((extension) => {
			extension.startingHand.forEach((item) => {
				if (item.tile.containsFeature(featureModel.code) || item.tile.refersFeature(featureModel.code)) {
					item.selected = false;
				}
			});
		});
	},
	//endregion
	//region Message pool selection.
	selectAllMessages() {
		this.selection.forEach((extension) => {
			extension.messages.forEach((item) => {
				item.selected = item.enabled;
			});
		});
	},
	deselectAllMessages() {
		this.selection.forEach((extension) => {
			extension.messages.forEach((item) => {
				item.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionMessages(extension) {
		extension.messages.forEach((item) => {
			item.selected = item.enabled;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionMessages(extension) {
		extension.messages.forEach((item) => {
			item.selected = false;
		});
	},
	//endregion
	//region Board event pool selection.
	selectAllBoardEvents() {
		this.selection.forEach((extension) => {
			extension.boardEvents.forEach((item) => {
				item.selected = item.enabled;
			});
		});
	},
	deselectAllBoardEvents() {
		this.selection.forEach((extension) => {
			extension.boardEvents.forEach((item) => {
				item.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionBoardEvents(extension) {
		extension.boardEvents.forEach((item) => {
			item.selected = item.enabled;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionBoardEvents(extension) {
		extension.boardEvents.forEach((item) => {
			item.selected = false;
		});
	},
	//endregion
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getExtension(code) {
		let result = null;
		this.selection.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getTileData(extension, code) {
		let result = null;
		extension.tiles.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getStartingHandTileData(extension, code) {
		let result = null;
		extension.startingHand.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getAdditionalStartTileData(extension, code) {
		let result = null;
		extension.additionalStartTiles.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getMessageData(extension, code) {
		let result = null;
		extension.messages.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getBoardEventData(extension, code) {
		let result = null;
		extension.boardEvents.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @returns {TileModel[]}
	 */
	get additionalStartTileModels() {
		let selectedModels = [];
		this.selection.forEach((item) => {
			item.additionalStartTiles.forEach((tileItem) => {
				if (tileItem.selected) {
					for (let i = 0; i < tileItem.quantity; i++) {
						selectedModels.push(tileItem.tile);
					}
				}
			});
		});
		return selectedModels;
	},
	/**
	 * @returns {TileModel[]}
	 */
	get selectedTileModels() {
		let selectedModels = [];
		this.selection.forEach((item) => {
			item.tiles.forEach((tileItem) => {
				if (tileItem.selected) {
					for (let i = 0; i < tileItem.quantity; i++) {
						selectedModels.push(tileItem.tile);
					}
				}
			});
		});
		return selectedModels;
	},
	/**
	 * @returns {TileModel[]}
	 */
	get selectedStartingHandTiles() {
		let selectedModels = [];
		this.selection.forEach((item) => {
			item.startingHand.forEach((tileItem) => {
				if (tileItem.selected) {
					for (let i = 0; i < tileItem.quantity; i++) {
						selectedModels.push(tileItem.tile);
					}
				}
			});
		});
		return selectedModels;
	},
	/**
	 * @returns {MessageModel[]}
	 */
	get selectedMessageModels() {
		let selectedMessageModels = [];
		this.selection.forEach((item) => {
			item.messages.forEach((messageItem) => {
				if (messageItem.selected) {
					for (let i = 0; i < messageItem.quantity; i++) {
						selectedMessageModels.push(messageItem.message);
					}
				}
			});
		});
		return selectedMessageModels;
	},
	/**
	 * @returns {BoardEventModel[]}
	 */
	get selectedBoardEventModels() {
		let selectedBoardEventModels = [];
		this.selection.forEach((item) => {
			item.boardEvents.forEach((eventItem) => {
				if (eventItem.selected) {
					for (let i = 0; i < eventItem.quantity; i++) {
						selectedBoardEventModels.push(eventItem.event);
					}
				}
			});
		});
		return selectedBoardEventModels;
	},
	/**
	 * @returns {boolean}
	 */
	get isValid() {
		let isValid = false;
		Object.keys(this.pawns).forEach((code) => {
			if (this.pawns[code]) {
				isValid = true;
			}
		});

		let names = [];
		this.players.forEach((player) => {
			if (!player.name || !player.color || ArrayUtils.contains(names, player.name)) {
				isValid = false;
			}
			else {
				names.push(player.name);
			}
		});

		if (this.activateMessages && (this.messagePeriod < 1 || this.messagePoolSize < 1 || !this.selectedMessageModels.length)) {
			isValid = false;
		}

		if (this.activateBoardEvents && (this.boardEventPeriod < 1 || this.boardEventPoolSize < 1 || !this.selectedBoardEventModels.length)) {
			isValid = false;
		}

		return !!(isValid && this.poolSize > 1 && this.selectedTileModels.length);
	},
	/**
	 * @returns {Object}
	 */
	toData() {
		let data = {
			poolSize: this.poolSize,
			poolType: this.poolType,
			start: this.start,
			board: this.board,
			activateStartingHand: this.activateStartingHand,
			activateMessages: this.activateMessages,
			messagePeriod: this.messagePeriod,
			messagePoolSize: this.messagePoolSize,
			messagePoolType: this.messagePoolType,
			activateBoardEvents: this.activateBoardEvents,
			boardEventPeriod: this.boardEventPeriod,
			boardEventPoolSize: this.boardEventPoolSize,
			boardEventPoolType: this.boardEventPoolType,
			startTile: this.startTile.code,
			players: [],
			pawns: [],
			neutralPawns: [],
			tokens: [],
			features: [],
			selection: []
		};

		this.players.forEach((player) => {
			data.players.push({
				name: player.name,
				color: player.color.code,
				peerId: player.peerId
			});
		});

		this.pawns.forEach((pawn) => {
			data.pawns.push({
				code: pawn.code,
				count: pawn.count
			});
		});

		this.neutralPawns.forEach((pawn) => {
			data.neutralPawns.push({
				code: pawn.code,
				count: pawn.count
			});
		});

		this.tokens.forEach((token) => {
			data.tokens.push({
				code: token.code,
				count: token.count
			});
		});

		this.features.forEach((feature) => {
			data.features.push({
				code: feature.code,
				enabled: feature.enabled
			});
		});

		this.selection.forEach((item) => {
			let itemData = {
				code: item.code,
				tiles: [],
				startingHand: [],
				additionalStartTiles: [],
				messages: [],
				boardEvents: []
			};

			item.tiles.forEach((tileItem) => {
				itemData.tiles.push({
					code: tileItem.code,
					selected: tileItem.selected,
					quantity: tileItem.quantity
				});
			});

			item.startingHand.forEach((tileItem) => {
				itemData.startingHand.push({
					code: tileItem.code,
					selected: tileItem.selected,
					quantity: tileItem.quantity
				});
			});

			item.additionalStartTiles.forEach((tileItem) => {
				itemData.additionalStartTiles.push({
					code: tileItem.code,
					selected: tileItem.selected,
					quantity: tileItem.quantity
				});
			});

			item.messages.forEach((messageItem) => {
				itemData.messages.push({
					code: messageItem.code,
					selected: messageItem.selected,
					quantity: messageItem.quantity
				});
			});

			item.boardEvents.forEach((boardEventItem) => {
				itemData.boardEvents.push({
					code: boardEventItem.code,
					selected: boardEventItem.selected,
					quantity: boardEventItem.quantity
				});
			});

			data.selection.push(itemData);
		});

		data.optionalRules = {
			extraTurnWhenFillHole: this.optionalRules.extraTurnWhenFillHole,
			allowToGetBackPawn: this.optionalRules.allowToGetBackPawn,
			allowPoolDetailView: this.optionalRules.allowPoolDetailView,
			allowAddTileToHand: this.optionalRules.allowAddTileToHand,
			showMessagesEstimation: this.optionalRules.showMessagesEstimation
		};

		return data;
	},
	/**
	 * @param {Object|null} data
	 */
	fromData(data) {
		if (!data) {
			return;
		}

		if (data.hasOwnProperty('poolSize')) {
			this.poolSize = data.poolSize;
		}

		if (data.hasOwnProperty('poolType')) {
			this.poolType = data.poolType;
		}

		if (data.hasOwnProperty('start')) {
			this.start = data.start;
		}

		if (data.hasOwnProperty('board')) {
			this.board = data.board;
		}

		if (data.hasOwnProperty('activateStartingHand')) {
			this.activateStartingHand = !!data.activateStartingHand;
		}

		if (data.hasOwnProperty('activateMessages')) {
			this.activateMessages = !!data.activateMessages;
		}

		if (this.activateMessages) {
			if (data.hasOwnProperty('messagePeriod')) {
				this.messagePeriod = data.messagePeriod;
			}
			if (data.hasOwnProperty('messagePoolSize')) {
				this.messagePoolSize = data.messagePoolSize;
			}
			if (data.hasOwnProperty('messagePoolType')) {
				this.messagePoolType = data.messagePoolType;
			}
		}

		if (this.activateBoardEvents) {
			if (data.hasOwnProperty('boardEventPeriod')) {
				this.boardEventPeriod = data.boardEventPeriod;
			}
			if (data.hasOwnProperty('boardEventPoolSize')) {
				this.boardEventPoolSize = data.boardEventPoolSize;
			}
			if (data.hasOwnProperty('boardEventPoolType')) {
				this.boardEventPoolType = data.boardEventPoolType;
			}
		}

		if (data.hasOwnProperty('startTile')) {
			Models.extensionsArray.forEach((extension) => {
				extension.tiles.forEach((tileModel) => {
					if (data.startTile === tileModel.code) {
						this.startTile = tileModel;
					}
				});
			});
		}

		if (data.hasOwnProperty('players')) {
			ArrayUtils.clear(Setup.players);
			data.players.forEach((item) => {
				this.players.push(new Player(item.name, Colors[item.color], item.peerId));
			});
		}

		if (data.hasOwnProperty('pawns')) {
			data.pawns.forEach((item) => {
				this.setPawnCount(item.code, item.count);
			});
		}

		if (data.hasOwnProperty('neutralPawns')) {
			data.neutralPawns.forEach((item) => {
				this.setNeutralPawnCount(item.code, item.count);
			});
		}

		if (data.hasOwnProperty('tokens')) {
			data.tokens.forEach((item) => {
				this.setTokenCount(item.code, item.count);
			});
		}

		if (data.hasOwnProperty('features')) {
			data.features.forEach((item) => {
				let feature = this.getFeature(item.code);
				if (feature) {
					feature.enabled = item.enabled;
				}
			});
		}

		if (data.hasOwnProperty('selection')) {
			data.selection.forEach((extensionItem) => {
				let extensionData = this.getExtension(extensionItem.code);
				if (extensionData) {
					(extensionItem.tiles || []).forEach((tileItem) => {
						let tileData = this.getTileData(extensionData, tileItem.code);
						if (tileData) {
							tileData.selected = tileItem.selected;
							if (tileItem.selected && tileItem.hasOwnProperty('quantity')) {
								tileData.quantity = tileItem.quantity;
							}
						}
					});

					(extensionItem.startingHand || []).forEach((tileItem) => {
						let tileData = this.getStartingHandTileData(extensionData, tileItem.code);
						if (tileData) {
							tileData.selected = tileItem.selected;
							if (tileItem.selected && tileItem.hasOwnProperty('quantity')) {
								tileData.quantity = tileItem.quantity;
							}
						}
					});

					(extensionItem.additionalStartTiles || []).forEach((tileItem) => {
						let tileData = this.getAdditionalStartTileData(extensionData, tileItem.code);
						if (tileData) {
							tileData.selected = tileItem.selected;
							if (tileItem.selected && tileItem.hasOwnProperty('quantity')) {
								tileData.quantity = tileItem.quantity;
							}
						}
					});

					(extensionItem.messages || []).forEach((messageItem) => {
						let messageData = this.getMessageData(extensionData, messageItem.code);
						if (messageData) {
							messageData.selected = messageItem.selected;
							if (messageItem.selected && messageItem.hasOwnProperty('quantity')) {
								messageData.quantity = messageItem.quantity;
							}
						}
					});

					(extensionItem.boardEvents || []).forEach((eventItem) => {
						let eventData = this.getBoardEventData(extensionData, eventItem.code);
						if (eventData) {
							eventData.selected = eventItem.selected;
							if (eventItem.selected && eventItem.hasOwnProperty('quantity')) {
								eventData.quantity = eventItem.quantity;
							}
						}
					});
				}
			});
		}

		if (data.optionalRules) {
			if (data.optionalRules.hasOwnProperty('extraTurnWhenFillHole')) {
				this.optionalRules.extraTurnWhenFillHole = data.optionalRules.extraTurnWhenFillHole;
			}
			if (data.optionalRules.hasOwnProperty('allowToGetBackPawn')) {
				this.optionalRules.allowToGetBackPawn = data.optionalRules.allowToGetBackPawn;
			}
			if (data.optionalRules.hasOwnProperty('allowPoolDetailView')) {
				this.optionalRules.allowPoolDetailView = data.optionalRules.allowPoolDetailView;
			}
			if (data.optionalRules.hasOwnProperty('allowAddTileToHand')) {
				this.optionalRules.allowAddTileToHand = data.optionalRules.allowAddTileToHand;
			}
			if (data.optionalRules.hasOwnProperty('showMessagesEstimation')) {
				this.optionalRules.showMessagesEstimation = data.optionalRules.showMessagesEstimation;
			}
		}

		if (this.waitingForServerInit) {
			this.waitingForServerInit = false;
			this.loadPlayerData();
		}
	},
	/**
	 * @returns {boolean}
	 */
	hasLast() {
		return LocalStorage.isset('lastGameSetup');
	},
	saveLast() {
		let data = this.toData();
		delete data.players;
		LocalStorage.setObject('lastGameSetup', data);
	},
	loadLast() {
		this.fromData(LocalStorage.getObject('lastGameSetup'));
	},
	/**
	 * @param {Player} player
	 */
	savePlayerData(player) {
		LocalStorage.setObject('lastPlayerData', { name: player.name, color: player.color.code });
		alert(I18n.trans('c.setup.player_data_saved'));
	},
	loadPlayerData() {
		let playerData = LocalStorage.getObject('lastPlayerData');
		if (playerData && playerData.name) {
			let player = this.getPlayerByPeerId(Connection.id);
			if (player) {
				let name = playerData.name;
				let nameAvailable = true;
				this.players.forEach((item) => {
					if (item.peerId !== player.peerId && item.name === name) {
						nameAvailable = false;
					}
				});
				if (nameAvailable) {
					player.name = name;
				}
				else {
					let options = {
						substitutions: { name: name },
						icon: 'images/icons/conflict-same.svg'
					};
					if (Preferences.getInstance().playSounds) {
						options.sound = Sounds.newAlert;
					}
					Alerts.warning('c.setup.alert_name_already_used', options);
				}

				let color = Color.getByCode(playerData.color);
				if (color) {
					let colorAvailable = true;
					this.players.forEach((item) => {
						if (item.peerId !== player.peerId && item.color.code === color.code) {
							colorAvailable = false;
						}
					});
					if (colorAvailable) {
						player.color = color;
					}
					else {
						let options = {
							substitutions: { name: color.name },
							icon: 'images/icons/conflict-same.svg'
						};
						if (Preferences.getInstance().playSounds) {
							options.sound = Sounds.newAlert;
						}
						Alerts.warning('c.setup.alert_color_already_used', options);
					}
				}
			}
		}
	}
};

/**
 * @param {Function} callback
 */
function callIfInSetup(callback) {
	if (System.router.currentRoute.path === '/setup') {
		callback();
	}
	else {
		console.log('Not in setup, ignore the listener.');
	}
}

//region Event listeners.
document.addEventListener('tcConnectionNewClient', (event) => {
	callIfInSetup(() => {
		Setup.addPlayer(event.detail.peerId);
		Connection.sendMessage('SetupData', Setup.toData());

		setTimeout(() => {
			Setup.players.forEach((player) => {
				if (player.peerId === event.detail.peerId) {
					let options = {
						substitutions: { name: player.name },
						icon: 'images/icons/new-player.svg'
					};
					if (Preferences.getInstance().playSounds) {
						options.sound = Sounds.newAlert;
					}
					Alerts.info('c.setup.alert_player_joined_the_game', options);
				}
			});
		}, 3000);
	});
});

document.addEventListener('tcConnectionSetupData', (event) => {
	callIfInSetup(() => {
		let player = Setup.getPlayerByPeerId(Connection.id);
		if (player) {
			event.detail.data.players.forEach((playerData) => {
				if (playerData.peerId === Connection.id) {
					playerData.name = player.name;
					playerData.color = player.color.code;
				}
			});
		}
		Setup.fromData(event.detail.data);
	});
});

document.addEventListener('tcConnectionSetupUpdatePlayer', (event) => {
	callIfInSetup(() => {
		let player = Setup.getPlayerByPeerId(event.detail.data.peerId);
		if (player) {
			player.name = event.detail.data.name;
			player.color = Color.getByCode(event.detail.data.color);
		}
	});
});
//endregion