/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @typedef {Object} RankingItem
 * @property {Player} player
 * @property {number} score
 * @property {number|null} position
 */

export class Ranking {
	//region Static stuff.
	/**
	 * @param {Game} game
	 * @param {string} code
	 * @returns {Ranking}
	 */
	static buildForTokenCode(game, code) {
		let ranking = new Ranking();
		game.players.forEach((player) => {
			ranking.setPlayerScore(player, player.getTokenCount(code));
		});
		return ranking;
	}

	//endregion

	/** @type {boolean} */
	evaluated = false;
	/** @type {RankingItem[]} */
	scores = [];

	/**
	 * @param {Player} player
	 * @param {number} score
	 */
	setPlayerScore(player, score) {
		this.scores.push({ player: player, score: score, position: null });
	}

	/**
	 * @param {Player} player
	 * @return {number|null}
	 */
	getPlayerPosition(player) {
		if (!this.evaluated) {
			this.evaluate();
		}

		let position = null;
		this.scores.forEach((item) => {
			if (item.player === player) {
				position = item.position;
			}
		});
		return position;
	}

	/**
	 * @param {number} position
	 * @returns {Player[]}
	 */
	getPlayersByPosition(position) {
		if (!this.evaluated) {
			this.evaluate();
		}

		let result = [];
		this.scores.forEach((item) => {
			if (item.position === position) {
				result.push(item.player);
			}
		});
		return result;
	}

	evaluate() {
		this.scores.sort((a, b) => {
			if (a.score > b.score) {
				return -1;
			}
			if (a.score < b.score) {
				return 1;
			}
			return 0;
		});

		let lastPosition = 0;
		let lastScore = null;
		this.scores.forEach((item) => {
			if (item.score !== lastScore) {
				lastPosition++;
				lastScore = item.score;
			}
			item.position = lastPosition;
		});
	}
}