/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Alerts, ArrayUtils, DateUtils, LocalStorage, MathUtils, Timeline, TimelineItemTrigger } from './core.mjs';
import { Models } from './models.mjs';
import { Board, PlacementSuggestion, Zones } from './board.mjs';
import { Feature, Tile } from './tiles.mjs';
import { MessageEvaluator, PawnEvaluator, Strategies, Structure, TokenEvaluator } from './strategies.mjs';
import { Player } from './players.mjs';
import { Setup } from './setup.mjs';
import { Connection } from "./connection.mjs";
import { Preferences } from "./preferences.mjs";
import { BoxDirectory, FeatureDirectory, PawnDirectory, PlayerDirectory } from "./directories.mjs";
import { NeutralPawn, PlayerPawn } from "./pawns.mjs";
import { Ranking } from "./ranking.mjs";
import { Sounds } from "./sounds.mjs";
import { BoardEffectStrategy } from "../strategies/board-effects.mjs";

let document = window.document;

/**
 * @typedef {Object} SerializedTilePool
 * @property {string[]} tilesCodes
 * @property {string[]} startTileCodes
 */

export class TilePool {
	/** @type {Tile[]} */
	startTiles = [];

	/** @type {Tile[]} */
	tiles = [];

	/**
	 * @param {TileModel[]=} startTiles
	 * @param {TileModel[]=} selectedTileModels
	 * @param {string=} poolType - 'selection' or 'random'
	 * @param {number|null=} poolSize
	 */
	constructor(startTiles, selectedTileModels, poolType, poolSize) {
		if (startTiles) {
			startTiles.forEach((tile) => {
				this.startTiles.push(new Tile(tile));
			});
		}

		if (selectedTileModels && poolType) {
			if (poolType === 'selection') {
				selectedTileModels.forEach((model) => {
					this.addTile(model);
				});
			}
			else if (poolType === 'random') {
				for (let i = 0; i < poolSize; i++) {
					this.addTile(ArrayUtils.randomItem(selectedTileModels));
				}
			}
			else {
				console.error('[TilePool.constructor] Unknown poolType:', poolType);
			}
		}
	}

	/**
	 * @returns {SerializedTilePool}
	 */
	get data() {
		let data = {
			startTileCodes: [],
			tilesCodes: []
		};

		this.startTiles.forEach((tile) => {
			data.startTileCodes.push(tile.code);
		});
		this.tiles.forEach((tile) => {
			data.tilesCodes.push(tile.code);
		});

		return data;
	}

	/**
	 * @param {SerializedTilePool} data
	 */
	set data(data) {
		this.startTiles = [];
		data.startTileCodes.forEach((code) => {
			this.startTiles.push(new Tile(Models.getTileByCode(code)));
		});

		this.tiles = [];
		data.tilesCodes.forEach((code) => {
			this.addTile(code);
		});
	}

	/**
	 * @param {TileModel|string} model
	 */
	addTile(model) {
		if (typeof (model) === 'string') {
			model = Models.getTileByCode(model);
		}
		this.tiles.push(new Tile(model));
	}

	/**
	 * @returns {boolean}
	 */
	isEmpty() {
		return this.tiles.length === 0;
	}

	/**
	 * @returns {Tile}
	 */
	draw() {
		return this.tiles.splice(ArrayUtils.randomIndex(this.tiles), 1)[0];
	}

	/**
	 * @param {Tile} tile
	 * @returns {Tile}
	 */
	redraw(tile) {
		let newTile = this.draw();
		this.tiles.push(tile);
		return newTile;
	}
}

/**
 * @typedef {Object} SerializedMessagePool
 * @property {number} period
 * @property {string[]} remainingCodes
 * @property {string[]} usedCodes
 */

export class MessagePool {
	/** @type {number} */
	period = 5;

	/** @type {MessageModel[]} */
	remainingMessages = [];

	/** @type {MessageModel[]} */
	usedMessages = [];

	/**
	 * @param {MessageModel[]=} selectedMessageModels
	 * @param {string=} poolType - 'selection' or 'random'
	 * @param {number|null=} poolSize
	 * @param {number|null=} period
	 */
	constructor(selectedMessageModels, poolType, poolSize, period) {
		if (period) {
			this.period = period;
		}

		if (selectedMessageModels && poolType) {
			if (poolType === 'selection') {
				selectedMessageModels.forEach((model) => {
					this.addRemainingMessage(model);
				});
			}
			else if (poolType === 'random') {
				for (let i = 0; i < poolSize; i++) {
					this.addRemainingMessage(ArrayUtils.randomItem(selectedMessageModels));
				}
			}
			else {
				console.error('[MessagePool.constructor] Unknown poolType:', poolType);
			}
		}
	}

	/**
	 * @returns {SerializedMessagePool}
	 */
	get data() {
		let data = {
			period: this.period,
			remainingCodes: [],
			usedCodes: []
		};
		this.remainingMessages.forEach((messageModel) => {
			data.remainingCodes.push(messageModel.code);
		});
		this.usedMessages.forEach((messageModel) => {
			data.usedCodes.push(messageModel.code);
		});
		return data;
	}

	/**
	 * @param {SerializedMessagePool} data
	 */
	set data(data) {
		this.period = data.period;
		this.remainingMessages = [];
		this.usedMessages = [];
		data.remainingCodes.forEach((code) => {
			this.addRemainingMessage(code);
		});
		data.usedCodes.forEach((code) => {
			this.addUsedMessage(code);
		});
	}

	/**
	 * @param {MessageModel|string} model
	 */
	addRemainingMessage(model) {
		if (typeof (model) === 'string') {
			model = Models.getMessageByCode(model);
		}
		this.remainingMessages.push(model);
	}

	/**
	 * @param {MessageModel|string} model
	 */
	addUsedMessage(model) {
		if (typeof (model) === 'string') {
			model = Models.getMessageByCode(model);
		}
		this.usedMessages.push(model);
	}

	/**
	 * @returns {MessageModel}
	 */
	draw() {
		// When all messages are used, refill the pool.
		if (this.remainingMessages.length === 0) {
			this.remainingMessages = this.usedMessages;
			this.usedMessages = [];
		}

		let message = this.remainingMessages.splice(ArrayUtils.randomIndex(this.remainingMessages), 1)[0];
		this.addUsedMessage(message);
		return message;
	}
}

/**
 * @typedef {Object} SerializedBoardEventPool
 * @property {number} period
 * @property {string[]} remainingCodes
 * @property {string[]} usedCodes
 */

export class BoardEventPool {
	/** @type {number} */
	period = 5;

	/** @type {BoardEventModel[]} */
	remainingEvents = [];

	/** @type {BoardEventModel[]} */
	usedEvents = [];

	/**
	 * @param {BoardEventModel[]=} selectedBoardEventModels
	 * @param {string=} poolType - 'selection' or 'random'
	 * @param {number|null=} poolSize
	 * @param {number|null=} period
	 */
	constructor(selectedBoardEventModels, poolType, poolSize, period) {
		if (period) {
			this.period = period;
		}

		if (selectedBoardEventModels && poolType) {
			if (poolType === 'selection') {
				selectedBoardEventModels.forEach((model) => {
					this.addRemainingEvent(model);
				});
			}
			else if (poolType === 'random') {
				for (let i = 0; i < poolSize; i++) {
					this.addRemainingEvent(ArrayUtils.randomItem(selectedBoardEventModels));
				}
			}
			else {
				console.error('[BoardEventPool.constructor] Unknown poolType:', poolType);
			}
		}
	}

	/**
	 * @returns {SerializedBoardEventPool}
	 */
	get data() {
		let data = {
			period: this.period,
			remainingCodes: [],
			usedCodes: []
		};
		this.remainingEvents.forEach((eventModel) => {
			data.remainingCodes.push(eventModel.code);
		});
		this.usedEvents.forEach((eventModel) => {
			data.usedCodes.push(eventModel.code);
		});
		return data;
	}

	/**
	 * @param {SerializedBoardEventPool} data
	 */
	set data(data) {
		this.period = data.period;
		this.remainingEvents = [];
		this.usedEvents = [];
		data.remainingCodes.forEach((code) => {
			this.addRemainingEvent(code);
		});
		data.usedCodes.forEach((code) => {
			this.addUsedEvent(code);
		});
	}

	/**
	 * @param {BoardEventModel|string} model
	 */
	addRemainingEvent(model) {
		if (typeof (model) === 'string') {
			model = Models.getBoardEventByCode(model);
		}
		this.remainingEvents.push(model);
	}

	/**
	 * @param {BoardEventModel|string} model
	 */
	addUsedEvent(model) {
		if (typeof (model) === 'string') {
			model = Models.getBoardEventByCode(model);
		}
		this.usedEvents.push(model);
	}

	/**
	 * @returns {BoardEventModel}
	 */
	draw() {
		// When all messages are used, refill the pool.
		if (this.remainingEvents.length === 0) {
			this.remainingEvents = this.usedEvents;
			this.usedEvents = [];
		}

		let event = this.remainingEvents.splice(ArrayUtils.randomIndex(this.remainingEvents), 1)[0];
		this.addUsedEvent(event);
		return event;
	}
}

/**
 * @typedef {Object} SerializedNeutralPawnPool
 * @property {Object} pawns
 */

export class NeutralPawnPool {
	/** @type {Object} */
	pawns = {};

	/**
	 * @param {Object=} neutralPawns
	 */
	constructor(neutralPawns) {
		if (neutralPawns) {
			neutralPawns.forEach((item) => {
				this.addNewPawns(item.code, item.count);
			});
		}
	}

	/**
	 * @returns {SerializedNeutralPawnPool}
	 */
	get data() {
		let data = {
			pawns: []
		};
		Object.keys(this.pawns).forEach((code) => {
			data.pawns.push({ code: code, count: this.pawns[code].length });
		});
		return data;
	}

	/**
	 * @param {SerializedNeutralPawnPool} data
	 */
	set data(data) {
		this.pawns = [];
		if (data.pawns) {
			data.pawns.forEach((item) => {
				this.addNewPawns(item.code, item.count);
			});
		}
	}

	/**
	 * @param {NeutralPawn} pawn
	 */
	addPawn(pawn) {
		if (!this.pawns[pawn.code]) {
			this.pawns[pawn.code] = [];
		}
		this.pawns[pawn.code].push(pawn);
	}

	/**
	 * @param {string} code
	 * @param {number} count
	 */
	addNewPawns(code, count) {
		let model = Models.getNeutralPawnByCode(code);
		for (let i = 0; i < count; i++) {
			this.addPawn(new NeutralPawn(model));
		}
	}

	/**
	 * @param {string} code
	 * @return {NeutralPawn}
	 */
	popPawn(code) {
		return this.pawns[code].splice(0, 1)[0];
	}

	/**
	 * @param {string} subType
	 * @return {NeutralPawn|null}
	 */
	popPawnBySubType(subType) {
		let filteredPool = [];
		Object.keys(this.pawns).forEach((code) => {
			let model = Models.getNeutralPawnByCode(code);
			if (model && model.subType === subType) {
				filteredPool = ArrayUtils.merge(filteredPool, this.pawns[code]);
			}
		});

		if (filteredPool.length) {
			let code = ArrayUtils.randomItem(filteredPool).code;
			return this.pawns[code].splice(0, 1)[0];
		}
		return null;
	}
}

/**
 * @typedef {Object} SerializedTokenPool
 * @property {Object} tokens Token counts by code.
 */

export class TokensPool {
	/** @type {Object} */
	tokens = {};

	/**
	 * @param {Object=} tokens
	 */
	constructor(tokens) {
		if (tokens) {
			tokens.forEach((item) => {
				this.addTokens(item.code, item.count);
			});
		}
	}

	/**
	 * @returns {SerializedTokenPool}
	 */
	get data() {
		return {
			tokens: this.tokens
		};
	}

	/**
	 * @param {SerializedTokenPool} data
	 */
	set data(data) {
		this.tokens = data.tokens;
	}

	/**
	 * @param {string} code
	 * @param {number} count
	 */
	addTokens(code, count) {
		this.tokens[code] = count;
	}

	/**
	 * @param {number} count
	 * @param {string[]} types
	 * @return {string[]}
	 */
	getRandomTokens(count, types) {
		let pool = [];

		if (types.length) {
			types.forEach((code) => {
				if (this.tokens[code]) {
					for (let i = 0; i < this.tokens[code]; i++) {
						pool.push(code);
					}
				}
			});
		}
		else {
			for (let code in this.tokens) {
				if (this.tokens.hasOwnProperty(code)) {
					for (let i = 0; i < this.tokens[code]; i++) {
						pool.push(code);
					}
				}
			}
		}

		let tokens = [];
		for (let i = 0; i < count; i++) {
			tokens.push(ArrayUtils.randomItem(pool));
		}
		return tokens;
	}
}

export class TurnSteps {
	static BEGIN = 'begin';
	static PLAY_FROM_HAND = 'playFromHand';
	static PLAY_TILE = 'playTile';
	static PLAY_PAWN = 'playPawn';
	static END_MAIN_PHASE = 'endMainPhase';
	static APPLY_MAIN_PHASE_SPECIAL_STEPS = 'applyMainPhaseSpecialSteps';
	static CHECK_STRUCTURES = 'checkStructures';
	static CHECK_TRADE_RESOURCES = 'checkTradeResources';
	static TRADE_RESOURCES = 'tradeResources';
	static CHECK_MESSAGES = 'checkMessages';
	static APPLY_MESSAGE = 'applyMessage';
	static END = 'end';

	static regulars = [
		TurnSteps.BEGIN,
		TurnSteps.PLAY_FROM_HAND,
		TurnSteps.PLAY_TILE,
		TurnSteps.PLAY_PAWN,
		TurnSteps.END_MAIN_PHASE,
		TurnSteps.APPLY_MAIN_PHASE_SPECIAL_STEPS,
		TurnSteps.CHECK_STRUCTURES,
		TurnSteps.CHECK_TRADE_RESOURCES,
		TurnSteps.TRADE_RESOURCES,
		TurnSteps.CHECK_MESSAGES,
		TurnSteps.END
	];

	static specials = [
		TurnSteps.APPLY_MESSAGE
	]
}

/**
 * @typedef {Object} SerializedPendingSpecialStep
 * @property {string} name
 * @property {Object} parameters
 * @property {TimelineItemTriggerData|null} source
 */

class PendingSpecialStep {
	/**
	 * @param {string=} name
	 * @param {Object=} parameters
	 * @param {TimelineItemTrigger|null=} source
	 */
	constructor(name, parameters, source) {
		this.name = name;
		this.parameters = parameters;
		this.source = source;
	}

	/**
	 * @returns {SerializedPendingSpecialStep}
	 */
	get data() {
		return {
			name: this.name,
			parameters: this.parameters,
			source: this.source ? this.source.toJSON : null
		};
	}

	/**
	 * @param {SerializedPendingSpecialStep} data
	 */
	set data(data) {
		this.name = data.name;
		this.parameters = data.parameters;
		this.source = data.source ? TimelineItemTrigger.fromData(data.source) : null;
	}
}

/**
 * @typedef {Object} SerializedGame
 * @property {number} tableTurnNumber
 * @property {SerializedPlayer[]} players
 * @property {OptionalRules} optionalRules
 * @property {number} currentPlayerIndex
 * @property {number|null} currentPlayerStartingPoints
 * @property {string|null} currentTile
 * @property {string[]} drawnTiles
 * @property {CoordinatesData|null} lastPlayedBox
 * @property {SerializedPlayedPawn[]} lastPlayedPawns
 * @property {SerializedPlayedPawn[]} lastGotBackPawns
 * @property {SerializedPlayedNeutralPawn[]} lastPlayedNeutralPawns
 * @property {boolean} hasExtraTurn
 * @property {boolean} isExtraTurn
 * @property {string} turnStep
 * @property {string} turnRegularStep
 * @property {SerializedPendingSpecialStep[]} pendingSpecialSteps
 * @property {SerializedTilePool} tilePool
 * @property {SerializedMessagePool} messagePool
 * @property {SerializedBoard} board
 * @property {boolean} ended
 * @property {TimelineData} timeline
 */

/**
 * @typedef {Object} PlayedPawn
 * @property {PlayerPawn} pawn
 * @property {Feature} feature
 * @property {Box} box
 */

/**
 * @typedef {Object} SerializedPlayedPawn
 * @property {SerializedPawn} pawn
 * @property {number} featureIndex
 * @property {CoordinatesData} boxCoordinates
 */

/**
 * @typedef {Object} PlayedNeutralPawn
 * @property {NeutralPawn} pawn
 * @property {Feature} feature
 * @property {Box} box
 */

/**
 * @typedef {Object} SerializedPlayedNeutralPawn
 * @property {SerializedNeutralPawn} pawn
 * @property {number} featureIndex
 * @property {CoordinatesData} boxCoordinates
 */

/**
 * @typedef {Object} OptionalRules
 * @property {boolean} extraTurnWhenFillHole
 * @property {boolean} allowToGetBackPawn
 * @property {boolean} allowPoolDetailView
 * @property {boolean} allowAddTileToHand
 * @property {boolean} showMessagesEstimation
 */

/**
 * @typedef {Object} TradeOfferPart
 * @property {string} type
 * @property {string} resourceType 'RANDOM'|'CHOICE'|'<tokenCode>'
 * @property {number} quantity
 */

/**
 * @typedef {Object} TradeOffer
 * @property {TradeOfferPart} from
 * @property {TradeOfferPart} to
 */

/**
 * @typedef {Object} TradeOffers
 * @property {TradeOffer[]} offers
 * @property {TimelineItemTrigger} source
 */

class Game {
	/** @type {Date|null} */
	startDate = null;
	/** @type {number} */
	lastHourAlert = 0;
	/** @type {number} */
	tableTurnNumber = 1;
	/** @type {number} */
	playerTurnNumber = 1;
	/** @type {boolean} */
	initialized = false;
	/** @type {boolean} */
	announced = false;
	/** @type {boolean} */
	ended = false;
	/** @type {boolean} */
	restoringData = false;
	/** @type {SerializedGame|null} */
	dataToRestore = null;
	/** @type {Player[]} */
	players = [];
	/** @type {OptionalRules} */
	optionalRules = {};
	/** @type {number|null} */
	currentPlayerIndex = null;
	/** @type {Player|null} */
	currentPlayer = null;
	/** @type {number|null} */
	currentPlayerStartingPoints = null;
	/** @type {Tile|null} */
	currentTile = null;
	/** @type {TradeOffers[]} */
	currentTradeOffers = [];
	/** @type {Tile[]} */
	drawnTiles = [];
	/** @type {MessageEvaluator|null} */
	currentMessage = null;
	/** @type {Box|null} */
	lastPlayedBox = null;
	/** @type {PlayedPawn[]} */
	lastPlayedPawns = [];
	/** @type {PlayedPawn[]} */
	lastGotBackPawns = [];
	/** @type {PlayedNeutralPawn[]} */
	lastPlayedNeutralPawns = [];
	/** @type {boolean} */
	hasExtraTurn = false;
	/** @type {boolean} */
	isExtraTurn = false;
	/** @type {string} */
	turnStep = TurnSteps.BEGIN;
	/** @type {string} */
	turnRegularStep = TurnSteps.BEGIN;
	/** @type {PendingSpecialStep[]} */
	pendingSpecialSteps = [];
	/** @type {TilePool} */
	tilePool = null;
	/** @type {MessagePool} */
	messagePool = null;
	/** @type {BoardEventPool} */
	boardEventPool = null;
	/** @type {NeutralPawnPool} */
	neutralPawnPool = null;
	/** @type {TokensPool} */
	tokensPool = null;
	modals = {
		/** @type {Object|null} */
		choosePawn: null,
		/** @type {boolean} */
		timeline: false,
	};
	preferences = Preferences.getInstance();

	constructor() {
		setInterval(() => {
			if (this.startDate) {
				let minutes = DateUtils.getMinutesDifference(this.startDate, new Date());
				if (Math.floor(minutes / 60) > this.lastHourAlert) {
					this.lastHourAlert++;
					let options = {
						substitutions: { hours: this.lastHourAlert },
						icon: 'images/icons/clock.svg'
					};
					if (this.preferences.playSounds) {
						options.sound = Sounds.newAlert;
					}
					if (this.lastHourAlert === 1) {
						Alerts.info('c.main.alert_playing_since_one_hour', options);
					}
					else {
						Alerts.info('c.main.alert_playing_since_n_hours', options);
					}
				}
			}
		}, 1000);
	}

	/**
	 * @returns {null|number}
	 */
	get remainingTurnsBeforeBoardEvent() {
		if (!this.boardEventPool || !this.boardEventPool.period) {
			return null;
		}
		let modulo = this.playerTurnNumber % this.boardEventPool.period;
		return modulo ? this.boardEventPool.period - modulo : 0;
	}

	/**
	 * @param {string} newStep
	 */
	startRegularStep(newStep) {
		this.turnStep = newStep;
		this.turnRegularStep = newStep;
	}

	/**
	 * @param {string} newStep
	 */
	startSpecialStep(newStep) {
		this.turnStep = newStep;
	}

	/**
	 * @param {string} step
	 * @param {Object} parameters
	 * @param {TimelineItemTrigger} source
	 */
	pushSpecialStep(step, parameters, source) {
		this.pendingSpecialSteps.push(new PendingSpecialStep(step, parameters, source));
	}

	/**
	 * @returns {boolean}
	 */
	applyNextSpecialStep() {
		if (!this.pendingSpecialSteps.length) {
			return false;
		}

		let step = this.pendingSpecialSteps.shift();

		switch (step.name) {
			case 'applyMessage':
				if (this.messagePool) {
					this.prepareMessage(step.source);
					return true;
				}
				console.warn('[Game.applyNextSpecialStep] No message pool, ignore applyMessage special step.');
				return false;
		}
		return false;
	}

	/**
	 * @returns {PlayerPawn[]}
	 */
	getNewlyPlayedPawns() {
		let newlyPlayedPawns = [];
		this.lastPlayedPawns.forEach((playedPawn) => {
			newlyPlayedPawns.push(playedPawn.pawn);
		});
		return newlyPlayedPawns;
	}

	/**
	 * @returns {NeutralPawn[]}
	 */
	getNewlyPlayedNeutralPawns() {
		let newlyPlayedPawns = [];
		this.lastPlayedNeutralPawns.forEach((playedPawn) => {
			newlyPlayedPawns.push(playedPawn.pawn);
		});
		return newlyPlayedPawns;
	}

	/**
	 * @param {PlayerPawn} pawn
	 * @returns {number|null}
	 */
	getIndexInLastPlayedPawns(pawn) {
		let result = null;
		this.lastPlayedPawns.forEach((playedPawn, index) => {
			// If the pawn was played this turn, remove it from the list.
			if (playedPawn.pawn === pawn) {
				result = index;
			}
		});
		return result;
	}

	canPlayMainAction() {
		if (this.turnStep !== TurnSteps.PLAY_PAWN || game.lastGotBackPawns.length || game.lastPlayedPawns.length > 1) {
			return false;
		}
		return game.lastPlayedPawns.length === 0 || game.lastPlayedPawns[0].pawn.model.secondCompatible;
	}

	/**
	 * @returns {boolean}
	 */
	isActive() {
		if (!Connection.instance) { // Local game.
			return true;
		}
		if (this.currentPlayer) { // Active player.
			return Connection.id === this.currentPlayer.peerId;
		}
		return Connection.isServer(); // When there is no active player, the server can act.
	}

	/**
	 * @param {string=} excludedPeerId
	 */
	dispatch(excludedPeerId) {
		Connection.sendMessage('GameUpdate', this.data, excludedPeerId);
	}

	/**
	 * @param {string} step
	 * @returns {boolean}
	 */
	isStepOrAfter(step) {
		return TurnSteps.regulars.indexOf(this.turnRegularStep) >= TurnSteps.regulars.indexOf(step);
	}

	/**
	 * @param {TimelineItemTrigger} source
	 */
	gainExtraTurn(source) {
		if (!this.hasExtraTurn) {
			this.hasExtraTurn = true;
			if (source) {
				Timeline.log('c.timeline.player_gets_double_turn', 'effect', null, source, this.currentPlayer.name);
			}
		}
		else {
			this.currentPlayer.addPoints(2, source);
		}
	}

	/**
	 * @param {TradeOffer[]} offers
	 * @param {TimelineItemTrigger} source
	 */
	addTradeOffers(offers, source) {
		this.currentTradeOffers.push({ offers: offers, source: source });
	}

	draw() {
		this.currentTile = this.tilePool.draw();
		this.drawnTiles = [this.currentTile];

		this.startRegularStep(TurnSteps.PLAY_TILE);
		this.dispatch();
	}

	redraw() {
		if (!this.drawnTiles.length || !this.isActive()) {
			return;
		}

		if (this.currentTile) {
			if (!this.tilePool.isEmpty()) {
				this.currentTile = this.tilePool.redraw(this.currentTile);
				this.drawnTiles = [this.currentTile];
				Timeline.log('c.timeline.tile_redrawn', 'action', null, null, this.currentPlayer.name);
			}
			else {
				Timeline.log('c.timeline.redraw_aborted_empty_pool', 'error');
			}
		}
		else {
			Timeline.log('c.timeline.redraw_aborted_no_current_tile', 'error');
		}

		this.dispatch();
	}

	/**
	 * @param {number} quantity
	 * @param {TimelineItemTrigger} source
	 */
	drawToHand(quantity, source) {
		for (let i = 0; i < quantity; i++) {
			let tile = this.tilePool.draw();
			if (tile) {
				this.currentPlayer.gainTile(tile, source);
			}
		}
	}

	/**
	 * @param {number} index
	 */
	selectHandTile(index) {
		if (this.drawnTiles.length || this.turnStep !== TurnSteps.PLAY_FROM_HAND) {
			return;
		}
		this.currentTile = this.currentPlayer.tiles[index];
	}

	addCurrentTileToHand() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.PLAY_TILE) {
			console.error('[Game.addCurrentTileToHand] Invalid step:', this.turnStep);
			return;
		}
		PlacementSuggestion.clear();

		this.currentPlayer.addTile(this.currentTile);
		this.currentTile = null;

		// Timeline.
		Timeline.log('c.timeline.tile_added_to_hand', 'action', null, null, this.currentPlayer.name);

		this.startRegularStep(TurnSteps.END);
		this.endTurn();
	}

	/**
	 * @param {Box} box
	 * @param {boolean} ctrlPressed
	 */
	playTile(box, ctrlPressed) {
		if (!this.currentTile) {
			return;
		}

		if (ctrlPressed || !this.isActive()) {
			// If the player is not active, suggest placement.
			let peerPlayer = PlayerDirectory.getByPeerId(Connection.id);
			if (peerPlayer) {
				let suggestion = new PlacementSuggestion(box.coordinates, this.currentTile, peerPlayer);
				PlacementSuggestion.add(suggestion);
				Connection.sendMessage('TilePlacementSuggestion', suggestion.data);
			}
			else {
				console.warn('[Game.playTile] no peer player found for suggestion!');
			}
			return;
		}
		PlacementSuggestion.clear();

		Board.putTile(box, this.currentTile);
		this.currentTile = null;
		this.lastPlayedBox = box;

		// Timeline.
		Timeline.log('c.timeline.tile_placed_at', 'action', box.coordinates, null, this.currentPlayer.name);

		this.startRegularStep(TurnSteps.PLAY_PAWN);
		box.showFeatures(this.currentPlayer, this); // This must be done after setting the turn step.

		this.dispatch();
	}

	undoPlayTile() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.PLAY_PAWN) {
			console.error('[Game.undoPlayTile] Invalid step:', this.turnStep);
			return;
		}

		let box = this.lastPlayedBox;
		this.currentTile = box.tile;
		Board.removeTile(box);
		this.lastPlayedBox = null;
		box.hideFeatures();

		// Timeline.
		Timeline.log('c.timeline.tile_taken_back', 'action', null, null, this.currentPlayer.name);

		this.startRegularStep(this.drawnTiles.length ? TurnSteps.PLAY_TILE : TurnSteps.PLAY_FROM_HAND);
		this.dispatch();
	}

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {PlayerPawn=} pawn
	 * @param {Player=} player
	 */
	playPawn(feature, box, pawn, player) {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.PLAY_PAWN) {
			console.error('[Game.playPawn] Invalid step:', this.turnStep);
			return;
		}

		if (!player) {
			player = this.currentPlayer;
		}

		if (!pawn) {
			let pawns = player.getPlayablePawns(new Structure(feature, box), feature, this);
			if (pawns.length === 0) {
				return;
			}
			if (pawns.length > 1) {
				this.modals.choosePawn = {
					player: player,
					feature: feature,
					box: box,
					pawns: pawns
				};
				return;
			}

			pawn = player.popPawn(pawns[0].code);
		}

		feature.addPawn(pawn);
		box.refreshPawns();
		box.hideFeatures();

		this.lastPlayedPawns.push({ pawn: pawn, feature: feature, box: box });

		// Timeline.
		Timeline.log('c.timeline.pawn_placed_at', 'action', box.coordinates, null, this.currentPlayer.name);

		if (this.currentPlayer.getDifferentAvailablePawns(this).length) {
			box.showFeatures(this.currentPlayer, this);

			this.dispatch();
			return;
		}

		this.startRegularStep(TurnSteps.END_MAIN_PHASE);
		this.dispatch();
	}

	/**
	 * @param {PlayerPawn} pawn
	 * @return {boolean}
	 */
	canGetBack(pawn) {
		if (!this.isActive() || pawn.owner !== this.currentPlayer) {
			return false;
		}

		if (this.getIndexInLastPlayedPawns(pawn) !== null) {
			return (this.turnStep === TurnSteps.PLAY_PAWN || this.turnStep === TurnSteps.END_MAIN_PHASE);
		}

		return !!(this.optionalRules.allowToGetBackPawn && this.canPlayMainAction());
	}

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {PlayerPawn} pawn
	 */
	getBackPawn(feature, box, pawn) {
		if (!this.canGetBack(pawn)) {
			return;
		}

		let isMainAction = this.doGetBackPawn(feature, box, pawn);

		// Get back helpers if there is no more follower in the structure.
		if (pawn.model.type === 'follower') {
			let structure = new Structure(feature, box);
			if (!structure.hasPlayerPawnOfType(pawn.owner, 'follower')) {
				structure.getPlayerPawnsOfType(pawn.owner, 'helper').forEach((otherPawn) => {
					this.doGetBackPawn(structure.getFeatureByPawn(otherPawn), structure.getBoxByPawn(otherPawn), otherPawn);
				});
			}
		}

		// Timeline.
		if (!isMainAction) {
			Timeline.log('c.timeline.pawn_placement_canceled', 'action', box.coordinates, null, this.currentPlayer.name);
		}
		else {
			Timeline.log('c.timeline.pawn_got_back_from', 'action', box.coordinates, null, this.currentPlayer.name);
		}

		this.startRegularStep(TurnSteps.PLAY_PAWN);
		if (this.currentPlayer.getDifferentAvailablePawns(this).length) {
			this.lastPlayedBox.showFeatures(this.currentPlayer, this);
			return;
		}
		this.startRegularStep(TurnSteps.END_MAIN_PHASE);

		this.dispatch();
	}

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {PlayerPawn} pawn
	 * @returns {boolean} true if this counts as the main action.
	 */
	doGetBackPawn(feature, box, pawn) {
		feature.removePawn(pawn);
		pawn.back();
		box.refreshPawns();

		let lastPlayed = this.getIndexInLastPlayedPawns(pawn);

		// If the pawn was played this turn, remove it from the list.
		if (lastPlayed !== null) {
			this.lastPlayedPawns.splice(lastPlayed, 1);
			return false;
		}
		// If the pawn was not played this turn, this counts as the main action.
		else {
			this.lastPlayedBox.hideFeatures();
			this.lastGotBackPawns.push({ pawn: pawn, feature: feature, box: box });
			return true;
		}
	}

	undoPlayPawn() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.END_MAIN_PHASE && this.turnStep !== TurnSteps.PLAY_PAWN) {
			console.error('[Game.undoPlayPawn] Invalid step:', this.turnStep);
			return;
		}

		this.lastPlayedPawns.forEach((playedPawn) => {
			let pawn = playedPawn.pawn;
			playedPawn.feature.removePawn(pawn);
			pawn.back();
			playedPawn.box.refreshPawns();

			// Timeline.
			Timeline.log('c.timeline.pawn_placement_canceled', 'action', playedPawn.box.coordinates, null, this.currentPlayer.name);
		});
		this.lastPlayedPawns = [];

		this.lastGotBackPawns.forEach((playedPawn) => {
			let pawn = playedPawn.pawn.owner.popPawn(playedPawn.pawn.code);
			playedPawn.feature.addPawn(pawn);
			playedPawn.box.refreshPawns();

			// Register pawn effects.
			if (pawn.hasCompletion()) {
				let evaluator = new PawnEvaluator(pawn, playedPawn.feature, playedPawn.box);
				evaluator.register();
			}

			// Timeline.
			Timeline.log('c.timeline.pawn_retrieval_cancel', 'action', playedPawn.box.coordinates, null, this.currentPlayer.name);
		});
		this.lastGotBackPawns = [];

		this.lastPlayedBox.showFeatures(this.currentPlayer, this);

		this.startRegularStep(TurnSteps.PLAY_PAWN);
		this.dispatch();
	}

	endMainPhase() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnRegularStep !== TurnSteps.PLAY_PAWN && this.turnRegularStep !== TurnSteps.END_MAIN_PHASE) {
			console.error('[Game.endMainPhase] Invalid step:', this.turnStep);
			return;
		}

		this.startRegularStep(TurnSteps.CHECK_STRUCTURES);
		this.checkStructures();
	}

	checkStructures() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.CHECK_STRUCTURES) {
			console.error('[Game.checkStructures] Invalid step:', this.turnStep);
			return;
		}

		let box = this.lastPlayedBox;
		box.hideFeatures();

		// Handle "played" effects.
		let alreadyTestedFeatures = [];
		box.tile.features.forEach((feature) => {
			if (feature.model.played.length) {
				if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(alreadyTestedFeatures, structure.features);

				feature.model.played.forEach((playedData) => {
					if (Strategies.checkCondition(playedData.conditions || [], structure, this.currentPlayer, this)) {
						let strategy = Strategies.getEffect(playedData.type);
						let source = new TimelineItemTrigger('c.main.source_feature', box.coordinates, null, { feature: feature.name });
						strategy.apply(structure, feature, playedData.configuration || {}, this, null, source);
					}
				});
			}
		});

		// Handle completed pawns effects.
		this.lastPlayedPawns.forEach((playedPawn) => {
			let pawn = playedPawn.pawn;
			if (pawn.hasCompletion()) {
				let evaluator = new PawnEvaluator(pawn, playedPawn.feature, playedPawn.box);
				if (evaluator.isCompleted()) {
					evaluator.onCompleted(this);
				}
				else {
					evaluator.register();
				}
			}
		});

		Strategies.getRegisteredPawns(box.x, box.y).forEach((registeredPawn) => {
			let pawn = registeredPawn.pawn;
			if (pawn.hasCompletion()) {
				let evaluator = new PawnEvaluator(pawn, registeredPawn.feature, registeredPawn.box);
				if (evaluator.isCompleted()) {
					evaluator.onCompleted(this);
				}
			}
		});

		// Handle completed/continued structures effects.
		alreadyTestedFeatures = [];
		box.tile.features.forEach((feature) => {
			if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
				return;
			}

			let structure = new Structure(feature, box);
			ArrayUtils.merge(alreadyTestedFeatures, structure.features);

			// Check continued structure.
			if (structure.features.length > 0) {
				structure.onContinued(feature, this);
			}

			// Check completed structure.
			if (structure.isCompleted()) {
				structure.onCompleted(feature, this);
			}
			else {
				structure.register();
			}
		});

		box.neighbourFeatures.forEach((item) => {
			let neighbourFeature = item[0];
			let neighbourBox = item[1];
			if (ArrayUtils.contains(alreadyTestedFeatures, neighbourFeature)) {
				return;
			}

			let structure = new Structure(neighbourFeature, neighbourBox);
			ArrayUtils.merge(alreadyTestedFeatures, structure.features);

			// Check completed structures.
			structure.checkCompletion(neighbourFeature, this);
		});

		Strategies.getRegisteredFeatures(box.x, box.y).forEach((registeredFeature) => {
			let feature = registeredFeature[0];
			if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
				return;
			}

			let structure = new Structure(feature, registeredFeature[1]);
			ArrayUtils.merge(alreadyTestedFeatures, structure.features);

			// Check continued structure.
			structure.onContinued(feature, this);

			// Check completed structures.
			structure.checkCompletion(feature, this);
		});

		// Handle structure influence.
		Strategies.getRegisteredInfluence(box.x, box.y).forEach((registeredFeature) => {
			registeredFeature.feature.influence.forEach((influenceData) => {
				let influenceStrategy = Strategies.getInfluence(influenceData.type);
				if (influenceStrategy.checkZone(registeredFeature.box.coordinates, influenceData.zone, box.coordinates)) {
					let structure = new Structure(registeredFeature.feature, registeredFeature.box);
					influenceStrategy.apply(box, structure, influenceData.configuration, this, null);
				}
			});
		});

		Structure.getBoxesStructures([box], {}, []).forEach((structure) => {
			structure.initialFeature.model.influence.forEach((influenceData) => {
				Strategies.getInfluence(influenceData.type).register(structure, influenceData.zone);
			});
		});

		// Fill a hole gives an extra turn.
		if (this.optionalRules.extraTurnWhenFillHole) {
			let fillHole = true;
			box.neighbours.forEach((neighbour) => {
				if (neighbour.isEmpty) {
					fillHole = false;
				}
			});
			if (fillHole) {
				this.gainExtraTurn(new TimelineItemTrigger('c.main.source_filled_hole', box.coordinates));
			}
		}

		this.startRegularStep(TurnSteps.APPLY_MAIN_PHASE_SPECIAL_STEPS);
		this.mainPhaseSpecialSteps();
	}

	mainPhaseSpecialSteps() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnRegularStep !== TurnSteps.APPLY_MAIN_PHASE_SPECIAL_STEPS) {
			console.error('[Game.mainPhaseSpecialSteps] Invalid step:', this.turnStep);
			return;
		}

		if (!this.applyNextSpecialStep()) {
			this.startRegularStep(TurnSteps.CHECK_TRADE_RESOURCES);
			this.checkTradeResources();
		}
	}

	checkTradeResources() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.CHECK_TRADE_RESOURCES) {
			console.error('[Game.checkTradeResources] Invalid step:', this.turnStep);
			return;
		}

		if (this.currentTradeOffers.length) {
			this.startRegularStep(TurnSteps.TRADE_RESOURCES);

			this.dispatch();
			return;
		}

		this.startRegularStep(TurnSteps.CHECK_MESSAGES);
		this.checkMessages();
	}

	nextResourceTradeOffer() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.TRADE_RESOURCES) {
			console.error('[Game.endTradingResources] Invalid step:', this.turnStep);
			return;
		}

		this.currentTradeOffers.splice(0, 1);
		if (!this.currentTradeOffers.length) {
			this.startRegularStep(TurnSteps.CHECK_MESSAGES);
			this.checkMessages();
		}
	}

	checkMessages() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.CHECK_MESSAGES) {
			console.error('[Game.checkMessages] Invalid step:', this.turnStep);
			return;
		}

		if (this.messagePool) {
			if (this.currentPlayerStartingPoints < this.currentPlayer.points && (this.currentPlayer.points % this.messagePool.period) === 0) {
				this.prepareMessage();
				return;
			}
		}

		this.startRegularStep(TurnSteps.END);
		this.endTurn();
	}

	/**
	 * @param {TimelineItemTrigger|null=} trigger
	 */
	prepareMessage(trigger) {
		let message = this.messagePool.draw();
		this.currentMessage = new MessageEvaluator(message, this);

		let key = trigger ? 'c.timeline.player_gets_message_with_trigger' : 'c.timeline.player_gets_message';
		Timeline.log(key, 'message', null, trigger, this.currentPlayer.name, { messageCode: message.code });
		if (Preferences.getInstance().playSounds) {
			Sounds.messageReceived.play();
		}

		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (feature.pawns.length) {
					let structure = new Structure(feature, box);
					feature.pawns.forEach((pawn) => {
						pawn.onMessageObtained(structure, feature, this);
					});
				}
			});
		});

		this.startSpecialStep(TurnSteps.APPLY_MESSAGE);
		this.dispatch();
	}

	/**
	 * @param {string} choice 'accept' or 'refuse'
	 */
	applyMessage(choice) {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.APPLY_MESSAGE) {
			console.error('[Game.applyMessage] Invalid step:', this.turnStep);
			return;
		}

		if (choice === 'accept') {
			this.currentMessage.accept();
		}
		else {
			this.currentMessage.refuse();
		}
		this.currentMessage.free();
		this.currentMessage = null;

		if (this.turnRegularStep === TurnSteps.APPLY_MAIN_PHASE_SPECIAL_STEPS) {
			this.mainPhaseSpecialSteps();
		}
		else {
			this.startRegularStep(TurnSteps.END);
			this.endTurn();
		}
	}

	endTurn() {
		if (!this.isActive()) {
			return;
		}

		if (this.turnStep !== TurnSteps.END) {
			console.error('[Game.endTurn] Invalid step:', this.turnStep);
			return;
		}

		this.cleanupTurnData();

		// Check end.
		if (this.tilePool.isEmpty()) {
			this.endGame();
		}
		// Check extra turn.
		else if (this.hasExtraTurn && !this.isExtraTurn) {
			this.isExtraTurn = true;

			this.startTurn();
		}
		// Next player.
		else {
			let wait = false;
			if (this.boardEventPool && this.boardEventPool.period && this.playerTurnNumber % this.boardEventPool.period === 0) {
				let event = this.boardEventPool.draw();
				event.effects.forEach((effectData) => {
					let strategy = Strategies.getBoardEffect(effectData.type);
					let source = new TimelineItemTrigger('c.timeline.board_event', null, null, { boardEventCode: event.code });
					strategy.apply(effectData.configuration, this, source);
				});
				this.dispatch();
			}

			this.hasExtraTurn = false;
			this.isExtraTurn = false;

			// Next player.
			this.currentPlayerIndex = ArrayUtils.nextIndex(this.players, this.currentPlayerIndex);
			this.currentPlayer = this.players[this.currentPlayerIndex];

			this.playerTurnNumber++;

			if (this.currentPlayerIndex === 0) {
				this.tableTurnNumber++;
			}

			setTimeout(() => { this.startTurn(); }, wait ? 1000 : 1);
		}
	}

	cleanupTurnData() {
		if (this.lastPlayedBox) {
			ArrayUtils.remove(this.currentPlayer.tiles, this.lastPlayedBox.tile); // If the tile was player from hand, remove it.
			this.lastPlayedBox.hideFeatures();
		}

		// Clear data.
		this.drawnTiles = [];
		this.currentTile = null;
		this.lastPlayedBox = null;
		this.lastPlayedPawns = [];
		this.lastPlayedNeutralPawns = [];
		this.lastGotBackPawns = [];
		this.currentTradeOffers = [];

		// Clear suggestion logs.
		Timeline.clearCategory('suggestion');
	}

	startTurn() {
		this.startRegularStep(TurnSteps.BEGIN);

		this.currentPlayerStartingPoints = this.currentPlayer.points;

		if (!this.currentPlayer.tiles.length) {
			this.draw(); // Draw tile.
		}
		else {
			this.startRegularStep(TurnSteps.PLAY_FROM_HAND);
			this.dispatch();
		}
	}

	init() {
		Setup.saveLast();

		let startTiles = [Setup.startTile];
		if (Setup.start.multiple) {
			if (Setup.start.additionalType === 'selection') {
				Setup.additionalStartTileModels.forEach((model) => {
					startTiles.push(model);
				});
			}
			else { // Random case.
				let models = Setup.additionalStartTileModels;
				for (let i = 0; i < Setup.start.additionalCount; i++) {
					startTiles.push(ArrayUtils.randomItem(models));
				}
			}
		}

		this.tilePool = new TilePool(startTiles, Setup.selectedTileModels, Setup.poolType, Setup.poolSize);
		if (Setup.activateMessages) {
			this.messagePool = new MessagePool(Setup.selectedMessageModels, Setup.messagePoolType, Setup.messagePoolSize, Setup.messagePeriod);
		}
		if (Setup.activateBoardEvents) {
			this.boardEventPool = new BoardEventPool(
				Setup.selectedBoardEventModels, Setup.boardEventPoolType, Setup.boardEventPoolSize, Setup.boardEventPeriod
			);
		}
		this.neutralPawnPool = new NeutralPawnPool(Setup.neutralPawns);
		this.tokensPool = new TokensPool(Setup.tokens);

		this.players = Setup.players;
		this.optionalRules = Setup.optionalRules;

		// Init pawns.
		Setup.pawns.forEach((item) => {
			this.players.forEach((player) => {
				player.addPawns(item.code, item.count);
			});
		});

		// Init tiles hands.
		if (Setup.activateStartingHand) {
			Setup.selectedStartingHandTiles.forEach((item) => {
				this.players.forEach((player) => {
					player.addTile(new Tile(item));
				});
			});
		}

		// Init board limits.
		Board.initLimits(Setup.board.horizontalType, Setup.board.horizontalSize, Setup.board.verticalType, Setup.board.verticalSize);

		// Push the first tile(s).
		let box = Board.initFirstBox();
		Board.putTile(box, this.tilePool.startTiles[0]);
		if (this.tilePool.startTiles.length > 1) {
			if (Setup.board.horizontalType !== 'infinite') {
				Setup.start.horizontalSize = Math.min(Setup.start.horizontalSize, Setup.board.horizontalSize);
			}
			if (Setup.board.verticalType !== 'infinite') {
				Setup.start.verticalSize = Math.min(Setup.start.verticalSize, Setup.board.verticalSize);
			}
			Board.initBoxes(Setup.start.horizontalSize, Setup.start.verticalSize);

			let minSpacing = Setup.start.minSpacing;

			this.tilePool.startTiles.slice(1).forEach((tile) => {
				let remainingAttempts = 100;
				let rBox;
				do {
					rBox = Board.getRealBox(
						MathUtils.randomInt(-Setup.start.horizontalSize, Setup.start.horizontalSize),
						MathUtils.randomInt(-Setup.start.verticalSize, Setup.start.verticalSize)
					);
					remainingAttempts--;
				}
				while (remainingAttempts && (!rBox || !rBox.isEmpty || Zones.getSquare(rBox.x, rBox.y, minSpacing).getNonEmptyBoxes().length));

				if (remainingAttempts) {
					tile.setRandomOrientation();
					Board.putTile(rBox, tile);
				}
				else {
					console.warn('[Game.init] No place found for additional start tile after 100 tries.');
				}
			});
		}

		// Register features from initial tile.
		box.tile.features.forEach((feature) => {
			let structure = new Structure(feature, box);
			if (!structure.isCompleted()) {
				structure.register();
			}
		});

		// Select first player.
		ArrayUtils.shuffle(this.players);
		this.currentPlayerIndex = 0;
		this.currentPlayer = this.players[0];
		this.initialized = true;
		this.startDate = new Date();
		Timeline.log('c.timeline.game_begins', 'global');

		this.startTurn();
	}

	endGame() {
		Timeline.log('c.timeline.final_points_count', 'global');

		// Evaluate pawns.
		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				feature.pawns.forEach((pawn) => {
					if (pawn.hasEndEvaluation()) {
						(new PawnEvaluator(pawn, feature, box)).onEndGame(this);
					}
				});
			});
		});

		// Evaluate structures.
		let alreadyTestedFeatures = [];

		Board.getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(alreadyTestedFeatures, structure.features);

				structure.onEndGame(this);
			});
		});

		// Evaluate tokens.
		Models.tokensArray.forEach((model) => {
			let evaluator = new TokenEvaluator(model, this);
			this.players.forEach((player) => {
				evaluator.evaluateEnd(player);
			});
		});

		// Final ranking.
		let globalRanking = new Ranking();
		this.players.forEach((player) => {
			globalRanking.setPlayerScore(player, player.points);
		});
		let winners = globalRanking.getPlayersByPosition(1);

		if (winners.length === 1) {
			Timeline.log('c.timeline.game_winner', 'global', null, null, null, { playerName: winners[0].name });
		}
		else {
			let playerNames = [];
			winners.forEach((player) => { playerNames.push(player.name); });
			let last = playerNames.pop();
			Timeline.log('c.timeline.game_winners', 'global', null, null, null, { playerNames: playerNames.join(', '), lastPlayerName: last });
		}
		this.ended = true;

		Timeline.log('c.timeline.game_over', 'global');
		this.dispatch();
	}

	forceEnd() {
		if (this.lastPlayedBox) {
			this.lastPlayedBox.hideFeatures();
		}

		// Clear data.
		this.cleanupTurnData();
		this.startRegularStep(TurnSteps.END);
		this.currentPlayer = null;
		this.hasExtraTurn = false;
		this.isExtraTurn = false;

		Timeline.log('c.timeline.forced_end_game', 'global');
		this.endGame();
	}

	save() {
		LocalStorage.setObject('lastGame', this.data);
	}

	loadLast() {
		this.data = LocalStorage.getObject('lastGame');
	}

	/**
	 * @returns {boolean}
	 */
	hasLast() {
		return LocalStorage.isset('lastGame');
	}

	/**
	 * @returns {SerializedGame|null}
	 */
	get data() {
		if (!this.initialized) {
			return null;
		}

		let data = {
			tableTurnNumber: this.tableTurnNumber,
			playerTurnNumber: this.playerTurnNumber,
			players: [],
			optionalRules: this.optionalRules,
			currentPlayerIndex: this.currentPlayerIndex,
			currentPlayerStartingPoints: this.currentPlayerStartingPoints,
			currentTile: this.currentTile ? this.currentTile.code : null,
			drawnTiles: [],
			currentMessage: this.currentMessage ? this.currentMessage.model.code : null,
			lastPlayedBox: this.lastPlayedBox ? this.lastPlayedBox.coordinates.toJSON() : null,
			lastPlayedPawns: [],
			lastPlayedNeutralPawns: [],
			lastGotBackPawns: [],
			currentTradeOffers: [],
			hasExtraTurn: this.hasExtraTurn,
			isExtraTurn: this.isExtraTurn,
			turnStep: this.turnStep,
			turnRegularStep: this.turnRegularStep,
			pendingSpecialSteps: [],
			tilePool: this.tilePool.data,
			messagePool: this.messagePool ? this.messagePool.data : null,
			boardEventPool: this.boardEventPool ? this.boardEventPool.data : null,
			neutralPawnPool: this.neutralPawnPool.data,
			tokensPool: this.tokensPool.data,
			board: Board.data,
			ended: this.ended,
			timeline: Timeline.data
		};

		this.players.forEach((player) => {
			data.players.push(player.data);
		});

		this.drawnTiles.forEach((tile) => {
			data.drawnTiles.push(tile.code);
		});

		this.lastPlayedPawns.forEach((playedPawn) => {
			let featureIndex = playedPawn.box.tile.features.indexOf(playedPawn.feature);
			let boxCoordinates = playedPawn.box.coordinates.toJSON();
			data.lastPlayedPawns.push({ pawn: playedPawn.pawn.data, featureIndex: featureIndex, boxCoordinates: boxCoordinates });
		});

		this.lastPlayedNeutralPawns.forEach((playedPawn) => {
			let featureIndex = playedPawn.box.tile.features.indexOf(playedPawn.feature);
			let boxCoordinates = playedPawn.box.coordinates.toJSON();
			data.lastPlayedNeutralPawns.push({ pawn: playedPawn.pawn.data, featureIndex: featureIndex, boxCoordinates: boxCoordinates });
		});

		this.lastGotBackPawns.forEach((playedPawn) => {
			let featureIndex = playedPawn.box.tile.features.indexOf(playedPawn.feature);
			let boxCoordinates = playedPawn.box.coordinates.toJSON();
			data.lastGotBackPawns.push({ pawn: playedPawn.pawn.data, featureIndex: featureIndex, boxCoordinates: boxCoordinates });
		});

		this.pendingSpecialSteps.forEach((pendingStep) => {
			data.pendingSpecialSteps.push(pendingStep.data);
		});

		this.currentTradeOffers.forEach((tradeOffer) => {
			data.currentTradeOffers.push({
				offers: tradeOffer.offers,
				source: tradeOffer.source.toJSON()
			});
		});

		return data;
	}

	/**
	 * @param {SerializedGame|null} data
	 */
	set data(data) {
		if (!data) {
			return;
		}
		if (this.restoringData) {
			console.warn('Already restoring data, wait 50ms.');
			this.dataToRestore = data;
			setTimeout(() => {
				if (this.dataToRestore) {
					let dataToRestore = this.dataToRestore;
					this.dataToRestore = null;
					this.data = dataToRestore;
				}
			}, 50);
			return;
		}
		this.restoringData = true;

		this.initialized = true;
		this.startDate = new Date();
		this.tableTurnNumber = data.tableTurnNumber || 1;
		this.playerTurnNumber = data.playerTurnNumber || 1;

		//region Clear previous data.
		FeatureDirectory.clear();
		PawnDirectory.clear();

		this.players.forEach((player) => {
			player.free();
		});
		ArrayUtils.clear(this.players);

		this.lastPlayedPawns = []; // free() method was already called when clearing players.
		this.lastGotBackPawns = []; // free() method was already called when clearing players.
		this.lastPlayedNeutralPawns = []; // free() method was already called when clearing the pawn directory.
		this.pendingSpecialSteps = [];

		if (this.currentMessage) {
			this.currentMessage.free();
			this.currentMessage = null;
		}
		//endregion.

		if (data.hasOwnProperty('players')) {
			data.players.forEach((item) => {
				this.players.push(Player.fromData(item));
			});
		}

		if (data.hasOwnProperty('optionalRules')) {
			this.optionalRules = data.optionalRules;
		}

		if (data.hasOwnProperty('board')) {
			if (data.lastPlayedBox) {
				data.board._lastPlayedBox = data.lastPlayedBox;
			}
			Board.data = data.board;
		}

		if (data.hasOwnProperty('currentPlayerIndex')) {
			this.currentPlayerIndex = data.currentPlayerIndex;
			this.currentPlayer = this.players[data.currentPlayerIndex];
		}

		if (data.hasOwnProperty('currentPlayerStartingPoints')) {
			this.currentPlayerStartingPoints = data.currentPlayerStartingPoints;
		}

		if (data.hasOwnProperty('drawnTiles')) {
			this.drawnTiles = [];
			data.drawnTiles.forEach((tileCode) => {
				this.drawnTiles.push(new Tile(Models.getTileByCode(tileCode)));
			});
		}

		if (data.hasOwnProperty('currentTile')) {
			this.currentTile = null;
			if (data.currentTile) {
				if (this.drawnTiles.length) {
					this.drawnTiles.forEach((tile) => {
						if (tile.code === data.currentTile) {
							this.currentTile = tile;
						}
					});
				}
				else {
					this.currentPlayer.tiles.forEach((tile) => {
						if (tile.code === data.currentTile) {
							this.currentTile = tile;
						}
					});
				}
			}
		}

		if (data.hasOwnProperty('currentMessage')) {
			this.currentMessage = data.currentMessage ? new MessageEvaluator(Models.getMessageByCode(data.currentMessage), this) : null;
		}

		if (data.hasOwnProperty('lastPlayedBox')) {
			this.lastPlayedBox = data.lastPlayedBox ? BoxDirectory.getByCoordinates(data.lastPlayedBox) : null;
		}

		if (data.hasOwnProperty('lastPlayedPawns')) {
			data.lastPlayedPawns.forEach((pawnData) => {
				let owner = null;
				this.players.forEach((player) => {
					if (player.name === pawnData.pawn.ownerName) {
						owner = player;
					}
				});
				let pawn = new PlayerPawn(Models.getPawnByCode(pawnData.pawn.code), owner);
				let box = BoxDirectory.getByCoordinates(pawnData.boxCoordinates);
				this.lastPlayedPawns.push({
					pawn: pawn,
					feature: box.tile.features[pawnData.featureIndex],
					box: box
				});
			});
		}

		this.currentTradeOffers = [];
		if (data.hasOwnProperty('currentTradeOffers')) {
			data.currentTradeOffers.forEach((item) => {
				this.currentTradeOffers.push({
					offers: item.offers,
					source: TimelineItemTrigger.fromData(item.source)
				});
			});
		}

		if (data.hasOwnProperty('lastGotBackPawns')) {
			data.lastGotBackPawns.forEach((pawnData) => {
				let owner = null;
				this.players.forEach((player) => {
					if (player.name === pawnData.pawn.ownerName) {
						owner = player;
					}
				});
				let pawn = new PlayerPawn(Models.getPawnByCode(pawnData.pawn.code), owner);
				let box = BoxDirectory.getByCoordinates(pawnData.boxCoordinates);
				this.lastGotBackPawns.push({
					pawn: pawn,
					feature: box.tile.features[pawnData.featureIndex],
					box: box
				});
			});
		}

		if (data.hasOwnProperty('lastPlayedNeutralPawns')) {
			data.lastPlayedNeutralPawns.forEach((pawnData) => {
				let pawn = new NeutralPawn(Models.getNeutralPawnByCode(pawnData.pawn.code));
				let box = BoxDirectory.getByCoordinates(pawnData.boxCoordinates);
				this.lastPlayedNeutralPawns.push({
					pawn: pawn,
					feature: box.tile.features[pawnData.featureIndex],
					box: box
				});
			});
		}

		if (data.hasOwnProperty('hasExtraTurn')) {
			this.hasExtraTurn = data.hasExtraTurn;
		}

		if (data.hasOwnProperty('isExtraTurn')) {
			this.isExtraTurn = data.isExtraTurn;
		}

		if (data.hasOwnProperty('turnStep')) {
			this.turnStep = data.turnStep;
		}
		if (data.hasOwnProperty('turnRegularStep')) {
			this.turnRegularStep = data.turnRegularStep;
		}
		if (data.hasOwnProperty('pendingSpecialSteps')) {
			data.pendingSpecialSteps.forEach((pendingStepData) => {
				let step = new PendingSpecialStep();
				step.data = pendingStepData;
				this.pendingSpecialSteps.push(step);
			});
		}

		if (data.hasOwnProperty('tilePool')) {
			if (!this.tilePool) {
				this.tilePool = new TilePool();
			}
			this.tilePool.data = data.tilePool;
		}

		if (data.messagePool) {
			if (!this.messagePool) {
				this.messagePool = new MessagePool();
			}
			this.messagePool.data = data.messagePool;
		}

		if (data.boardEventPool) {
			if (!this.boardEventPool) {
				this.boardEventPool = new BoardEventPool();
			}
			this.boardEventPool.data = data.boardEventPool;
		}

		if (!this.neutralPawnPool) {
			this.neutralPawnPool = new NeutralPawnPool();
		}
		if (data.hasOwnProperty('neutralPawnPool')) {
			this.neutralPawnPool.data = data.neutralPawnPool;
		}

		if (!this.tokensPool) {
			this.tokensPool = new TokensPool();
		}
		if (data.hasOwnProperty('tokensPool')) {
			this.tokensPool.data = data.tokensPool;
		}

		if (data.hasOwnProperty('ended')) {
			this.ended = data.ended;
		}

		if (data.hasOwnProperty('timeline')) {
			Timeline.data = data.timeline;
		}

		// Restore step state.
		switch (this.turnStep) {
			case TurnSteps.PLAY_TILE:
			case TurnSteps.APPLY_MESSAGE:
				break;

			case TurnSteps.PLAY_PAWN:
				if (this.isActive()) {
					this.lastPlayedBox.showFeatures(this.currentPlayer, this);
				}
				break;

			case TurnSteps.END_MAIN_PHASE:
				this.lastPlayedBox.hideFeatures();
				break;
		}

		this.restoringData = false;
	}
}

export let game = new Game();

//region Event listeners on board events.
document.addEventListener('playCurrentTile', (event) => {
	game.playTile(event.detail.box, event.detail.ctrlPressed);
});

document.addEventListener('playCurrentPawn', (event) => {
	game.playPawn(event.detail.feature, event.detail.box);
});

document.addEventListener('giveExtraTurn', (event) => {
	game.gainExtraTurn(event.detail.source);
});

document.addEventListener('drawToHand', (event) => {
	game.drawToHand(event.detail.quantity, event.detail.source);
});

document.addEventListener('scorePoints', (event) => {
	game.currentPlayer.addPoints(event.detail.points, event.detail.source);
});

document.addEventListener('enterBox', (event) => {
	event.detail.box.onEnter(game.currentTile);
});

document.addEventListener('leaveBox', (event) => {
	event.detail.box.onLeave();
});

document.addEventListener('centerOnCoordinates', (event) => {
	let coordinates = event.detail.coordinates;
	if (coordinates) {
		Board.centerOnCoordinates(coordinates);
	}
});

document.addEventListener('tcConnectionStartGame', (event) => {
	game.data = event.detail.data;
});

document.addEventListener('tcConnectionGameUpdate', (event) => {
	game.data = event.detail.data;
	PlacementSuggestion.clear();

	if (Connection.isServer()) {
		game.dispatch(event.detail.peerId);
	}
});

document.addEventListener('tcConnectionRequestReconnection', (event) => {
	if (!Connection.isServer()) {
		console.error('[on tcConnectionRequestReconnection] Not server!');
		return;
	}

	let previousId = event.detail.data.previousId;
	console.log('[on tcConnectionRequestReconnection] Try to reconnect', previousId);

	let found = false;
	game.players.forEach((player) => {
		console.log('[on tcConnectionRequestReconnection] Look for player', player.peerId, previousId);
		if (player.peerId === previousId) {
			player.peerId = event.detail.peerId;
			console.log('[on tcConnectionRequestReconnection] send message', event.detail.connection);
			Connection.sendMessageToTarget(event.detail.connection, 'Reconnected', game.data);
			found = true;
		}
	});

	if (!found) {
		console.error('[on tcConnectionRequestReconnection] No player found for this id!');
	}
	else {
		Connection.instance.closeConnection(previousId);
	}
});

document.addEventListener('tcConnectionTilePlacementSuggestion', (event) => {
	if (game.turnStep !== TurnSteps.PLAY_TILE) {
		console.warn('[on tcConnectionTilePlacementSuggestion] Invalid step: ' + game.turnStep);
		return;
	}

	PlacementSuggestion.add(PlacementSuggestion.fromData(event.detail.data));

	if (Connection.isServer()) {
		Connection.sendMessage('TilePlacementSuggestion', event.detail.data, event.detail.peerId);
	}
});

document.addEventListener('tcConnectionBoardEffectStrategyAlert', (event) => {
	BoardEffectStrategy.alert(game, event.detail.data.message, event.detail.data.substitutions, false);

	if (Connection.isServer()) {
		Connection.sendMessage('BoardEffectStrategyAlert', event.detail.data, event.detail.peerId);
	}
});

//endregion