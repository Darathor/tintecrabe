/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core.mjs";

/**
 * @typedef {Object} PlacementData
 * @property {ConditionData[]} conditions
 */

export class AbstractPawnModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 * @param {number} defaultCount
	 * @param {EffectData[]} continuation
	 */
	constructor(code, src, type, defaultCount, continuation) {
		this.code = code;
		this.src = src;
		this.type = type;
		this.defaultCount = defaultCount;
		this.continuation = continuation;
	}
}

export class PlayerPawnModel extends AbstractPawnModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 * @param {number} defaultCount
	 * @param {boolean} secondCompatible
	 * @param {PlacementData} placement
	 * @param {EffectData[]} continuation
	 * @param {EvaluationData[]} majority
	 * @param {EffectData[]} messageObtained
	 * @param {CompletionData[]} completion
	 * @param {EvaluationData[]} evaluationCompleted
	 * @param {EvaluationData[]} evaluationEnd
	 */
	constructor(code, src, type, defaultCount, secondCompatible, placement, continuation, majority, messageObtained, completion, evaluationCompleted, evaluationEnd) {
		super(code, src, type, defaultCount, continuation);
		this.secondCompatible = secondCompatible;
		this.placement = placement;
		this.majority = majority;
		this.messageObtained = messageObtained;
		this.completion = completion;
		this.evaluationCompleted = evaluationCompleted;
		this.evaluationEnd = evaluationEnd;
	}

	/**
	 * @returns {string}
	 */
	get nameKey() {
		return 'pawn_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.nameKey);
	}

	/**
	 * @returns {string}
	 */
	get help() {
		return I18n.trans('pawn_' + this.code + '_help');
	}

	/**
	 * @returns {string}
	 */
	get defaultSrc() {
		return this.getSrc('orange');
	}

	/**
	 * @param {string} color
	 * @returns {string}
	 */
	getSrc(color) {
		return this.src.replace('{COLOR}', color);
	}
}

export class NeutralPawnModel extends AbstractPawnModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 * @param {string|null} subType
	 * @param {number} defaultCount
	 * @param {EffectData[]} continuation
	 */
	constructor(code, src, type, subType, defaultCount, continuation) {
		super(code, src, type, defaultCount, continuation);
		this.subType = subType;
	}

	/**
	 * @returns {string}
	 */
	get nameKey() {
		return 'neutral_pawn_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.nameKey);
	}

	/**
	 * @returns {string}
	 */
	get help() {
		return I18n.trans('neutral_pawn_' + this.code + '_help');
	}
}