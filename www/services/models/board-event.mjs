/**
 * Copyright (C) 2023 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core.mjs";

/**
 * @typedef {Object} BoardEffectData
 * @property {string} type
 * @property {Object} configuration
 */

export class BoardEventModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {number} defaultQuantity
	 * @param {BoardEffectData[]} effects
	 */
	constructor(code, src, defaultQuantity, effects) {
		this.code = code;
		this.src = src;
		this.defaultQuantity = defaultQuantity;
		this.effects = effects;
	}

	/**
	 * @returns {string}
	 */
	get nameKey() {
		return 'board_event_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.nameKey);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('board_event_' + this.code + '_help');
	}
}

