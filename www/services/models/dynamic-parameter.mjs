/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core.mjs";

/**
 * @typedef {Object} DynamicParameterData
 * @property {string} code
 * @property {string} i18nPrefix
 * @property {string} type
 * @property {*} [default]
 * @property {number} [min]
 * @property {number} [max]
 */

export class DynamicParameter {
	static TYPE_STRING = 'string';
	static TYPE_NUMBER = 'number';
	static TYPE_FEATURE_TYPE = 'FeatureType';

	/**
	 * @param {Object[]} strategies
	 * @param {Object} dynamicParameters
	 * @returns {Object[]}
	 */
	static applyDynamicConfiguration(strategies, dynamicParameters) {
		strategies = JSON.parse(JSON.stringify(strategies)); // Clone strategies.
		return DynamicParameter.doApplyDynamicConfiguration(strategies, dynamicParameters);
	}

	static doApplyDynamicConfiguration(object, dynamicParameters) {
		if (Array.isArray(object)) {
			object.forEach((item, key) => {
				object[key] = DynamicParameter.doApplyDynamicConfiguration(item, dynamicParameters);
			});
		}
		else if (object && typeof object === 'object' && object.constructor === Object ) {
			if (object._dynamic) {
				Object.keys(object._dynamic).forEach((key) => {
					object[key] = dynamicParameters[object._dynamic[key]];
				});
				delete object._dynamic;
			}
			Object.keys(object).forEach((key) => {
				object[key] = DynamicParameter.doApplyDynamicConfiguration(object[key], dynamicParameters);
			});
		}
		return object;
	}

	/**
	 * @param {DynamicParameterData} data
	 */
	static fromData(data) {
		switch (data.type) {
			case 'number':
				return new NumberDynamicParameter(data.code, data.i18nPrefix, data.type, data.default || null, data.min || 0, data.max || 99);
			default:
				return new DynamicParameter(data.code, data.i18nPrefix, data.type, data.default || null);
		}
	}

	constructor(code, i18nPrefix, type, defaultValue) {
		this.code = code;
		this.i18nPrefix = i18nPrefix;
		this.type = type;
		this.defaultValue = defaultValue;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.i18nPrefix + '_' + this.code);
	}
}

class NumberDynamicParameter extends DynamicParameter {
	constructor(code, i18nPrefix, type, defaultValue, min, max) {
		super(code, i18nPrefix, type, defaultValue);
		this.min = min;
		this.max = max;
	}
}