/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, FileLoader, I18n } from "../core.mjs";
import { Models } from "../models.mjs";
import { NeutralPawnModel, PlayerPawnModel } from "./pawn.mjs";
import { TokenModel } from "./token.mjs";
import { FeatureModel } from "./feature.mjs";
import { TileModel } from "./tile.mjs";
import { MessageModel } from "./message.mjs";
import { BoardEventModel } from "./board-event.mjs";
import { Extension } from "./extension.mjs";

export let ModelsLoader = {
	/**
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	load(extensions, onDone) {
		Models.extensionCodes = extensions;
		new ExtensionLoader(I18n.LCID, extensions, onDone);
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addPawns(modelsData, extension) {
		modelsData.forEach((data) => {
			let pawnModel = new PlayerPawnModel(
				data.code, data.src, data.type, data.defaultCount || 0, data.secondCompatible || false,
				data.placement || {}, data.continuation || [], data.majority || [], data.messageObtained || [],
				data.completion || [], data.evaluationCompleted || [], data.evaluationEnd || []
			);
			Models.pawns[data.code] = pawnModel;
			Models.pawnsArray.push(pawnModel);
			extension.pawns.push(pawnModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addNeutralPawns(modelsData, extension) {
		modelsData.forEach((data) => {
			let pawnModel = new NeutralPawnModel(
				data.code, data.src, data.type, data.subType || null, data.defaultCount || 0, data.continuation || []
			);
			Models.neutralPawns[data.code] = pawnModel;
			Models.neutralPawnsArray.push(pawnModel);
			extension.neutralPawns.push(pawnModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addTokens(modelsData, extension) {
		modelsData.forEach((data) => {
			let tokenModel = new TokenModel(
				data.code, data.src, data.type, data.defaultCount || 0, data.scoreEnd
			);
			Models.tokens[data.code] = tokenModel;
			Models.tokensArray.push(tokenModel);
			extension.tokens.push(tokenModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addFeatures(modelsData, extension) {
		modelsData.forEach((data) => {
			let featureModel = new FeatureModel(
				data.code, data.categories || [], data.src, data.compatibleWith || [], data.placementConstraints || [], data.externalLink || false,
				data.pawns || {}, data.played || [], data.influence || [], data.continuation || [], data.specialNeighbours || [],
				data.completion || [], data.completionEffects || [], data.evaluationCompleted || [], data.evaluationEnd || [],
				data.dynamicParameters || []
			);
			Models.features[data.code] = featureModel;
			Models.featuresArray.push(featureModel);
			extension.features.push(featureModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 */
	updateFeatures(modelsData) {
		modelsData.forEach((data) => {
			let featureModel = Models.features[data.code];
			if (!featureModel) {
				console.error('Unknown feature code:', data.code);
				return;
			}

			if (data.pawns && data.pawns.allowed) {
				if (!featureModel.pawns.allowed) {
					featureModel.pawns.allowed = [];
				}
				data.pawns.allowed.forEach((pawnCode) => {
					featureModel.pawns.allowed.push(pawnCode);
				});
			}

			if (data.continuation && data.continuation.length) {
				if (!featureModel.continuation) {
					featureModel.continuation = [];
				}
				data.continuation.forEach((continuationData) => {
					featureModel.continuation.push(continuationData);
				});
			}

			if (data.evaluationCompleted && data.evaluationCompleted.length) {
				if (!featureModel.evaluationCompleted) {
					featureModel.evaluationCompleted = [];
				}
				data.evaluationCompleted.forEach((evaluationData) => {
					featureModel.evaluationCompleted.push(evaluationData);
				});
			}

			if (data.evaluationEnd && data.evaluationEnd.length) {
				if (!featureModel.evaluationEnd) {
					featureModel.evaluationEnd = [];
				}
				data.evaluationEnd.forEach((evaluationData) => {
					featureModel.evaluationEnd.push(evaluationData);
				});
			}
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addTiles(modelsData, extension) {
		modelsData.forEach((data) => {
			let tileModel = new TileModel(
				data.code, data.src, data.sides, data.features, data.defaultQuantity || 1, data.startingHand || 0, data.start || false,
				data.additionalStart || false
			);
			Models.tiles[data.code] = tileModel;
			Models.tilesArray.push(tileModel);
			extension.tiles.push(tileModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addMessages(modelsData, extension) {
		modelsData.forEach((data) => {
			let messageModel = new MessageModel(
				data.code, data.src || '', data.defaultQuantity || 1, data.effects || [], data.featureReferences || []
			);
			Models.messages[data.code] = messageModel;
			Models.messagesArray.push(messageModel);
			extension.messages.push(messageModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addBoardEvents(modelsData, extension) {
		modelsData.forEach((data) => {
			let eventModel = new BoardEventModel(
				data.code, data.src || '', data.defaultQuantity || 1, data.effects || []
			);
			Models.boardEvents[data.code] = eventModel;
			Models.boardEventsArray.push(eventModel);
			extension.boardEvents.push(eventModel);
		});
	},
	/**
	 * @param {string} name
	 * @param {Object} response
	 */
	addExtension(name, response) {
		let extension = new Extension(response.code, response.symbolSrc || null, response.author, response.descriptionKey);
		ArrayUtils.replace(Models.extensionCodes, name, extension.code);
		Models.extensions[extension.code] = extension;
		this.rebuildExtensionsArray();

		// New models.
		if ('pawns' in response) {
			this.addPawns(response.pawns, extension);
		}
		if ('neutralPawns' in response) {
			this.addNeutralPawns(response.neutralPawns, extension);
		}
		if ('tokens' in response) {
			this.addTokens(response.tokens, extension);
		}
		if ('features' in response) {
			this.addFeatures(response.features, extension);
		}
		if ('tiles' in response) {
			this.addTiles(response.tiles, extension);
		}
		if ('messages' in response) {
			this.addMessages(response.messages, extension);
		}
		if ('boardEvents' in response) {
			this.addBoardEvents(response.boardEvents, extension);
		}

		// Existing models update.
		if ('featuresUpdate' in response) {
			this.updateFeatures(response.featuresUpdate);
		}
	},
	rebuildExtensionsArray() {
		ArrayUtils.clear(Models.extensionsArray);
		Models.extensionCodes.forEach((code) => {
			if (Models.extensions[code]) {
				Models.extensionsArray.push(Models.extensions[code]);
			}
		})
	}
};

class ExtensionLoader {
	/**
	 * @param {string} LCID
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	constructor(LCID, extensions, onDone) {
		let loader = new FileLoader();
		let extensionsData = {};
		let updatedOnDone = () => {
			extensions.forEach((extension) => {
				ModelsLoader.addExtension(extension, extensionsData[extension]);
			});
			onDone();
		};
		extensions.forEach((extension) => {
			loader.loadFile('extensions/' + extension + '/extension.json', (response) => { extensionsData[extension] = response; }, updatedOnDone);
			loader.loadFile('extensions/' + extension + '/i18n/' + LCID + '.json', (data) => { I18n.importPackage(data, ''); }, updatedOnDone);
			I18n.registerFilePath('', 'extensions/' + extension + '/i18n/');
		});
	}
}