/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { DynamicParameter } from "./dynamic-parameter.mjs";
import { I18n } from "../core.mjs";

/**
 * @typedef {Object} PlacementConstraint
 * @property {string} type
 * @property {Object} configuration
 */

/**
 * @typedef {Object} PawnsData
 * @property {string[]} allowed
 * @property {ConditionData[]} conditions
 */

/**
 * @typedef {Object} FeatureInfluenceData
 * @property {string} type
 * @property {ZoneData} zone
 * @property {Object} configuration
 */

/**
 * @typedef {Object} FeatureSelectionData
 * @property {string} type
 * @property {ConditionData[]} conditions
 * @property {Object} configuration
 */

export class FeatureModel {
	/** @member {DynamicParameter[]} */
	dynamicParameters = [];

	/**
	 * @param {string} code
	 * @param {string[]} categories
	 * @param {string} src
	 * @param {string[]} compatibleWith
	 * @param {PlacementConstraint[]} placementConstraints
	 * @param {boolean} externalLink
	 * @param {PawnsData} pawns
	 * @param {EffectData[]} played
	 * @param {FeatureInfluenceData[]} influence
	 * @param {EffectData[]} continuation
	 * @param {FeatureSelectionData[]} specialNeighbours
	 * @param {CompletionData[]} completion
	 * @param {EffectData[]} completionEffects
	 * @param {EvaluationData[]} evaluationCompleted
	 * @param {EvaluationData[]} evaluationEnd
	 * @param {DynamicParameterData[]} dynamicParameters
	 */
	constructor(code, categories, src, compatibleWith, placementConstraints, externalLink, pawns, played, influence, continuation, specialNeighbours, completion, completionEffects, evaluationCompleted, evaluationEnd, dynamicParameters) {
		this.code = code;
		this.categories = categories;
		this.src = src;
		this.compatibleWith = compatibleWith;
		this.placementConstraints = placementConstraints;
		this.externalLink = externalLink;
		this.pawns = pawns;
		this.played = played;
		this.influence = influence;
		this.continuation = continuation;
		this.specialNeighbours = specialNeighbours;
		this.completion = completion;
		this.completionEffects = completionEffects;
		this.evaluationCompleted = evaluationCompleted;
		this.evaluationEnd = evaluationEnd;
		if (dynamicParameters) {
			dynamicParameters.forEach((parameterData) => {
				parameterData.i18nPrefix = this.nameKey;
				this.dynamicParameters.push(DynamicParameter.fromData(parameterData));
			});
		}
	}

	/**
	 * @returns {string}
	 */
	get nameKey() {
		return 'feature_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.nameKey);
	}

	/**
	 * @returns {string}
	 */
	get help() {
		return I18n.trans('feature_' + this.code + '_help');
	}
}
