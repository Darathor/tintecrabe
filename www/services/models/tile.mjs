/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Models } from "../models.mjs";
import { DynamicParameter } from "./dynamic-parameter.mjs";

export class TileModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {Array[]} sides
	 * @param {Array[]} features
	 * @param {number} defaultQuantity
	 * @param {number} startingHand
	 * @param {boolean} start
	 * @param {boolean} additionalStart
	 */
	constructor(code, src, sides, features, defaultQuantity, startingHand, start, additionalStart) {
		this.code = code;
		this.src = src;
		this.sides = sides;
		this.featuresData = features;
		this.defaultQuantity = defaultQuantity;
		this.startingHand = startingHand;
		this.start = start;
		this.additionalStart = additionalStart;
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	containsFeature(code) {
		let result = false;
		this.featuresData.forEach((featureData) => {
			if (featureData[0] === code) {
				result = true;
			}
		});
		return result;
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	refersFeature(code) {
		let result = false;
		this.featuresData.forEach((featureData) => {
			let feature = Models.getFeatureByCode(featureData[0]);
			feature.dynamicParameters.forEach((parameter) => {
				if (parameter.type === DynamicParameter.TYPE_FEATURE_TYPE && featureData[4][parameter.code] === code) {
					result = true;
				}
			});
		});
		return result;
	}
}