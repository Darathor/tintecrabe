/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n } from "../core.mjs";

/**
 * @typedef {Object} PlayerEffectData
 * @property {string} type
 * @property {Object} configuration
 */

export class MessageModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {number} defaultQuantity
	 * @param {PlayerEffectData[]} effects
	 * @param {string[]} featureReferences
	 */
	constructor(code, src, defaultQuantity, effects, featureReferences) {
		this.code = code;
		this.src = src;
		this.defaultQuantity = defaultQuantity;
		this.effects = effects;
		this.featureReferences = featureReferences;
	}

	/**
	 * @returns {string}
	 */
	get descriptionKey() {
		return 'message_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans(this.descriptionKey);
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	refersFeature(code) {
		return ArrayUtils.contains(this.featureReferences, code);
	}
}