/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core.mjs";

export class TokenModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 * @param {number} defaultCount
	 * @param {EvaluationData[]} scoreEnd
	 */
	constructor(code, src, type, defaultCount, scoreEnd) {
		this.code = code;
		this.src = src;
		this.type = type;
		this.defaultCount = defaultCount;
		this.scoreEnd = scoreEnd;
	}

	/**
	 * @returns {string}
	 */
	get nameKey() {
		return 'token_' + this.code;
	};

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans(this.nameKey);
	}

	/**
	 * @returns {string}
	 */
	get help() {
		return I18n.trans('token_' + this.code + '_help');
	}
}

