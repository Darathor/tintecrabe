/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Board } from '../services/board.mjs';

let Vue = window.Vue;

Board.initLimits('finite-without-edges', 2, 'finite-without-edges', 2);

let n = 8;

let columns = [];
let rows = [];
for (let i = -n; i <= n; i++) {
	columns.push(i);
	rows.push(i);
}

let vm = new Vue({
	el: '#board-view',
	data: {
		Board: Board,
		columns: columns,
		rows: rows
	}
});