# Sources

## Original

* cancel.svg
* confirm.svg
* conflict-same.svg
* playing-turn.svg
* new-player.svg
* tile-pool.svg
* tile-rotate-left.svg
* tiles-hand.svg
* tiles-hand-add.svg
* warning.svg

## [Font Awesome](https://fontawesome.com/)

* add.svg
* arrow-down.svg
* arrow-left.svg
* arrow-right.svg
* arrow-up.svg
* clock.svg
* close.svg
* copy.svg
* delete.svg
* deselect-all.svg
* edit.svg
* info.svg
* language.svg
* mouse-left-click.svg (modified)
* mouse-right-click.svg (modified)
* redraw.svg
* save.svg
* select-all.svg
* suggestion.svg
* target.svg
* toggle-down.svg
* toggle-left.svg
* toggle-right.svg
* toggle-up.svg
* volume-mute.svg
* volume-up.svg