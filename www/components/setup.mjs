/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Colors } from '../services/players.mjs';
import { Setup } from '../services/setup.mjs';
import { Models } from '../services/models.mjs';
import { I18n } from '../services/core.mjs';
import { Preferences } from "../services/preferences.mjs";

let Vue = window.Vue;

Vue.component('tc-setup-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-setup-setup panel">
			<div class="panel-body">
				<p v-if="Setup.hasLast && !client">
					<button type="button" class="btn btn--block" @click="Setup.loadLast()">{{ 'c.setup.load_last_configuration' | trans }}</button>
				</p>
				<p>
					{{ 'c.setup.tile_size' | trans(['lab']) }}
					<button type="button" class="btn" @click="Preferences.setupTileSize = 'small'" :disabled="Preferences.setupTileSize == 'small'">
						{{ 'c.setup.tile_size_small' | trans }}
					</button>
					<button type="button" class="btn" @click="Preferences.setupTileSize = 'large'" :disabled="Preferences.setupTileSize == 'large'">
						{{ 'c.setup.tile_size_large' | trans }}
					</button>
				</p>
				<label class="no-margin-bottom">
					<input type="checkbox" class="switch" v-model="Preferences.setupHideDisabledTiles" />
					{{ 'c.setup.hide_disabled_tiles' | trans }}
				</label>
			</div>
		</div>`
});

Vue.component('tc-players-setup', {
	props: ['peerId', 'server'],
	data: function() {
		return {
			Setup: Setup,
			Colors: Colors,
			I18n: I18n
		}
	},
	template: `<div class="tc-players-setup panel">
			<h2 class="panel-header">{{ 'c.setup.players' | trans }}</h2>
			<div class="panel-body">
				<table>
					<thead>
						<tr>
							<th class="w150p">{{ 'c.setup.player_name' | trans }}</th>
							<th class="w150p">{{ 'c.setup.player_color' | trans }}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr class="player-item" v-for="player in Setup.players">
							<td><input type="text" v-model.trim="player.name" :disabled="player.peerId && player.peerId !== peerId" maxlength="30" /></td>
							<td>
								<select v-model="player.color" :disabled="player.peerId && player.peerId !== peerId">
									<option v-for="color in Colors" :value="color" :style="{ color: color.cssCode, background: color.cssSecondary }"
										v-if="color.isAvailable(Setup.players, player)">{{ color.name }}</option>
								</select>
							</td>
							<td>
								<button v-if="!peerId || (server && peerId !== player.peerId)" type="button" class="btn" @click="Setup.removePlayer(player)"
									:disabled="Setup.players.length === 1 || (peerId && peerId === player.peerId)">
									<img src="images/icons/delete.svg" class="icon-sm" :alt="'c.setup.delete' | trans"
										v-tooltip="I18n.trans('c.setup.delete_player')" />
								</button>
								<button v-if="peerId && peerId === player.peerId" type="button" class="btn" @click="Setup.savePlayerData(player)">
									<img src="images/icons/save.svg" class="icon-sm" :alt="'c.setup.save' | trans"
										v-tooltip="I18n.trans('c.setup.save_data_for_next_games')" />
								</button>
							</td>
						</tr>
					</tbody>
				</table>
				<button type="button" class="btn" @click="Setup.addPlayer()" v-if="!peerId">
					<img src="images/icons/add.svg" class="icon-sm" alt="" /> {{ 'c.setup.add_player' | trans }}
				</button>
			</div>
		</div>`
});

Vue.component('tc-pawns-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-pawns-setup panel">
			<h2 class="panel-header">{{ 'c.setup.pawns' | trans }}</h2>
			<div class="panel-body">
				<div class="pawn-item" v-for="item in Setup.pawns">
					<label>
						<input type="number" v-model.number="item.count" min="0" max="99" step="1" size="2" :disabled="client" />
						<tc-pawn-model-summary :code="item.code"></tc-pawn-model-summary>
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-neutral-pawns-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-neutral-pawns-setup panel">
			<h2 class="panel-header">
				{{ 'c.setup.neutral_pawns' | trans }}
				<div class="switch-container fr">
					<button type="button" class="btn btn-icon-sm" v-if="!Setup.collapse.neutralPawns" v-tooltip="I18n.trans('c.main.collapse')"
						@click="Setup.collapse.neutralPawns = true">
						<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" v-if="Setup.collapse.neutralPawns" v-tooltip="I18n.trans('c.main.expand')"
						@click="Setup.collapse.neutralPawns = false">
						<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
					</button>
				</div>
			</h2>
			<div class="panel-body" v-if="!Setup.collapse.neutralPawns">
				<div class="pawn-item" v-for="item in Setup.neutralPawns">
					<label>
						<input type="number" v-model.number="item.count" min="0" max="99" step="1" size="2" :disabled="client" />
						<tc-neutral-pawn-model-summary :code="item.code"></tc-neutral-pawn-model-summary>
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-tokens-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-tokens-setup panel">
			<h2 class="panel-header">
				{{ 'c.setup.tokens' | trans }}
				<div class="switch-container fr">
					<button type="button" class="btn btn-icon-sm" v-if="!Setup.collapse.tokens" v-tooltip="I18n.trans('c.main.collapse')"
						@click="Setup.collapse.tokens = true">
						<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" v-if="Setup.collapse.tokens" v-tooltip="I18n.trans('c.main.expand')"
						@click="Setup.collapse.tokens = false">
						<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
					</button>
				</div>
			</h2>
			<div class="panel-body" v-if="!Setup.collapse.tokens">
				<div class="pawn-item" v-for="item in Setup.tokens">
					<label>
						<input type="number" v-model.number="item.count" min="0" max="99" step="1" size="2" :disabled="client" />
						<tc-token-model-summary :code="item.code"></tc-token-model-summary>
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-multiple-start-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-multiple-start-setup panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch " v-model="Setup.start.multiple" value="selection" :disabled="client" />
				</div>
				{{ 'c.setup.multiple_start' | trans }}
			</h2>
			<div class="panel-body" v-if="Setup.start.multiple">
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.start.additionalType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.additionalStartTileModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.start.additionalType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.start.additionalCount" min="1" max="10" step="1" size="2"
							:disabled="client || Setup.start.additionalType !== 'random'" />)
					</label>
				</div>
				<h3>{{ 'c.setup.start_zone_size' | trans }}</h3>
				<div>
					<label>
						{{ 'c.setup.start_zone_horizontal_size' | trans }}
						<input type="number" v-model.number="Setup.start.horizontalSize" :min="1" max="25" step="1" size="2" />
					</label>
					<label>
						{{ 'c.setup.start_zone_vertical_size' | trans }}
						<input type="number" v-model.number="Setup.start.verticalSize" :min="1" max="25" step="1" size="2" />
					</label>
					<label>
						{{ 'c.setup.start_minimum_spacing_between_tiles' | trans }}
						<input type="number" v-model.number="Setup.start.minSpacing" :min="1" max="5" step="1" size="2" />
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-starting-hand-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-starting-hand-setup panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch " v-model="Setup.activateStartingHand" value="selection" :disabled="client" />
				</div>
				{{ 'c.setup.starting_hand' | trans }}
			</h2>
			<div class="panel-body" v-if="Setup.activateStartingHand">
				<div>
					<label>
						{{ 'c.setup.starting_hand_size' | trans(['lab']) }} {{ Setup.selectedStartingHandTiles.length }}
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-pool-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-pool-setup panel">
			<h2 class="panel-header">{{ 'c.setup.tiles_pool' | trans }}</h2>
			<div class="panel-body">
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.poolType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.selectedTileModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.poolType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.poolSize" min="2" max="999" step="1" size="3" :disabled="client || Setup.poolType !== 'random'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-message-pool-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-message-pool-setup panel">
			<h2 class="panel-header">
				{{ 'c.setup.messages' | trans }}
				<div class="switch-container fr">
					<input type="checkbox" class="switch" v-model="Setup.activateMessages" :disabled="client" />
				</div>
			</h2>
			<div class="panel-body" v-if="Setup.activateMessages">
				<label>
					{{ 'c.setup.message_period' | trans(['ucf']) }}
					<input type="number" v-model="Setup.messagePeriod" min="2" max="999" step="1" size="3" :disabled="client" />
				</label>
				<h3>{{ 'c.setup.message_pool' | trans }}</h3>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.messagePoolType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.selectedMessageModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.messagePoolType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.messagePoolSize" min="2" max="999" step="1" size="3"
							:disabled="client || Setup.messagePoolType !== 'random'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-board-events-pool-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-message-pool-setup panel">
			<h2 class="panel-header">
				{{ 'c.setup.board_events' | trans }}
				<div class="switch-container fr">
					<input type="checkbox" class="switch" v-model="Setup.activateBoardEvents" :disabled="client" />
				</div>
			</h2>
			<div class="panel-body" v-if="Setup.activateBoardEvents">
				<label>
					{{ 'c.setup.board_event_period' | trans(['ucf']) }}
					<input type="number" v-model="Setup.boardEventPeriod" min="2" max="999" step="1" size="3" :disabled="client" />
				</label>
				<h3>{{ 'c.setup.board_event_pool' | trans }}</h3>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.boardEventPoolType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.selectedBoardEventModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.boardEventPoolType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.boardEventPoolSize" min="2" max="999" step="1" size="3"
							:disabled="client || Setup.boardEventPoolType !== 'random'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-board-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			I18n: I18n
		}
	},
	template: `<div class="tc-board-setup panel">
			<h2 class="panel-header">
				{{ 'c.setup.board' | trans }}
				<div class="switch-container fr">
					<button type="button" class="btn btn-icon-sm" v-if="!Setup.collapse.board" v-tooltip="I18n.trans('c.main.collapse')"
						@click="Setup.collapse.board = true">
						<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" v-if="Setup.collapse.board" v-tooltip="I18n.trans('c.main.expand')"
						@click="Setup.collapse.board = false">
						<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
					</button>
				</div>
			</h2>
			<div class="panel-body" v-if="!Setup.collapse.board">
				<h3>{{ 'c.setup.horizontal_axis' | trans }}</h3>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.horizontalType" value="infinite" :disabled="client" />
						{{ 'c.setup.infinite' | trans }}
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.horizontalType" value="finite-with-edges" :disabled="client" />
						{{ 'c.setup.finite_with_edges' | trans }}
					</label>
					<label v-tooltip="I18n.trans('c.setup.axis_size_tooltip')">
						({{ 'c.setup.axis_size' | trans }}
						<input type="number" v-model.number="Setup.board.horizontalSize" min="1" max="999" step="1" size="3" :disabled="client || Setup.board.horizontalType !== 'finite-with-edges'" />)
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.horizontalType" value="finite-without-edges" :disabled="client" />
						{{ 'c.setup.finite_without_edges' | trans }}
					</label>
					<label v-tooltip="I18n.trans('c.setup.axis_size_tooltip')">
						({{ 'c.setup.axis_size' | trans }}
						<input type="number" v-model.number="Setup.board.horizontalSize" min="1" max="999" step="1" size="3" :disabled="client || Setup.board.horizontalType !== 'finite-without-edges'" />)
					</label>
				</div>
				<h3>{{ 'c.setup.vertical_axis' | trans }}</h3>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.verticalType" value="infinite" :disabled="client" />
						{{ 'c.setup.infinite' | trans }}
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.verticalType" value="finite-with-edges" :disabled="client" />
						{{ 'c.setup.finite_with_edges' | trans }}
					</label>
					<label v-tooltip="I18n.trans('c.setup.axis_size_tooltip')">
						({{ 'c.setup.axis_size' | trans }}
						<input type="number" v-model.number="Setup.board.verticalSize" min="1" max="999" step="1" size="3" :disabled="client || Setup.board.verticalType !== 'finite-with-edges'" />)
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.board.verticalType" value="finite-without-edges" :disabled="client" />
						{{ 'c.setup.finite_without_edges' | trans }}
					</label>
					<label v-tooltip="I18n.trans('c.setup.axis_size_tooltip')">
						({{ 'c.setup.axis_size' | trans }}
						<input type="number" v-model.number="Setup.board.verticalSize" min="1" max="999" step="1" size="3" :disabled="client || Setup.board.verticalType !== 'finite-without-edges'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-optional-rules-setup', {
	props: ['client'],
	data: function() {
		return {
			Rules: Setup.optionalRules,
		}
	},
	template: `<div class="tc-optional-rules-setup panel">
			<h2 class="panel-header">{{ 'c.setup.optional_rules' | trans }}</h2>
			<div class="panel-body">
				<label>
					<input type="checkbox" class="switch" v-model="Rules.extraTurnWhenFillHole" :disabled="client" />
					{{ 'c.setup.extra_turn_when_fill_hole' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowToGetBackPawn" :disabled="client" />
					{{ 'c.setup.allow_to_take_back_pawn' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowAddTileToHand" :disabled="client" />
					{{ 'c.setup.allow_add_tile_to_hand' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowPoolDetailView" :disabled="client" />
					{{ 'c.setup.allow_pool_detail_view' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.showMessagesEstimation" :disabled="client" />
					{{ 'c.setup.show_messages_estimation' | trans }}
				</label>
			</div>
		</div>`
});

Vue.component('tc-first-tile-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-first-tile-setup setup-tile-list panel">
			<h2 class="panel-header">{{ 'c.setup.choose_starting_tile' | trans }}</h2>
			<div class="panel-body">
				<div class="tiles">
					<div class="item" v-for="tile in Setup.startTileOptions" :class="{ selected: tile === Setup.startTile }">
						<tc-tile-thumbnail :tile="tile"></tc-tile-thumbnail>
						<label class="item-selector text-muted">
							<input type="radio" class="radio" v-model="Setup.startTile" :value="tile" :disabled="client" /> {{ tile.code }}
						</label>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-setup-tile-item', {
	props: ['client', 'item'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-setup-tile-item item" v-show="item.enabled || !Preferences.setupHideDisabledTiles"
			:class="{ selected: item.selected }">
			<tc-tile-thumbnail :tile="item.tile"></tc-tile-thumbnail>
			<label class="item-selector text-muted">
				<input type="checkbox" class="checkbox" v-model="item.selected" :disabled="!item.enabled || client" /> {{ item.tile.code }}
			</label>
			<input class="selected-quantity" type="number" v-model.number="item.quantity" min="1" max="99" step="1" size="2"
				:disabled="!item.selected || client" />
		</div>`
});

Vue.component('tc-additional-start-tiles-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-additional-start-tiles-setup setup-tile-list panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch " v-model="Setup.start.multiple" value="selection" :disabled="client" />
				</div>
				{{ 'c.setup.choose_additional_starting_tiles' | trans }}
			</h2>
			<div class="panel-body" v-show="Setup.start.multiple">
				<div v-for="extensionData in Setup.selection" v-if="extensionData.additionalStartTiles.length">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
					</h3>
					<div class="tiles">
						<tc-setup-tile-item v-for="item in extensionData.additionalStartTiles" :key="item.code" :item="item" :client="client">
						</tc-setup-tile-item>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-setup-feature-item', {
	props: ['client', 'item', 'mode'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n,
			selectTiles: () => {
				switch (this.mode) {
					case 'tiles':
						Setup.selectTilesWithFeature(this.item.model);
						break;
					case 'startingHand':
						Setup.selectStartingHandTilesWithFeature(this.item.model);
						break;
				}
			},
			deselectTiles: () => {
				switch (this.mode) {
					case 'tiles':
						Setup.deselectTilesWithFeature(this.item.model);
						break;
					case 'startingHand':
						Setup.deselectStartingHandTilesWithFeature(this.item.model);
						break;
				}
			}
		}
	},
	computed: {
		selectedCount() {
			let count = 0;
			if (this.item.enabled) {
				this.item[this.mode].forEach((tileData) => {
					if (tileData.selected) {
						count += tileData.quantity;
					}
				});
			}
			return count;
		},
		totalCount() {
			return this.item[this.mode].length;
		}
	},
	template: `<div class="item" :class="{ selected: item.enabled }">
			<tc-feature-model-summary :model="item.model"></tc-feature-model-summary>
			<label class="item-selector">
				<input class="checkbox" type="checkbox" v-model="item.enabled" :change="Setup.refreshFeature(item)" :disabled="client"
					v-tooltip="I18n.trans('c.setup.uncheck_to_disable_feature')" />
				<span>{{ selectedCount }}<small class="text-muted">/{{ totalCount }}</small></span>
			</label>
			<div v-if="!client" class="mass-selection">
				<button v-if="mode === 'tiles'" type="button" class="btn" @click="selectTiles()" :disabled="!item.enabled"
					v-tooltip="I18n.trans('c.setup.select_all_this_feature')">
					<img src="images/icons/select-all.svg" class="icon-sm" alt="" />
				</button>
				<button type="button" class="btn" @click="deselectTiles()" :disabled="!item.enabled"
					v-tooltip="I18n.trans('c.setup.deselect_all_this_feature')">
					<img src="images/icons/deselect-all.svg" class="icon-sm" alt="" />
				</button>
			</div>
		</div>`
});

Vue.component('tc-tiles-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-tiles-list-setup setup-tile-list panel">
			<h2 class="panel-header">
				{{ 'c.setup.choose_tiles' | trans }} ({{ Setup.selectedTileModels.length }})
				<span v-if="!client">
					<button type="button" class="btn btn-icon" @click="Setup.selectAllTiles()" v-tooltip="I18n.trans('c.setup.select_all_tiles')">
						<img src="images/icons/select-all.svg" class="icon" alt="" />
					</button>
					<button type="button" class="btn btn-icon" @click="Setup.deselectAllTiles()" v-tooltip="I18n.trans('c.setup.deselect_all_tiles')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body">
				<div>
					<h3>{{ 'c.setup.selection_by_feature_type' | trans }}</h3>
					<div class="features">
						<tc-setup-feature-item v-for="item in Setup.features" :key="item.code" :client="client" :item="item" :mode="'tiles'">
						</tc-setup-feature-item>
					</div>
				</div>
				<div v-for="extensionData in Setup.selection" v-if="extensionData.tiles.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.selectExtensionTiles(extensionData)"
								v-tooltip="I18n.trans('c.setup.select_all_this_extension')">
								<img src="images/icons/select-all.svg" class="icon" alt="" />
							</button>
							<button type="button" class="btn btn-icon" @click="Setup.deselectExtensionTiles(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="tiles">
						<tc-setup-tile-item v-for="item in extensionData.tiles" :key="item.code" :item="item" :client="client"></tc-setup-tile-item>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-starting-hand-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-starting-hand-list-setup setup-tile-list panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch " v-model="Setup.activateStartingHand" value="selection" :disabled="client" />
				</div>
				{{ 'c.setup.choose_starting_hand' | trans }} ({{ Setup.selectedStartingHandTiles.length }})
				<span v-if="!client && Setup.activateStartingHand">
					<button type="button" class="btn btn-icon" @click="Setup.deselectStartingHandAllTiles()"
						v-tooltip="I18n.trans('c.setup.deselect_all_tiles')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body" v-show="Setup.activateStartingHand">
				<div>
					<h3>{{ 'c.setup.selection_by_feature_type' | trans }}</h3>
					<div class="features">
						<tc-setup-feature-item v-for="item in Setup.features" :key="item.code" :client="client" :item="item" :mode="'startingHand'">
						</tc-setup-feature-item>
					</div>
				</div>
				<div v-for="extensionData in Setup.selection" v-if="extensionData.tiles.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.deselectStartingHandExtensionTiles(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="tiles">
						<tc-setup-tile-item v-for="item in extensionData.startingHand" :key="item.code" :item="item" :client="client">
						</tc-setup-tile-item>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-messages-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-messages-list-setup panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch" v-model="Setup.activateMessages" :disabled="client" />
				</div>
				{{ 'c.setup.choose_messages' | trans }} ({{ Setup.selectedMessageModels.length }})
				<span v-if="!client">
					<button type="button" class="btn btn-icon" @click="Setup.selectAllMessages()"
						v-tooltip="I18n.trans('c.setup.select_all_messages')">
						<img src="images/icons/select-all.svg" class="icon" alt="" />
					</button>
					<button type="button" class="btn btn-icon" @click="Setup.deselectAllMessages()"
						v-tooltip="I18n.trans('c.setup.deselect_all_messages')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body" v-show="Setup.activateMessages">
				<div v-for="extensionData in Setup.selection" v-if="extensionData.messages.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.selectExtensionMessages(extensionData)"
								v-tooltip="I18n.trans('c.setup.select_all_this_extension')">
								<img src="images/icons/select-all.svg" class="icon" alt="" />
							</button>
							<button type="button" class="btn btn-icon" @click="Setup.deselectExtensionMessages(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="messages">
						<div class="item" v-for="item in extensionData.messages" v-show="item.enabled || !Preferences.setupHideDisabledTiles"
							:class="{ selected: item.selected }">
							<tc-message-thumbnail :message="item.message"></tc-message-thumbnail>
							<label class="item-selector text-muted">
								<input type="checkbox" class="checkbox" v-model="item.selected" :disabled="!item.enabled || client" />
								{{ item.message.code }}
							</label>
							<input class="selected-quantity" type="number" v-model.number="item.quantity" min="1" max="99" step="1" size="2"
								:disabled="!item.selected || client" />
						</div>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-board-events-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-board-events-list-setup panel">
			<h2 class="panel-header">
				<div class="switch-container fr">
					<input type="checkbox" class="switch" v-model="Setup.activateBoardEvents" :disabled="client" />
				</div>
				{{ 'c.setup.choose_board_events' | trans }} ({{ Setup.selectedBoardEventModels.length }})
				<span v-if="!client">
					<button type="button" class="btn btn-icon" @click="Setup.selectAllBoardEvents()"
						v-tooltip="I18n.trans('c.setup.select_all_board_events')">
						<img src="images/icons/select-all.svg" class="icon" alt="" />
					</button>
					<button type="button" class="btn btn-icon" @click="Setup.deselectAllBoardEvents()"
						v-tooltip="I18n.trans('c.setup.deselect_all_board_events')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body" v-show="Setup.activateBoardEvents">
				<div v-for="extensionData in Setup.selection" v-if="extensionData.boardEvents.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.selectExtensionBoardEvents(extensionData)"
								v-tooltip="I18n.trans('c.setup.select_all_this_extension')">
								<img src="images/icons/select-all.svg" class="icon" alt="" />
							</button>
							<button type="button" class="btn btn-icon" @click="Setup.deselectExtensionBoardEvents(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="board-events">
						<div class="item" v-for="item in extensionData.boardEvents" v-show="item.enabled || !Preferences.setupHideDisabledTiles"
							:class="{ selected: item.selected }">
							<tc-board-event-thumbnail :event="item.event"></tc-board-event-thumbnail>
							<label class="item-selector text-muted">
								<input type="checkbox" class="checkbox" v-model="item.selected" :disabled="!item.enabled || client" />
								{{ item.event.code }}
							</label>
							<input class="selected-quantity" type="number" v-model.number="item.quantity" min="1" max="99" step="1" size="2"
								:disabled="!item.selected || client" />
						</div>
					</div>
				</div>
			</div>
		</div>`
});