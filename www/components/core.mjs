/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Preferences } from "../services/preferences.mjs";
import { I18n, Alerts } from '../services/core.mjs';

let Vue = window.Vue;

Vue.component('modal', {
	props: ['title', 'subtitle', 'modalStyle'],
	mounted: function() {
		this.$refs.modal.focus();
	},
	template: `<transition name="modal">
			<div class="modal-mask" @click.stop="$emit(\'close\')">
				<div class="modal-wrapper">
					<div class="modal-container" :style="modalStyle" @click.stop="" @keyup.esc="$emit(\'close\')" tabindex="0" ref="modal">
						<div class="modal-header">
							<button class="btn" type="button" @click="$emit(\'close\')">
								<img src="images/icons/close.svg" class="icon" alt="" />
							</button>
							<h3>{{ title }} <small class="text-muted" v-if="subtitle">{{ subtitle }}</small></h3>
						</div>
						<div class="modal-body"><slot name="body">default body</slot></div>
					</div>
				</div>
			</div>
		</transition>`
});

Vue.component('confirm', {
	props: ['title', 'okLabel', 'koLabel'],
	template: `<transition name="modal">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<div class="modal-header">{{ title }} <button class="btn" type="button" @click="$emit(\'close\')"></button></div>
						<div class="modal-body">
							<slot name="body">
								default body
							</slot>
						</div>
						<div class="modal-footer">
							<slot name="footer">
								<button class="btn modal-ko-button" @click="$emit(\'close\')">{{ okLabel }}</button>
								<button class="btn modal-ok-button" @click="$emit(\'close\')">{{ koLabel }}</button>
							</slot>
						</div>
					</div>
				</div>
			</div>
		</transition>`
});

Vue.component('tc-header-preferences', {
	data() {
		return {
			Preferences: Preferences.getInstance(),
			I18n: I18n
		};
	},
	template: `<div id="tc-header-preferences">
			<div class="header-item header-item-clickable">
				<img src="images/icons/language.svg" class="icon clickable" alt="'c.main.language' | trans"
				v-tooltip="I18n.trans('c.main.language_button_tooltip')" @click="I18n.switchLCID()" />
			</div>
			<div class="header-item header-item-clickable">
				<img v-if="Preferences.playSounds" src="images/icons/volume-up.svg" class="icon clickable" :alt="'c.main.sounds_enabled' | trans"
					v-tooltip="I18n.trans('c.main.disable_sounds')" @click="Preferences.playSounds = false" />
				<img v-if="!Preferences.playSounds" src="images/icons/volume-mute.svg" class="icon clickable" :alt="'c.main.sounds_disabled' | trans"
					v-tooltip="I18n.trans('c.main.enable_sounds')" @click="Preferences.playSounds = true" />
			</div>
		</div>`
});

Vue.component('tc-current-time', {
	data() {
		setInterval(() => {
			let date = new Date();
			this.currentTime = ('' + date.getHours()).padStart(2, '0') + ':' + ('' + date.getMinutes()).padStart(2, '0') + ':' +
				('' + date.getSeconds()).padStart(2, '0');
		}, 1000);
		return {
			currentTime: ''
		}
	},
	template: `<div id="tc-current-time">
			<div class="header-item"><img src="images/icons/clock.svg" class="icon" alt="" /> {{ currentTime }}</div>
		</div>`
});

Vue.component('tc-alert-toaster', {
	data() {
		return {
			Alerts: Alerts,
			I18n: I18n
		}
	},
	template: `<div id="tc-alert-toaster">
			<transition-group name="toaster-items" tag="div">
				<div class="toaster-item alert" v-for="item in Alerts.items" :key="item.id" :class="'alert--' + item.level">
					<img v-if="item.icon" :src="item.icon" class="toaster-item-icon" alt="" />
					<div class="toaster-item-content">
						{{ item.message | trans(['ucf'], item.substitutions) }}
					</div>
					<img src="images/icons/close.svg" class="toaster-item-close icon clickable" @click="Alerts.remove(item)" alt="x"
						v-tooltip="I18n.trans('c.main.remove_alert')" />
				</div>
			</transition-group>
		</div>`
});