/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n } from '../services/core.mjs';
import {Models} from '../services/models.mjs';

let Vue = window.Vue;

Vue.component('tc-models-infos-modals', {
	data: function() {
		return {
			Details: Models.Details
		}
	},
	template: `<div class="tc-models-infos-modals">
			<modal :title="'c.main.tile_details' | trans" :subtitle="Details.tile.code" modal-style="width: 710px;"
				v-if="Details.tile" @close="Details.tile = null">
				<div slot="body">
					<tc-tile-description :tile="Details.tile"></tc-tile-description>
				</div>
			</modal>
			<modal :title="'c.main.message_details' | trans" :subtitle="Details.message.code" modal-style="width: 710px;"
				v-if="Details.message" @close="Details.message = null">
				<div slot="body">
					<tc-message-description :message="Details.message"></tc-message-description>
				</div>
			</modal>
			<modal :title="'c.main.board_event_details' | trans" :subtitle="Details.boardEvent.code" modal-style="width: 710px;"
				v-if="Details.boardEvent" @close="Details.boardEvent = null">
				<div slot="body">
					<tc-board-event-description :event="Details.boardEvent"></tc-board-event-description>
				</div>
			</modal>
		</div>`
});

Vue.component('tc-tile-thumbnail', {
	props: ['tile', 'showCode', 'showDetails'],
	data: function() {
		return {
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-tile-thumbnail">
			<img :src="tile.src" alt="" @click="Models.Details.tile = tile" v-tooltip="I18n.trans('c.main.click_to_show_tile_details')" />
			<div v-if="showDetails && tile.additionalStart" v-tooltip="I18n.trans('c.main.additional_starting_tile_candidate')" class="secondary-start-symbol has-help">★</div>
			<div v-if="showDetails && tile.start" v-tooltip="I18n.trans('c.main.starting_tile_candidate')" class="start-symbol has-help">★</div>
			<small v-if="showCode || showDetails" class="text-muted">
				{{ tile.code }}
				<small v-if="showDetails && tile.defaultQuantity > 1" v-tooltip="I18n.trans('c.main.default_quantity')" class="has-help">
					x{{ tile.defaultQuantity }}
				</small>
			</small>
		</div>`
});

Vue.component('tc-tile-description', {
	props: ['tile'],
	data: function() {
		return {
			feature: null,
			I18n: I18n
		}
	},
	methods: {
		selectFeature(feature) {
			this.feature = feature;
		},
		isNeighbour(index) {
			return this.feature && ArrayUtils.contains(this.feature[3], index);
		}
	},
	template: `<div class="tc-tile-description flex-horizontal">
			<div class="tile-container" v-if="tile">
				<img :src="tile.src" alt="" />
				<span v-for="(item, index) in tile.featuresData" :style="{left: item[1] + '%', top: item[2] + '%' }"
					:class="['feature', {'current': item === feature, 'neighbour': isNeighbour(index)}]"
					v-tooltip="I18n.trans('c.main.click_to_show_feature_details')" @click="selectFeature(item)"></span>
			</div>
			<div class="feature-container" v-if="feature">
				<h4>{{ 'c.main.selected_feature_details' | trans }}</h4>
				<tc-feature-model-summary :code="feature[0]" deployed="true"></tc-feature-model-summary>
			</div>
		</div>`
});

Vue.component('tc-feature-model-summary', {
	props: ['code', 'model', 'feature', 'deployed'],
	data() {
		return {
			item: this.model || (this.feature ? this.feature.model : Models.features[this.code])
		};
	},
	watch: {
		code() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		model() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		feature() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		}
	},
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.item.src + '" alt="" /></span> ' + this.item.help
		}
	},
	template: `<span class="tc-feature-model-summary">
			<span v-if="!deployed" class="has-help" v-tooltip="tooltip">
				<span class="thumbnail thumbnail-26">
					<img v-if="item.src" :src="item.src" alt="" />
				</span>
				<span class="item-name">{{ item.name | ucfirst }}</span>
				<small class="item-code text-muted">{{ item.code }}</small>
			</span>
			<span v-if="deployed">
				<span class="thumbnail thumbnail-26">
					<img v-if="item.src" :src="item.src" alt="" />
				</span>
				<span class="item-name">{{ item.name | ucfirst }}</span>
				<small class="item-code text-muted">{{ item.code }}</small>
				<div class="help" v-html="item.help"></div>
			</span>
		</span>`
});

Vue.component('tc-feature-model-description', {
	props: ['code', 'model', 'feature'],
	data() {
		return {
			item: this.model || (this.feature ? this.feature.model : Models.features[this.code])
		};
	},
	watch: {
		code() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		model() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		feature() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		}
	},
	template: `<div class="tc-feature-model-description">
			<span class="thumbnail thumbnail-26">
				<img v-if="item.src" :src="item.src" alt="" />
			</span>
			<span class="item-name">{{ item.name | ucfirst }}</span>
			<small class="item-code text-muted">{{ item.code }}</small>
		</div>`
});

Vue.component('tc-pawn-model-summary', {
	props: ['code', 'model', 'pawn'],
	data() {
		return {
			item: this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code])
		};
	},
	watch: {
		code() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		},
		model() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		},
		pawn() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		}
	},
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.item.defaultSrc + '" alt="" /></span> ' + this.item.help
		}
	},
	template: `<span class="tc-pawn-model-summary has-help" v-tooltip="tooltip">
			<span class="pawn pawn-26"><img :src="item.defaultSrc" alt="" /></span>
			<span class="item-name">{{ item.name | ucfirst }}</span>
			<small class="item-code text-muted">{{ item.code }}</small>
		</span>`
});

Vue.component('tc-neutral-pawn-model-summary', {
	props: ['code', 'model', 'pawn'],
	data() {
		return {
			item: this.model || (this.pawn ? this.pawn.model : Models.neutralPawns[this.code])
		};
	},
	watch: {
		code() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.neutralPawns[this.code]);
		},
		model() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.neutralPawns[this.code]);
		},
		pawn() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.neutralPawns[this.code]);
		}
	},
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.item.src + '" alt="" /></span> ' + this.item.help
		}
	},
	template: `<span class="tc-neutral-pawn-model-summary has-help" v-tooltip="tooltip">
			<span class="pawn pawn-26"><img :src="item.src" alt="" /></span>
			<span class="item-name">{{ item.name | ucfirst }}</span>
			<small class="item-code text-muted">{{ item.code }}</small>
		</span>`
});

Vue.component('tc-token-model-summary', {
	props: ['code', 'model'],
	data() {
		return {
			item: this.model || Models.tokens[this.code]
		};
	},
	watch: {
		code() {
			this.item = this.model || Models.tokens[this.code];
		},
		model() {
			this.item = this.model || Models.tokens[this.code];
		}
	},
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.item.src + '" alt="" /></span> ' + this.item.help
		}
	},
	template: `<span class="tc-token-model-summary has-help" v-tooltip="tooltip">
			<span class="token token-26"><img :src="item.src" alt="" /></span>
			<span class="item-name">{{ item.name | ucfirst }}</span>
			<small class="item-code text-muted">{{ item.code }}</small>
		</span>`
});

Vue.component('tc-message-thumbnail', {
	props: ['message', 'showCode', 'showDetails'],
	data: function() {
		return {
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-message-thumbnail">
			<img :src="message.src ? message.src : 'images/elements/tile-back.jpg'" alt="" @click="Models.Details.message = message"
				v-tooltip="I18n.trans('c.main.click_to_show_message_details')" />
			<small v-if="showCode || showDetails" class="text-muted">
				{{ message.code }}
				<small v-if="showDetails && message.defaultQuantity > 1" v-tooltip="I18n.trans('c.main.default_quantity')" class="has-help">
					x{{ message.defaultQuantity }}
				</small>
			</small>
		</div>`
});

Vue.component('tc-message-description', {
	props: ['message'],
	data: function() {
		return {
		}
	},
	template: `<div class="tc-message-description flex-horizontal">
			<div class="message-container" v-if="message">
				<img :src="message.src ? message.src : 'images/elements/tile-back.jpg'" alt="" />
			</div>
			<div class="description-container">
				<h4>{{ 'c.main.message_effect' | trans }}</h4>
				<div class="message-effect" v-html="message.description"></div>
			</div>
		</div>`
});

Vue.component('tc-board-event-thumbnail', {
	props: ['event', 'showCode', 'showDetails'],
	data: function() {
		return {
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-board-event-thumbnail">
			<img :src="event.src ? event.src : 'images/elements/tile-back.jpg'" alt="" @click="Models.Details.boardEvent = event"
				v-tooltip="I18n.trans('c.main.click_to_show_board_event_details')" />
			<small v-if="showCode || showDetails" class="text-muted">
				{{ event.code }}
				<small v-if="showDetails && event.defaultQuantity > 1" v-tooltip="I18n.trans('c.main.default_quantity')" class="has-help">
					x{{ event.defaultQuantity }}
				</small>
			</small>
		</div>`
});

Vue.component('tc-board-event-description', {
	props: ['event'],
	data: function() {
		return {
		}
	},
	template: `<div class="tc-board-event-description flex-horizontal">
			<div class="event-container" v-if="event">
				<img :src="event.src ? event.src : 'images/elements/tile-back.jpg'" alt="" />
			</div>
			<div class="description-container">
				<h4  v-html="event.name"></h4>
				<div class="event-effect" v-html="event.description"></div>
			</div>
		</div>`
});