/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Timeline, I18n, Coordinates, StringUtils, ArrayUtils } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import { Board } from '../services/board.mjs';
import { game, TurnSteps } from '../services/game.mjs';
import { Connection } from '../services/connection.mjs';
import { BoxDirectory } from "../services/directories.mjs";
import { System } from "../services/system.mjs";
import { PlayerEffect } from "../structures/PlayerEffect.mjs";
import { PlayerPawn } from "../services/pawns.mjs";

let Vue = window.Vue;

Vue.component('tc-choose-pawn-modal', {
	data() {
		let player = game.modals.choosePawn.player;
		return {
			player: player,
			feature: game.modals.choosePawn.feature,
			pawns: game.modals.choosePawn.pawns,
			/**
			 * @param {PlayerPawn} pawn
			 */
			playPawn(pawn) {
				game.playPawn(game.modals.choosePawn.feature, game.modals.choosePawn.box, player.popPawn(pawn.code), player);
				this.close();
			},
			close() {
				game.modals.choosePawn = null;
				document.dispatchEvent(new CustomEvent('modalChoosePawnClosed', { detail: null }));
			}
		}
	},
	template: `<modal :title="'c.main.which_pawn_to_place' | trans" @close="close()" class="tc-choose-pawn-modal">
			<div slot="body">
				<span v-for="pawn in pawns">
					<button class="btn" type="button" @click="playPawn(pawn)"><tc-pawn :pawn="pawn" size="26"></tc-pawn></button>
				</span>
			</div>
		</modal>`
});

Vue.component('tc-current-tile', {
	data() {
		return {
			Connection: Connection,
			game: game,
			showTileDetails() {
				Models.Details.tile = game.currentTile.model;
			}
		};
	},
	computed: {
		tile() {
			return game.currentTile
		},
		tooltips() {
			if (game.currentTile) {
				return {
					rotateLeft: I18n.trans('c.main.rotate_left'),
					rotateRight: I18n.trans('c.main.rotate_right'),
					showDetails: I18n.trans('c.main.tile_details'),
					redrawTile: I18n.trans('c.main.redraw_tile'),
				};
			}
			return {};
		}
	},
	template: `<div class="tc-current-tile aside">
			<div class="aside-title">{{ 'c.main.current_tile' | trans }}</div>
			<div class="aside-content">
				<div class="actions">
					<button type="button" class="btn" @click="tile.rotateLeft()" v-tooltip="tooltips.rotateLeft" :disabled="!tile">
						<img src="images/icons/tile-rotate-left.svg" class="icon icon-lg" alt="" />
					</button>
					<button type="button" class="btn" @click="tile.rotateRight()" v-tooltip="tooltips.rotateRight" :disabled="!tile">
						<img src="images/icons/tile-rotate-left.svg" class="icon icon-lg flip-horizontal" alt="" />
					</button>
					<button type="button" class="btn" @click="showTileDetails()" v-tooltip="tooltips.showDetails" :disabled="!tile">
						<img src="images/icons/info.svg" class="icon icon-lg" alt="" />
					</button>
					
					<button type="button" class="btn--inverse action-redraw" @click="game.redraw()" v-tooltip="tooltips.redrawTile"
						:disabled="!tile || (Connection.id && Connection.id !== game.currentPlayer.peerId)">
						<img src="images/icons/redraw.svg" class="icon icon-lg" alt="" />
					</button>
				</div>
				<img v-if="tile" :src="tile.src" :alt="tile.code" :class="'tile rotate-' + tile.rotation" @click="showTileDetails()" class="clickable" />
				<img v-if="!tile" src="images/elements/tile-back.jpg" alt="" :class="'tile'" />
			</div>
		</div>`
});

Vue.component('tc-statistics', {
	data() {
		return {
			game: game,
			I18n: I18n,
			OptionalRules: game.optionalRules
		};
	},
	template: `<div class="tc-statistics aside">
			<div class="aside-title">{{ 'c.main.game_data' | trans }}</div>
			<div class="aside-content">
				<ul>
					<li v-if="game.tilePool" v-tooltip="I18n.trans('c.main.remaining_tiles')">
						<div>
							<img src="images/icons/tile-pool.svg" class="icon icon-xl" alt="" />
						</div>
						<div>
							<router-link v-if="OptionalRules.allowPoolDetailView" to="/pool" class="btn">{{ game.tilePool.tiles.length }}</router-link>
							<span v-if="!OptionalRules.allowPoolDetailView">{{ game.tilePool.tiles.length }}</span>
						</div>
					</li>
					<li v-tooltip="I18n.trans('c.main.turn_number')">
						<div>
							<img src="images/icons/playing-turn.svg" class="icon icon-xl" alt="" />
						</div>
						<div>
							{{ game.tableTurnNumber }}
						</div>
					</li>
					<li v-if="game.messagePool" v-tooltip="I18n.trans('c.main.message_period', [], { period: game.messagePool.period })">
						<div>
							<img src="images/icons/message.svg" class="icon icon-xl" alt="" />
						</div>
						<div>
							{{ game.messagePool.period }}
						</div>
					</li>
					<li v-if="game.boardEventPool && game.boardEventPool.period"
						v-tooltip="I18n.trans('c.main.remaining_turns_before_board_event', [], { remaining: game.remainingTurnsBeforeBoardEvent, period: game.boardEventPool.period })">
						<div>
							<img src="images/icons/countdown.svg" class="icon icon-xl" alt="" />
						</div>
						<div>
							{{ game.remainingTurnsBeforeBoardEvent }}
						</div>
					</li>
				</ul>
			</div>
		</div>`
});

Vue.component('tc-players', {
	data() {
		return {
			Connection: Connection,
			game: game
		};
	},
	template: `<div class="tc-players aside">
			<div class="aside-title">{{ 'c.main.players' | trans }}</div>
			<div class="aside-content">
				<ul>
					<li v-for="(player, index) in game.players" :class="{ current: index === game.currentPlayerIndex, self: Connection.id && Connection.id === player.peerId }">
						<tc-player :player="player" :index="index"></tc-player>
					</li>
				</ul>
			</div>
		</div>`
});

Vue.component('tc-player', {
	props: ['player', 'index'],
	data() {
		return {
			game: game,
			Models: Models,
			I18n: I18n,
			showTileDetails(tile) {
				Models.Details.tile = tile.model;
			}
		};
	},
	computed: {
		tokens() {
			let tokens = [];
			Models.tokensArray.forEach((token) => {
				let quantity = this.player.getTokenCount(token.code);
				if (quantity) {
					tokens.push({ model: token, quantity: quantity });
				}
			});
			return tokens;
		}
	},
	template: `<div class="tc-player">
			<span class="bullet" :style="{background: player.color.cssCode, border: '1px solid ' + player.color.cssSecondary }"></span>
			<span class="name">{{ player.name }}</span>
			<span class="text-muted">–</span>
			{{ player.points }} <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />
			<strong v-if="index === game.currentPlayerIndex" class="current-marker">
				[{{ 'c.main.active_player' | trans }}<span v-if="game.hasExtraTurn" class="tag" v-tooltip="I18n.trans('c.main.has_double_turn_tooltip')">x2</span>]
			</strong>
			<div class="player-pawns">
				<span v-for="model in Models.pawnsArray" v-if="player.pawns[model.code] && player.pawns[model.code].length">
					<span class="no-wrap">
						<tc-pawn :pawn="player.pawns[model.code][0]" size="26"></tc-pawn>
						<span v-if="player.pawns[model.code].length > 1" class="pawn-count">×{{ player.pawns[model.code].length }}</span>
					</span>
				</span>
			</div>
			<div class="player-tokens">
				<span v-for="token in tokens">
					<span class="no-wrap">
						<tc-token :model="token.model" size="26"></tc-token>
						<span v-if="token.quantity" class="token-count">×{{ token.quantity }}</span>
					</span>
				</span>
			</div>
			<div class="player-tiles">
				<span v-for="tile in player.tiles">
					<img :src="tile.src" :alt="tile.code" class="tile clickable" @click="showTileDetails(tile)" v-tooltip="tile.tooltip" />
				</span>
			</div>
		</div>`
});

Vue.component('tc-timeline', {
	data() {
		return {
			Timeline: Timeline,
			I18n: I18n,
			Preferences: game.preferences,
			showFullHistory() {
				game.modals.timeline = true;
			}
		};
	},
	template: `<div class="tc-timeline aside">
			<div class="aside-title">
				{{ 'c.main.history' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideTimeline" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideTimeline = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideTimeline" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideTimeline = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideTimeline ? 'collapsed': ''">
				<ul>
					<li v-for="item in Timeline.items.slice(0, 15)">
						<tc-timeline-item :item="item"></tc-timeline-item>
					</li>
				</ul>
				<p class="u-txt-center">
					<button v-if="Timeline.items.length > 15" type="button" class="btn" @click="showFullHistory()">
						{{ 'c.main.view_full_history' | trans }}
					</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-timeline-item', {
	props: ['item'],
	template: `<div class="tc-timeline-item" :class="['category-' + item.category]">
			<span class="time">{{ item.date | time }}</span>
			<span class="message" @click="item.onClick()" :class="[item.clickable ? 'clickable' : '']"
				v-tooltip="item.tooltip">
				{{ item.message | trans(['ucf'], item.substitutions) }}
			</span>
			<span class="message-trigger" v-if="item.trigger" @click="item.trigger.onClick()" :class="[item.trigger.clickable ? 'clickable' : '']"
				v-tooltip="item.trigger.tooltip">
				({{ item.trigger.label }})
			</span>
		</div>`
});

Vue.component('tc-timeline-modal', {
	data() {
		return {
			items: Timeline.items,
			close() {
				game.modals.timeline = false;
			}
		}
	},
	template: `<modal :title="'c.main.history' | trans" @close="close()" class="tc-timeline-modal">
			<div slot="body">
				<ul>
					<li v-for="item in items">
						<tc-timeline-item :item="item"></tc-timeline-item>
					</li>
				</ul>
			</div>
		</modal>`
});

Vue.component('tc-ranking', {
	data() {
		return {
			I18n: I18n,
			Preferences: game.preferences,
			game: game
		};
	},
	computed: {
		ranking() {
			let ranking = [];
			game.players.forEach((player) => { ranking.push({ player: player, score: player.points, rank: null }); });
			ranking.sort((a, b) => { return b.score - a.score });

			// Calculate ranks.
			let lastScore = null;
			let currentRank = 0;
			let tie = 1;
			ranking.forEach((item) => {
				if (item.score !== lastScore) {
					currentRank += tie;
					lastScore = item.score;
					tie = 1;
				}
				else {
					tie++;
				}
				item.rank = currentRank;
			})
			return ranking;
		}
	},
	template: `<div class="tc-ranking aside" v-if="game.ended">
			<div class="aside-title">
				{{ 'c.main.ranking' | trans }}
			</div>
			<div class="aside-content">
				<ul>
					<li v-for="item in ranking">
						<span class="player-container">
							<span class="bullet" :style="{background: item.player.color.cssCode, border: '1px solid ' + item.player.color.cssSecondary }"></span>
							<span class="name">{{ item.player.name }}</span>
							<span class="text-muted">–</span>
							{{ item.score }} <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />
						</span>
						<span class="rank-container" :class="'rank-level-' + item.rank">
							<span class="rank">
								<span class="rank-content">{{ item.rank }}</span>
							</span>
						</span>
					</li>
				</ul>
			</div>
		</div>`
});

Vue.component('tc-game-admin', {
	props: ['client', 'server'],
	data() {
		return {
			game: game,
			Preferences: game.preferences,
			I18n: I18n
		};
	},
	template: `<div class="tc-game-admin aside" v-if="!client">
			<div class="aside-title">
				{{ 'c.main.game_admin' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideGameAdmin" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideGameAdmin = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideGameAdmin" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideGameAdmin = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideGameAdmin ? 'collapsed' : ''">
				<p v-if="!server">
					<button type="button" class="btn" @click="game.save()">{{ 'c.main.save_game' | trans }}</button>
				</p>
				<p>
					<button type="button" class="btn" @click="game.forceEnd()" :disabled="game.ended">{{ 'c.main.force_end' | trans }}</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-game-debug', {
	props: ['client', 'server'],
	data() {
		return {
			game: game,
			Preferences: game.preferences,
			System: System,
			I18n: I18n
		};
	},
	template: `<div class="tc-game-debug aside" v-if="client || server">
			<div class="aside-title">
				{{ 'c.main.game_debug' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideGameDebug" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideGameDebug = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideGameDebug" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideGameDebug = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideGameDebug ? 'collapsed' : ''">
				<p v-if="server">
						<button type="button" class="btn" @click="game.dispatch()">{{ 'c.main.broadcast_client_data' | trans }}</button>
				</p>
				<p v-if="client">
					<button type="button" class="btn" @click="System.reload()">{{ 'c.main.reload_page' | trans }}</button>
				</p>
				<p v-if="client || server">
					<button type="button" class="btn" @click="System.ping()">{{ 'c.main.send_ping' | trans }}</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-preferences', {
	data() {
		return {
			Preferences: game.preferences,
			I18n: I18n
		};
	},
	template: `<div class="tc-preferences aside">
			<div class="aside-title">
				{{ 'c.main.preferences' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsidePreferences" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsidePreferences = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsidePreferences" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsidePreferences = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsidePreferences ? 'collapsed' : ''">
				<p>
					{{ 'c.main.zoom' | trans(['lab']) }}
					<button type="button" class="btn" @click="Preferences.zoomLevel = 1" :disabled="Preferences.zoomLevel == 1">x1</button>
					<button type="button" class="btn" @click="Preferences.zoomLevel = 2" :disabled="Preferences.zoomLevel == 2">x2</button>
					<button type="button" class="btn" @click="Preferences.zoomLevel = 4" :disabled="Preferences.zoomLevel == 4">x4</button>
				</p>
				<p>
					<label class="no-margin-bottom">
						<input type="checkbox" class="switch" v-model="Preferences.showCoordinatesOnInaccessible" value="selection" />
						{{ 'c.main.show_coordinates_on_inaccessible' | trans }}
					</label>
				</p>
				<p>
					<label class="no-margin-bottom">
						<input type="checkbox" class="switch" v-model="Preferences.activateTooltipOnTileHover" value="selection" />
						{{ 'c.main.activate_tooltip_on_tile_hover' | trans }}
					</label>
				</p>
				<p>
					<label class="no-margin-bottom">
						<input type="checkbox" class="switch" v-model="Preferences.playSounds" value="selection" />
						{{ 'c.main.play_sounds' | trans }}
					</label>
				</p>
			</div>
		</div>`
});

Vue.component('tc-player-actions', {
	data() {
		document.addEventListener('modalChoosePawnClosed', () => {
			this.$nextTick(() => {
				if (this.$refs.actions) {
					this.$refs.actions.focus();
				}
			});
		});
		return {
			game: game,
			I18n: I18n,
			onCancel() {
				if (!game.isActive()) {
					return;
				}

				if (this.inPlayPawnPhase) {
					game.undoPlayTile();
				}
				else if (this.inSecondPlayPawnPhase || this.inEndMainPhase) {
					game.undoPlayPawn();
				}
				else {
					console.warn('[tc-player-actions] Nothing to cancel!');
				}
			}
		};
	},
	computed: {
		visible() {
			return game.turnStep === TurnSteps.PLAY_FROM_HAND || game.turnStep === TurnSteps.PLAY_TILE || game.turnStep === TurnSteps.PLAY_PAWN
				|| game.turnStep === TurnSteps.END_MAIN_PHASE || game.turnStep === TurnSteps.TRADE_RESOURCES
				|| game.turnStep === TurnSteps.APPLY_MESSAGE;
		},
		active() {
			return game.isActive();
		},
		inPlayFromHandPhase() {
			return game.turnStep === TurnSteps.PLAY_FROM_HAND;
		},
		inPlayTilePhase() {
			return game.turnStep === TurnSteps.PLAY_TILE;
		},
		inPlayPawnPhase() {
			return game.turnStep === TurnSteps.PLAY_PAWN && !(game.lastPlayedPawns.length || game.lastGotBackPawns.length);
		},
		inSecondPlayPawnPhase() {
			return game.turnStep === TurnSteps.PLAY_PAWN && (game.lastPlayedPawns.length || game.lastGotBackPawns.length);
		},
		inEndMainPhase() {
			return game.turnStep === TurnSteps.END_MAIN_PHASE;
		},
		inTradeResourcesPhase() {
			return game.turnStep === TurnSteps.TRADE_RESOURCES;
		},
		inMessagePhase() {
			return game.turnStep === TurnSteps.APPLY_MESSAGE;
		},
		canAddToHand() {
			return game.turnStep === TurnSteps.PLAY_TILE && game.optionalRules.allowAddTileToHand;
		},
		canGetPawnBack() {
			return game.turnStep === TurnSteps.PLAY_PAWN && game.optionalRules.allowToGetBackPawn && game.canPlayMainAction();
		}
	},
	watch: {
		'game.turnStep'() {
			if (this.visible) {
				this.$nextTick(() => {
					if (this.$refs.actions) {
						this.$refs.actions.focus();
					}
				});
			}
		}
	},
	template: `<div class="tc-player-actions" v-if="visible" ref="actions" tabindex="0" @keyup.esc="onCancel()">
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="active && inPlayFromHandPhase">
					<h2>{{ 'c.main.choose_tile_to_play' | trans }}</h2>
					<div class="help">{{ 'c.main.choose_tile_to_play_help' | trans }}</div>
					<div class="button-bar">
						<button type="button" class="btn btn-tile" @click="game.selectHandTile(index)" 
							v-for="(tile, index) in game.currentPlayer.tiles" v-tooltip="I18n.trans('c.main.select_this_tile_from_hand')">
							<img :src="tile.src" class="tile" width="60" height="60" />
						</button>
						<button type="button" class="btn btn-tile" @click="game.draw()" v-tooltip="I18n.trans('c.main.draw_from_pool')">
							<img src="images/elements/tile-back.jpg" class="tile" width="60" height="60" />
						</button>
					</div>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="active && inPlayTilePhase">
					<h2>{{ 'c.main.play_tile' | trans }}</h2>
					<div v-if="!canAddToHand">
						<div class="help">{{ 'c.main.play_tile_help' | trans }}</div>
					</div>
					<div v-if="canAddToHand">
						<div class="help">{{ 'c.main.play_tile_or_add_to_hand_help' | trans }}</div>
						<div class="button-bar">
							<button type="button" class="btn" @click="game.addCurrentTileToHand()">
								<img src="images/icons/tiles-hand-add.svg" class="icon" alt="" />
								{{ 'c.main.add_to_hand' | trans }}
							</button>
						</div>
					</div>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="active && inPlayPawnPhase">
					<h2>{{ 'c.main.first_placement' | trans }}</h2>
					<div v-if="canGetPawnBack" class="help">{{ 'c.main.play_or_get_back_pawn_help' | trans }}</div>
					<div v-if="!canGetPawnBack" class="help">{{ 'c.main.play_pawn_help' | trans }}</div>
					<div class="button-bar">
						<button type="button" class="btn" @click="game.undoPlayTile()">
							<img src="images/icons/cancel.svg" class="icon" alt="" />
							{{ 'c.main.cancel' | trans }}
						</button>
						<button type="button" class="btn" @click="game.endMainPhase()">
							<img src="images/icons/confirm.svg" class="icon" alt="" />
							{{ 'c.main.do_nothing' | trans }}
						</button>
					</div>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="active && inSecondPlayPawnPhase">
					<h2>{{ 'c.main.second_placement' | trans }}</h2>
					<div v-if="canGetPawnBack" class="help">{{ 'c.main.play_or_get_back_second_pawn_help' | trans }}</div>
					<div v-if="!canGetPawnBack" class="help">{{ 'c.main.play_pawn_second_help' | trans }}</div>
					<div class="button-bar">
						<button type="button" class="btn" @click="game.undoPlayPawn()">
							<img src="images/icons/cancel.svg" class="icon" alt="" />
							{{ 'c.main.cancel' | trans }}
						</button>
						<button type="button" class="btn" @click="game.endMainPhase()">
							<img src="images/icons/confirm.svg" class="icon" alt="" />
							{{ 'c.main.do_nothing' | trans }}
						</button>
					</div>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="active && inEndMainPhase">
					<h2>{{ 'c.main.confirm_placement' | trans }}</h2>
					<div class="button-bar">
						<button type="button" class="btn" @click="game.undoPlayPawn()">
							<img src="images/icons/cancel.svg" class="icon" alt="" />
							{{ 'c.main.cancel' | trans }}
						</button>
						<button type="button" class="btn btn--success" @click="game.endMainPhase()">
							<img src="images/icons/confirm.svg" class="icon" alt="" />
							{{ 'c.main.confirm' | trans }}
						</button>
					</div>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="inTradeResourcesPhase">
					<tc-player-action-trade-resources :readonly="!active"></tc-player-action-trade-resources>
				</div>
			</transition>
			<transition name="fadePositionTop" mode="out-in">
				<div v-if="inMessagePhase">
					<tc-player-action-message :readonly="!active"></tc-player-action-message>
				</div>
			</transition>
		</div>`
});

Vue.component('tc-player-action-trade-resources', {
	props: ['readonly'],
	data() {
		return {
			game: game,
			I18n: I18n,
			totalTrades: game.currentTradeOffers.length,
			selected: null,
			selectedFromResources: [],
			selectedFromTiles: [],
			acceptResourceTrade() {
				/** @type TradeOffer */
				let offer = this.selected;

				let points = 0;
				let gainTokens = [];
				let looseTokens = [];
				let gainPawnCodes = [];
				let loosePawnCodes = [];
				let gainTiles = [];
				let looseTiles = [];

				// Handle "from" part.
				if (offer.from.type === 'points') {
					points -= offer.from.quantity;
				}
				else if (offer.from.type === 'slave') {
					for (let i = 0; i < offer.from.quantity; i++) {
						loosePawnCodes.push('SLA');
					}
				}
				else if (offer.from.type === 'resource') {
					this.selectedFromResources.forEach((token) => {
						looseTokens.push(token.model.code);
					});
				}
				else if (offer.from.type === 'tile') {
					this.selectedFromTiles.forEach((tile) => {
						looseTiles.push(tile);
					});
				}

				// Handle "to" part.
				if (offer.to.type === 'points') {
					points += offer.to.quantity;
				}
				if (offer.to.type === 'slave') {
					for (let i = 0; i < offer.to.quantity; i++) {
						gainPawnCodes.push('SLA');
					}
				}
				else if (offer.to.type === 'resource' && offer.to.resourceType === 'RANDOM') {
					gainTokens = ArrayUtils.merge(gainTokens, game.tokensPool.getRandomTokens(offer.to.quantity, []));
				}
				else if (offer.to.type === 'tile' && offer.to.resourceType === 'RANDOM') {
					for (let i = 0; i < offer.to.quantity; i++) {
						gainTiles.push(game.tilePool.draw());
					}
				}

				// Apply effect.
				let effect = new PlayerEffect(points, false, gainTokens, looseTokens, gainPawnCodes, loosePawnCodes, gainTiles, looseTiles);
				effect.apply(this.game.currentPlayer, this.game, this.game.currentTradeOffers[0].source)

				// Clear selection.
				this.selected = null;
				this.selectedFromResources = [];

				// Go to nex trade offer.
				this.game.nextResourceTradeOffer();
			}
		};
	},
	computed: {
		playerTokens() {
			let tokens = [];
			Models.tokensArray.forEach((token) => {
				let quantity = this.game.currentPlayer.getTokenCount(token.code);
				for (let i = 0; i < quantity; i++) {
					tokens.push({ model: token, key: token.code + i });
				}
			});
			return tokens;
		},
		playerTiles() {
			let tiles = [];
			this.game.currentPlayer.tiles.forEach((tile) => {
				if (game.lastPlayedBox.tile !== tile) {
					tiles.push(tile);
				}
			});
			return tiles;
		},
		isTradeValid() {
			if (!this.selected) {
				return false;
			}

			/** @type TradeOffer */
			let offer = this.selected;

			// Check "from" validity.
			if (offer.from.type === 'points' && this.game.currentPlayer.points < offer.from.quantity) {
				return false;
			}

			if (offer.from.type === 'slave' && this.game.currentPlayer.getPawnsByCode('SLA').length < offer.from.quantity) {
				return false;
			}

			if (offer.from.type === 'resource') {
				if (offer.from.resourceType === 'CHOICE' && this.selectedFromResources.length !== offer.from.quantity) {
					return false;
				}
			}

			if (offer.from.type === 'tile') {
				if (offer.from.resourceType === 'CHOICE' && this.selectedFromTiles.length !== offer.from.quantity) {
					return false;
				}
			}

			// Check "to" validity.

			return true;
		}
	},
	template: `<div class="tc-player-action-trade-resources">
			<h2 v-if="!readonly">
				{{ 'c.main.trade_resources' | trans }} ({{ totalTrades - game.currentTradeOffers.length + 1 }}/{{ totalTrades }})
			</h2>
			<h2 v-if="readonly">
				{{ 'c.main.trade_resources_by_player' | trans(['ucf'], { player: game.currentPlayer.name }) }} ({{ totalTrades - game.currentTradeOffers.length + 1 }}/{{ totalTrades }})
			</h2>
			<div class="flex-container">
				<div class="item-fluid">
					<label v-for="offer in game.currentTradeOffers[0].offers" class="trade-offer">
						<input type="radio" class="radio" name="tradeOffer" v-model="selected" :value="offer" />
						<span class="offer-from">
							<tc-player-action-trade-resources-offer-part :part="offer.from" :disabled="readonly"></tc-player-action-trade-resources-offer-part>
						</span>
						<img src="../images/icons/grey/operator-trade.svg" class="icon" alt="" />
						<span class="offer-to">
							<tc-player-action-trade-resources-offer-part :part="offer.to" :disabled="readonly"></tc-player-action-trade-resources-offer-part>
						</span>
					</label>
				</div>
				<div class="w50p"></div>
				<div class="item-fluid">
					<div class="txtleft" v-if="selected && selected.from.type === 'resource'">
						<span v-for="token in playerTokens" :key="token.key" class="player-token">
							<label>
								<input type="checkbox" class="checkbox" v-model="selectedFromResources" :value="token" :disabled="readonly" />
								<tc-token :model="token.model" size="26"></tc-token>
							</label>
						</span>
					</div>
					<div class="txtleft" v-if="selected && selected.from.type === 'tile'">
						<span v-for="tile in playerTiles" class="player-tiles">
							<label>
								<input type="checkbox" class="checkbox" v-model="selectedFromTiles" :value="tile" :disabled="readonly" />
								<img :src="tile.src" :alt="tile.code" class="tile clickable" v-tooltip="tile.tooltip" />
							</label>
						</span>
					</div>
				</div>
			</div>
			<div class="button-bar">
				<button type="button" class="btn btn--success" @click="acceptResourceTrade()" :disabled="readonly || !isTradeValid">
					<img src="images/icons/confirm.svg" class="icon" alt="" />
					{{ 'c.main.accept_resource_trade' | trans }}
				</button>
				<button type="button" class="btn" @click="game.nextResourceTradeOffer()" :disabled="readonly">
					<img src="images/icons/close.svg" class="icon" alt="" />
					{{ 'c.main.decline_resource_trade' | trans }}
				</button>
			</div>
		</div>`
});

Vue.component('tc-player-action-trade-resources-offer-part', {
	props: ['part', 'readonly'],
	computed: {
		resourceChoiceTooltip() {
			return '<div class="txtcenter"><img src="../images/elements/resource-choice.png" alt="" width="75" height="75" /><br />'
				+ I18n.trans('c.main.resource_choice') + '</div>';
		},
		resourceRandomTooltip() {
			return '<div class="txtcenter"><img src="../images/elements/resource-random.png" alt="" width="75" height="75" /><br />'
				+ I18n.trans('c.main.resource_random') + '</div>';
		},
		tileChoiceTooltip() {
			return '<div class="txtcenter"><img src="../images/elements/tile-choice.png" alt="" width="75" height="75" /><br />'
				+ I18n.trans('c.main.tile_choice') + '</div>';
		},
		tileRandomTooltip() {
			return '<div class="txtcenter"><img src="../images/elements/tile-random.png" alt="" width="75" height="75" /><br />'
				+ I18n.trans('c.main.tile_random') + '</div>';
		},
		slaveTooltip() {
			return '<div class="txtcenter"><img src="../extensions/markets/pawns/slave/orange.svg" alt="" width="75" height="75" /><br />'
				+ I18n.trans('pawn_SLA', ['ucf']) + '</div>';
		}
	},
	template: `<div class="tc-player-action-trade-resources">
			<span v-if="part.type === 'resource' && part.resourceType === 'CHOICE'">
				<span v-for="n in part.quantity" class="token token-26" v-tooltip="resourceChoiceTooltip">
					<img src="../images/elements/resource-choice.png" class="icon-token" :alt="'c.main.resource_choice' | trans" />
				</span>
			</span>
			<span v-if="part.type === 'resource' && part.resourceType === 'RANDOM'">
				<span v-for="n in part.quantity" class="token token-26" v-tooltip="resourceRandomTooltip">
					<img src="../images/elements/resource-random.png" class="icon-token" :alt="'c.main.resource_random' | trans" />
				</span>
			</span>
			<span v-if="part.type === 'tile' && part.resourceType === 'CHOICE'">
				<span v-for="n in part.quantity" class="token token-26" v-tooltip="tileChoiceTooltip">
					<img src="../images/elements/tile-choice.png" class="icon-token" :alt="'c.main.tile_choice' | trans" />
				</span>
			</span>
			<span v-if="part.type === 'tile' && part.resourceType === 'RANDOM'">
				<span v-for="n in part.quantity" class="token token-26" v-tooltip="tileRandomTooltip">
					<img src="../images/elements/tile-random.png" class="icon-token" :alt="'c.main.tile_random' | trans" />
				</span>
			</span>
			<span v-if="part.type === 'slave'">
				<span v-for="n in part.quantity" class="token token-26" v-tooltip="slaveTooltip">
					<img src="../extensions/markets/pawns/slave/orange.svg" class="icon-token" :alt="'pawn_SLA' | trans" />
				</span>
			</span>
			<span v-if="part.type === 'points'">
				<span v-if="part.quantity > 1">{{ part.quantity }}</span>
				<img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />
			</span>
		</div>`
});

Vue.component('tc-player-action-message', {
	props: ['readonly'],
	data() {
		return {
			game: game,
			showEstimation: game.optionalRules.showMessagesEstimation,
			I18n: I18n
		};
	},
	computed: {
		messageEstimation() {
			return game.currentMessage.estimate();
		},
		messageEstimationPoints() {
			let points = this.messageEstimation.points;
			if (this.messageEstimation.extraTurn && game.isExtraTurn) {
				points += 2;
			}
			return (points >= 0 ? '+' : '') + points;
		},
		messageEstimationExtraTurn() {
			return this.messageEstimation.extraTurn && !game.isExtraTurn;
		}
	},
	template: `<div class="tc-player-action-message">
			<h2 v-if="!readonly">{{ 'c.main.message_received' | trans }}</h2>
			<h2 v-if="readonly">
				{{ 'c.main.message_received_by_player' | trans(['ucf'], { player: game.currentPlayer.name }) }}
			</h2>
			<tc-message-description :message="game.currentMessage.model"></tc-message-description>
			<div class="button-bar">
				<button type="button" class="btn" @click="game.applyMessage('accept')" :disabled="readonly">
					{{ 'c.main.accept_message' | trans }}
					<span class="estimated-points" v-if="showEstimation">
						<span v-if="messageEstimationExtraTurn">(<span class="tag" v-tooltip="I18n.trans('c.main.gain_extra_turn')">x2</span>)</span>
						<span v-if="messageEstimationPoints !== '+0' || !messageEstimationExtraTurn">
							({{ messageEstimationPoints }} <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />)
						</span>
					</span>
				</button>
				<button type="button" class="btn" @click="game.applyMessage('refuse')" :disabled="readonly">
					{{ 'c.main.refuse_message' | trans }}
					<span class="estimated-points">
						(+2 <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />)
					</span>
				</button>
			</div>
		</div>`
});

Vue.component('tc-pawn', {
	props: ['pawn', 'feature', 'box', 'size'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		tooltip() {
			let tooltip = '<span class="help-thumbnail"><img src="' + this.pawn.model.getSrc(this.pawn.owner.color.code) + '" alt="" /></span>'
				+ '<strong>' + StringUtils.ucfirst(this.pawn.name) + '</strong><br />' + this.pawn.model.help;
			if (this.feature) {
				tooltip += '<hr /><span class="help-thumbnail"><img src="' + this.feature.model.src + '" alt="" /></span>'
					+ '<strong>' + StringUtils.ucfirst(this.feature.name) + '</strong><br />' + this.feature.model.help;
			}
			return tooltip;
		}
	},
	template: `<span :class="['pawn', this.size ? 'pawn-' + this.size : null, (box && feature && game.canGetBack(pawn)) ? 'pawn-retrievable' : null]"
			v-tooltip="tooltip" @click="(box && feature && game.canGetBack(pawn)) ? game.getBackPawn(feature, box, pawn) : false">
			<img :alt="pawn.name | ucfirst" :src="pawn.src" />
		</span>`
});

Vue.component('tc-neutral-pawn', {
	props: ['pawn', 'feature', 'box', 'size'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		tooltip() {
			let tooltip = '<span class="help-thumbnail"><img src="' + this.pawn.model.src + '" alt="" /></span>'
				+ '<strong>' + StringUtils.ucfirst(this.pawn.name) + '</strong><br />' + this.pawn.model.help;
			if (this.feature) {
				tooltip += '<hr /><span class="help-thumbnail"><img src="' + this.feature.model.src + '" alt="" /></span>'
					+ '<strong>' + StringUtils.ucfirst(this.feature.name) + '</strong><br />' + this.feature.model.help;
			}
			return tooltip;
		}
	},
	template: `<span :class="['neutral-pawn pawn', this.size ? 'pawn-' + this.size : null]" v-tooltip="tooltip">
			<img :alt="pawn.name | ucfirst" :src="pawn.src" />
		</span>`
});

Vue.component('tc-token', {
	props: ['code', 'model', 'feature', 'box', 'size'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		tooltip() {
			let tooltip = '<span class="help-thumbnail"><img src="' + this.token.src + '" alt="" /></span>'
				+ '<strong>' + StringUtils.ucfirst(this.token.name) + '</strong><br />' + this.token.help;
			if (this.feature) {
				tooltip += '<hr /><span class="help-thumbnail"><img src="' + this.feature.model.src + '" alt="" /></span>'
					+ '<strong>' + StringUtils.ucfirst(this.feature.name) + '</strong><br />' + this.feature.model.help;
			}
			return tooltip;
		},
		token() {
			return this.model || Models.tokens[this.code];
		}
	},
	template: `<span :class="['token', this.size ? 'token-' + this.size : null]" v-tooltip="tooltip">
			<img :alt="token.name | ucfirst" :src="token.src" />
		</span>`
});

let onCoordinatesChanged = (component) => {
	component.box = Board.getRealBox(component.x, component.y);
	if (!component.box && Board.isValidCoordinates(component.x, component.y)) {
		component.box = Board.initBox(component.x, component.y);
	}
	component.loopClass = Board.getLoopClass(component.x, component.y);
}

Vue.component('tc-board-box', {
	props: ['x', 'y'],
	data() {
		return {
			Preferences: game.preferences,
			Board: Board,
			box: Board.getRealBox(this.x, this.y),
			loopClass: Board.getLoopClass(this.x, this.y)
		}
	},
	watch: {
		x() {
			onCoordinatesChanged(this);
		},
		y() {
			onCoordinatesChanged(this);
		},
		limitsHash() {
			onCoordinatesChanged(this);
		}
	},
	computed: {
		limitsHash() {
			return Board.limitsHash;
		}
	},
	template: `<td v-if="box" :id="(x === box.x && y === box.y) ? box.elementId : null"
				:class="['tc-board-box', loopClass, box.cssClasses, { highlight: box == Board.highlightedBox }]">
			<div v-show="box.isEmpty && !box.hasPreview && !box.placementSuggestion && (Preferences.showCoordinatesOnInaccessible || box.isAccessible)"
				class="box-coordinates">
				{{  box.coordinates | coordinates }}
			</div>
			<div v-if="!box.isEmpty">
				<img :src="box.tile.src" :class="['tile', 'rotate-' + box.tile.rotation]" />
			</div>
		</td>`
});

Vue.component('tc-board-box-content', {
	props: ['x', 'y'],
	data() {
		return {
			Board: Board,
			I18n: I18n,
			Preferences: game.preferences,
			box: Board.getRealBox(this.x, this.y),
			loopClass: Board.getLoopClass(this.x, this.y),
			previewTooltip() {
				let messages = [];
				if (this.box && this.box.hasPreview) {
					if (this.box.isPreviewValid) {
						if (game.isActive()) {
							if (Connection.id) {
								messages.push(I18n.trans('c.main.play_or_suggest_tile_tooltip'));
							}
							else {
								messages.push(I18n.trans('c.main.play_tile_tooltip'));
							}
						}
						else {
							messages.push(I18n.trans('c.main.suggest_tile_tooltip'));
						}
					}
					messages.push(I18n.trans('c.main.arrows_to_orient_the_tile'));
				}
				return messages.join('<br />');
			},
			focus(event) {
				event.target.focus();
			}
		}
	},
	watch: {
		x() {
			onCoordinatesChanged(this);
		},
		y() {
			onCoordinatesChanged(this);
		},
		limitsHash() {
			onCoordinatesChanged(this);
		}
	},
	computed: {
		tooltip() {
			return this.box.tile ? this.box.tile.tooltip : null;
		},
		limitsHash() {
			return Board.limitsHash;
		}
	},
	template: `<td v-if="box" :class="['tc-board-box', loopClass, box.cssClasses, { highlight: box == Board.highlightedBox }]"
				@mouseenter="box.onMouseEnter()" @mouseleave="box.onMouseLeave()">
			<div v-if="box.isEmpty && !box.hasPreview && box.placementSuggestion" :style="{'box-shadow': box.placementSuggestion.cssColor + ' 0 0 2px 2px' }">
				<div class="box-coordinates">{{  box.coordinates | coordinates }}</div>
				<img :src="box.placementSuggestion.tile.src" :class="['tile', 'rotate-' + box.placementSuggestion.tile.rotation]" />
				<img src="images/icons/suggestion.svg" class="suggestion-icon icon" :style="{'box-shadow': box.placementSuggestion.cssColor + ' 0 0 2px 2px' }" />
			</div>
			<div v-if="box.isEmpty && box.hasPreview">
				<div class="box-coordinates" v-tooltip="previewTooltip()" @click.exact.stop="box.onPlayPreviewTile(false)"
					@click.alt.stop="box.onPlayPreviewTile(true)"
					@keyup.q.stop="box.onPreviewSetOrientation('left')" @keyup.a.stop="box.onPreviewSetOrientation('left')"
					@keyup.d.stop="box.onPreviewSetOrientation('right')"
					@keyup.z.stop="box.onPreviewSetOrientation('up')" @keyup.w.stop="box.onPreviewSetOrientation('up')" 
					@keyup.s.stop="box.onPreviewSetOrientation('down')"
					tabindex="0" @mouseover="focus($event)">{{  box.coordinates | coordinates }}</div>
				<img :src="box.previewTile.src" :class="['tile', 'rotate-' + box.previewTile.rotation]" />
				<img src="images/icons/tile-rotate-left.svg" v-tooltip="I18n.trans('c.main.rotate_left')"
					class="preview-button preview-button-rotate-left icon" @click.stop="box.onPreviewRotateLeftClick()" />
				<img src="images/icons/tile-rotate-left.svg" v-tooltip="I18n.trans('c.main.rotate_right')"
					class="preview-button preview-button-rotate-right icon flip-horizontal" @click.stop="box.onPreviewRotateRightClick()" />
			</div>
			<div v-if="!box.isEmpty">
				<div v-if="Preferences.activateTooltipOnTileHover" v-tooltip="tooltip" class="tile-tooltip-activator" :data-code="box.tile.code"></div>
				<tc-board-box-feature v-for="feature in box.tile.features" v-bind:key="'feat' + feature.id" :feature="feature" :box="box"></tc-board-box-feature>
				<tc-board-box-accessible-feature v-for="feature in box.availableFeatures" v-bind:key="'acce' + feature.id" :feature="feature" :box="box"></tc-board-box-accessible-feature>
				<span v-for="feature in box.occupiedFeatures" class="feature occupied"
					:style="{left: feature.getX(box.tile.rotation) + '%', top: feature.getY(box.tile.rotation) + '%'}">
					<tc-pawn v-for="pawn in feature.pawns" :key="pawn.id" :pawn="pawn" :feature="feature" :box="box"></tc-pawn>
					<tc-neutral-pawn v-for="pawn in feature.neutralPawns" :key="pawn.id" :pawn="pawn" :feature="feature" :box="box"></tc-neutral-pawn>
				</span>
			</div>
		</td>`
});

Vue.component('tc-board-box-feature', {
	props: ['feature', 'box'],
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.feature.model.src + '" alt="" /></span>'
				+ '<strong>' + StringUtils.ucfirst(this.feature.name) + '</strong><br />' + this.feature.model.help;
		},
		style() {
			return { left: this.feature.getX(this.box.tile.rotation) + '%', top: this.feature.getY(this.box.tile.rotation) + '%' };
		}
	},
	template: `<span class="feature help" v-tooltip="tooltip" :style="style"></span>`
});

Vue.component('tc-board-box-accessible-feature', {
	props: ['feature', 'box'],
	computed: {
		tooltip() {
			return '<span class="help-thumbnail"><img src="' + this.feature.model.src + '" alt="" /></span>'
				+ '<strong>' + StringUtils.ucfirst(this.feature.name) + '</strong><br />' + this.feature.model.help;
		},
		style() {
			return { left: this.feature.getX(this.box.tile.rotation) + '%', top: this.feature.getY(this.box.tile.rotation) + '%' };
		}
	},
	template: `<span class="feature accessible" v-tooltip="tooltip" :style="style" @click.stop="box.onPlayPawnOnFeature(feature)""></span>`
});

Vue.component('tc-board', {
	data() {
		return {
			BoxDirectory: BoxDirectory,
			Preferences: game.preferences,
			Board: Board,
			showTopEdge() {
				return Board.verticalType === 'finite-with-edges' && Board.limits.top === Board.limits.maxTop;
			},
			showBottomEdge() {
				return Board.verticalType === 'finite-with-edges' && Board.limits.bottom === Board.limits.maxBottom;
			},
			showLeftEdge() {
				return Board.horizontalType === 'finite-with-edges' && Board.limits.left === Board.limits.maxLeft;
			},
			showRightEdge() {
				return Board.horizontalType === 'finite-with-edges' && Board.limits.right === Board.limits.maxRight;
			}
		};
	},
	template: `<div class="tc-board" :class="'zoom-x' + Preferences.zoomLevel">
			<div class="tc-board-content">
				<div class="spacer"></div><table class="layer-1">
					<tbody>
						<tr v-if="showTopEdge()">
							<td v-if="showLeftEdge()" class="top-edge left-edge"></td>
							<td class="top-edge" v-for="x in Board.columns"></td>
							<td v-if="showRightEdge()" class="top-edge right-edge"></td>
						</tr>
						<tr v-for="y in Board.rows">
							<td v-if="showLeftEdge()" class="left-edge"></td>
							<tc-board-box v-for="x in Board.columns" :x="x" :y="y" :key="x + '_' + y"></tc-board-box>
							<td v-if="showRightEdge()" class="right-edge"></td>
						</tr>
						<tr v-if="showBottomEdge()">
							<td v-if="showLeftEdge()" class="bottom-edge left-edge"></td>
							<td class="bottom-edge" v-for="x in Board.columns"></td>
							<td v-if="showRightEdge()" class="bottom-edge right-edge"></td>
						</tr>
					</tbody>
				</table><div class="spacer"></div>
				<table class="layer-2">
					<tbody>
						<tr v-if="showTopEdge()">
							<td v-if="showLeftEdge()" class="top-edge left-edge"></td>
							<td class="top-edge" v-for="x in Board.columns"></td>
							<td v-if="showRightEdge()" class="top-edge right-edge"></td>
						</tr>
						<tr v-for="y in Board.rows">
							<td v-if="showLeftEdge()" class="left-edge"></td>
							<tc-board-box-content v-for="x in Board.columns" :x="x" :y="y" :key="x + '_' + y"></tc-board-box-content>
							<td v-if="showRightEdge()" class="right-edge"></td>
						</tr>
						<tr v-if="showBottomEdge()">
							<td v-if="showLeftEdge()" class="bottom-edge left-edge"></td>
							<td class="bottom-edge" v-for="x in Board.columns"></td>
							<td v-if="showRightEdge()" class="bottom-edge right-edge"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>`
});

Vue.component('tc-center-on-coordinates', {
	data() {
		return {
			I18n: I18n,
			Coordinates: Coordinates,
			coordinates: { x: null, y: null },
			Board: Board
		}
	},
	template: `<div class="tc-center-on-coordinates">
			<form @submit.prevent="Board.centerOnCoordinates(new Coordinates(coordinates.x, coordinates.y))">
				<div>
					[
					<label v-tooltip="I18n.trans('c.main.x_axis')">
						<input v-model.trim="coordinates.x" type="text" size="3" pattern="-?[a-zA-Z]{1,2}" /> 
					</label>
					;
					<label v-tooltip="I18n.trans('c.main.y_axis')">
						<input v-model.number="coordinates.y" type="number" step="1" size="3" />
					</label>
					]
					<button class="btn btn-icon-sm" type="submit" v-tooltip="I18n.trans('c.main.center_on_these_coordinates')">
						<img src="images/icons/target.svg" alt="'c.main.center_on_these_coordinates' | trans" class="icon-sm" />
					</button>
				</div>
			</form>
		</div>`
});