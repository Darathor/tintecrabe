/**
 * Copyright (C) 2021 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils } from "../services/core.mjs";

export class PlayerEffect {
	/**
	 * @returns {PlayerEffect}
	 */
	static getNewEmpty() {
		return new PlayerEffect(0, false, [], []);
	}

	/**
	 * @param {number} points
	 * @param {boolean} extraTurn
	 * @param {string[]=} gainTokenCodes
	 * @param {string[]=} looseTokenCodes
	 * @param {string[]=} gainPawnCodes
	 * @param {string[]=} loosePawnCodes
	 * @param {Tile[]=} gainTiles
	 * @param {Tile[]=} looseTiles
	 */
	constructor(points, extraTurn, gainTokenCodes, looseTokenCodes, gainPawnCodes, loosePawnCodes, gainTiles, looseTiles) {
		this.points = points;
		this.extraTurn = extraTurn;
		this.gainTokenCodes = gainTokenCodes || [];
		this.looseTokenCodes = looseTokenCodes || [];
		this.gainPawnCodes = gainPawnCodes || [];
		this.loosePawnCodes = loosePawnCodes || [];
		this.gainTiles = gainTiles || [];
		this.looseTiles = looseTiles || [];
	}

	/**
	 * @param {PlayerEffect} effect
	 */
	merge(effect) {
		this.points += effect.points;
		this.gainTokenCodes = ArrayUtils.merge(this.gainTokenCodes, effect.gainTokenCodes);
		this.looseTokenCodes = ArrayUtils.merge(this.looseTokenCodes, effect.looseTokenCodes);
		this.gainPawnCodes = ArrayUtils.merge(this.gainPawnCodes, effect.gainPawnCodes);
		this.loosePawnCodes = ArrayUtils.merge(this.loosePawnCodes, effect.loosePawnCodes);
		this.gainTiles = ArrayUtils.merge(this.gainTiles, effect.gainTiles);
		this.looseTiles = ArrayUtils.merge(this.looseTiles, effect.looseTiles);

		if (effect.extraTurn) {
			if (this.extraTurn) {
				this.points += 2
			}
			else {
				this.extraTurn = true;
			}
		}
	}

	/**
	 * @param {Player} player
	 * @param {Game} game
	 * @param {TimelineItemTrigger} source
	 */
	apply(player, game, source) {
		// Gain points.
		if (this.points !== 0) {
			player.addPoints(this.points, source);
		}

		// Gain tokens.
		if (this.gainTokenCodes && this.gainTokenCodes.length) {
			player.gainTokens(this.gainTokenCodes, source);
		}

		// Loose tokens.
		if (this.looseTokenCodes && this.looseTokenCodes.length) {
			player.looseTokens(this.looseTokenCodes, source);
		}

		// Gain pawns.
		if (this.gainPawnCodes && this.gainPawnCodes.length) {
			player.gainPawns(this.gainPawnCodes, source);
		}

		// Loose pawns.
		if (this.loosePawnCodes && this.loosePawnCodes.length) {
			player.loosePawns(this.loosePawnCodes, source);
		}

		// Gain tiles.
		if (this.gainTiles && this.gainTiles.length) {
			this.gainTiles.forEach((tile) => {
				player.gainTile(tile, source);
			});
		}

		// Loose tiles.
		if (this.looseTiles && this.looseTiles.length) {
			this.looseTiles.forEach((tile) => {
				player.looseTile(tile, source);
				game.tilePool.addTile(tile.model);
			});
		}

		// Extra turn handling.
		if (this.extraTurn) {
			document.dispatchEvent(new CustomEvent('giveExtraTurn', { detail: { source: source } }));
		}
	}
}