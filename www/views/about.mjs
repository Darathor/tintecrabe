/**
 * Copyright (C) 2021 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from '../services/core.mjs';

export default {
	data() {
		return {
			I18n: I18n
		}
	},
	computed: {
		LCID: () => {
			return I18n.LCID;
		}
	},
	template: `<div id="view-about">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.about_tintecrabe' | trans }}</h1>
				</div>
				<div class="view-body" v-if="LCID === 'fr_FR'">
					<div class="panel">
						<h2 class="panel-header txtcenter">Crédits</h2>
						<div class="panel-body">
							<p>Tintecrabe est librement inspiré de l'excellent jeu de plateau <a href="https://fr.wikipedia.org/wiki/Carcassonne_(jeu)" hreflang="fr" class="external">Carcassonne</a> de Klaus-Jürgen Wrede, ainsi que certaines de ses extensions, officielles ou non.</p>
							<p>Ce jeu est réalisé par <a href="https://blog.darathor.net/" hreflang="fr" class="external">Darathor</a> et partagé sous licence <a href="http://mozilla.org/MPL/2.0/" hreflang="en" class="external">MPL</a> pour le code source et <a href="https://creativecommons.org/licenses/by/4.0/legalcode.fr" hreflang="fr" class="external">CC-BY</a> pour les éléments graphiques, à l'exception des éléments suivants.</p>
							<p>Le code repose sur les frameworks <a href="https://fr.vuejs.org/" hreflang="fr" class="external">VueJS 2</a> et <a href="https://www.knacss.com/doc-old.html" hreflang="fr" class="external">KNACSS 7</a>, ainsi que les bibliothèques <a href="https://peerjs.com/" hreflang="en" class="external">PeerJS</a> et <a href="https://vuejsprojects.com/v-tooltip" hreflang="en" class="external">v-tooltip</a>.</p>
							<p>Les textures d'arrière-plan de pierre et bois sont dérivées de textures réalisées par <a href="https://www.artstation.com/skripka" hreflang="en" class="external">Helen Skripka</a>.</p>
							<p>
								Les icônes suivantes proviennent du pack <a href="https://fontawesome.com/" hreflang="en" class="external">Font Awesome</a> (parfois modifiées) : 
								<img src="images/icons/add.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-down.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-left.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-right.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-up.svg" class="icon" alt="" />,
								<img src="images/icons/clock.svg" class="icon" alt="" />,
								<img src="images/icons/close.svg" class="icon" alt="" />,
								<img src="images/icons/copy.svg" class="icon" alt="" />,
								<img src="images/icons/delete.svg" class="icon" alt="" />,
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />,
								<img src="images/icons/edit.svg" class="icon" alt="" />,
								<img src="images/icons/info.svg" class="icon" alt="" />,
								<img src="images/icons/language.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-left-click.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-right-click.svg" class="icon" alt="" />,
								<img src="images/icons/redraw.svg" class="icon" alt="" />,
								<img src="images/icons/save.svg" class="icon" alt="" />,
								<img src="images/icons/select-all.svg" class="icon" alt="" />,
								<img src="images/icons/suggestion.svg" class="icon" alt="" />,
								<img src="images/icons/target.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-down.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-left.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-right.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-up.svg" class="icon" alt="" />,
								<img src="images/icons/volume-mute.svg" class="icon" alt="" /> et 
								<img src="images/icons/volume-up.svg" class="icon" alt="" />.
							</p>
							<p>
								Les différents sons proviennent de <a href="https://lasonotheque.org/" hreflang="fr" class="external">LaSonotheque.fr</a> :
								<a href="https://lasonotheque.org/detail-0283-chant-du-coq.html" hreflang="fr" class="external">début de partie</a> (par DenisChardonnet),
								<a href="https://lasonotheque.org/detail-0292-clochette-1.html" hreflang="fr" class="external">début de tour</a> (par GlaneurDeSons),
								<a href="https://lasonotheque.org/detail-0813-applaudissements-25-50-pers-2.html" hreflang="fr" class="external">fin de partie</a> (par Joseph SARDIN),
								<a href="https://lasonotheque.org/detail-1588-marteau-de-president-1-coup.html" hreflang="fr" class="external">nouvelle alerte</a> (par Joseph SARDIN) et
								<a href="https://lasonotheque.org/detail-2893-envol-oiseau-3.html" hreflang="fr" class="external">message reçu</a> (par Joseph SARDIN).
							</p>
							</ul>
							<p class="no-margin-bottom">Enfin, je tiens à remercier tout particulièrement la pandémie de Covid-19 pour la période de chômage partiel qui m'a donné l'occasion de démarrer le développement de ce jeu 🙂</p>
						</div>
					</div>
					<div class="panel">
						<h2 class="panel-header txtcenter">Contribuer</h2>
						<div class="panel-body">
							<p class="no-margin-bottom">Si vous souhaitez contribuer au développement du jeu ou remonter des suggestions ou commentaires, vous pouvez passer par le dépôt <a href="https://framagit.org/Darathor/tintecrabe" class="external">GitLab</a> ou bien me contacter sur <a href="https://mamot.fr/@Darathor" hreflang="fr">Mastodon</a>.</p>
						</div>
					</div>
				</div>
				<div class="view-body" v-if="LCID !== 'fr_FR'">
					<div class="panel">
						<h2 class="panel-header txtcenter">Credits</h2>
						<div class="panel-body">
							<p>Tintecrabe is loosely based on the excellent board game <a href="https://en.wikipedia.org/wiki/Carcassonne_(board_game)" hreflang="en" class="external">Carcassonne</a> by Klaus-Jürgen Wrede, as well as some of its official and unofficial extensions.</p>
							<p>This game is made by <a href="https://blog.darathor.net/" hreflang="fr" class="external">Darathor</a> and shared under the <a href="http://mozilla.org/MPL/2.0/" hreflang="en" class="external">MPL</a> license for the source code and <a href="https://creativecommons.org/licenses/by/4.0/legalcode" hreflang="fr" class="external">CC-BY</a> for the graphics, except for the following elements.</p>
							<p>The code is based on the <a href="https://vuejs.org/" hreflang="en" class="external">VueJS 2</a> and <a href="https://www.knacss.com/doc-old.html" hreflang="fr" class="external">KNACSS 7</a> frameworks, as well as <a href="https://peerjs.com/" hreflang="en" class="external">PeerJS</a> and <a href="https://vuejsprojects.com/v-tooltip" hreflang="en" class="external">v-tooltip</a> libraries.</p>
							<p>The stone and wood background textures are derived from textures made by <a href="https://www.artstation.com/skripka" hreflang="en" class="external">Helen Skripka</a>.</p>
							<p>
								The following icons are from the <a href="https://fontawesome.com/" hreflang="en" class="external">Font Awesome</a> package (sometimes modified): 
								<img src="images/icons/add.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-down.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-left.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-right.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-up.svg" class="icon" alt="" />,
								<img src="images/icons/clock.svg" class="icon" alt="" />,
								<img src="images/icons/close.svg" class="icon" alt="" />,
								<img src="images/icons/copy.svg" class="icon" alt="" />,
								<img src="images/icons/delete.svg" class="icon" alt="" />,
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />,
								<img src="images/icons/edit.svg" class="icon" alt="" />,
								<img src="images/icons/info.svg" class="icon" alt="" />,
								<img src="images/icons/language.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-left-click.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-right-click.svg" class="icon" alt="" />,
								<img src="images/icons/redraw.svg" class="icon" alt="" />,
								<img src="images/icons/save.svg" class="icon" alt="" />,
								<img src="images/icons/select-all.svg" class="icon" alt="" />,
								<img src="images/icons/suggestion.svg" class="icon" alt="" />,
								<img src="images/icons/target.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-down.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-left.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-right.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-up.svg" class="icon" alt="" />,
								<img src="images/icons/volume-mute.svg" class="icon" alt="" /> and
								<img src="images/icons/volume-up.svg" class="icon" alt="" />.
							</p>
							<p>
								The different sounds come from <a href="https://lasonotheque.org/" hreflang="fr" class="external">LaSonotheque.fr</a>:
								<a href="https://lasonotheque.org/detail-0283-chant-du-coq.html" hreflang="fr" class="external">game start</a> (by DenisChardonnet),
								<a href="https://lasonotheque.org/detail-0292-clochette-1.html" hreflang="fr" class="external">turn start</a> (by GlaneurDeSons),
								<a href="https://lasonotheque.org/detail-0813-applaudissements-25-50-pers-2.html" hreflang="fr" class="external">game end</a> (by Joseph SARDIN),
								<a href="https://lasonotheque.org/detail-1588-marteau-de-president-1-coup.html" hreflang="fr" class="external">new alert</a> (by Joseph SARDIN) and
								<a href="https://lasonotheque.org/detail-2893-envol-oiseau-3.html" hreflang="fr" class="external">message received</a> (by Joseph SARDIN).
							</p>
							<p class="no-margin-bottom">I would like to thank especially the Covid-19 pandemic for the period of partial unemployment that gave me the opportunity to start the development of this game 🙂</p>
						</div>
					</div>
					<div class="panel">
						<h2 class="panel-header txtcenter">Contribute</h2>
						<div class="panel-body">
							<p class="no-margin-bottom">If you would like to contribute to the development of the game or bring up suggestions or comments, you can go through the <a href="https://framagit.org/Darathor/tintecrabe" class="external">GitLab</a> repository or contact me on <a href="https://mamot.fr/@Darathor" hreflang="fr">Mastodon</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</div>`
}