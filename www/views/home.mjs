/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { game } from '../services/game.mjs';
import { Connection } from '../services/connection.mjs';

export default {
	data: function() {
		let data = {
			game: game,
			serverId: '',
			newGame() {
				this.$router.push('/setup');
			},
			continueLastGame() {
				game.loadLast();
				this.$router.push('/play');
			},
			createOnlineGame() {
				Connection.startServer().then(() => {
					this.$router.push('/setup');
				});
			},
			joinOnlineGame() {
				Connection.startClient(data.serverId).then(() => {
					this.$router.push('/setup');
				});
			}
		};
		return data;
	},
	template: `<div id="view-home">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.play_tintecrabe' | trans }}</h1>
				</div>
				<div class="panel">
					<h2 class="panel-header">{{ 'c.main.play_locally' | trans }}</h2>
					<div class="panel-body flex-horizontal buttons">
						<button type="button" class="btn" @click="newGame()">{{ 'c.main.new_game' | trans }}</button>
						<button type="button" class="btn" @click="continueLastGame()" :disabled="!game.hasLast()">{{ 'c.main.continue_last_game' | trans }}</button>
					</div>
				</div>
				<div class="panel">
					<h2 class="panel-header">{{ 'c.main.play_online' | trans }}</h2>
					<div class="panel-body flex-horizontal buttons">
						<button type="button" class="btn" @click="createOnlineGame()">{{ 'c.main.new_game' | trans }}</button>
						<div>
							<label>{{ 'c.main.server_identifier' | trans }} <input type="text" v-model="serverId" /></label>
							<button type="button" class="btn" @click="joinOnlineGame()" :disabled="!serverId">{{ 'c.main.join_game' | trans }}</button>
						</div>
					</div>
				</div>
			</div>
		</div>`
}