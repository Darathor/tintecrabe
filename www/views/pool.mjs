/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Models } from '../services/models.mjs';
import { game } from '../services/game.mjs';

export default {
	data() {
		return {
			Models: Models,
			game: game,
			Pool: game.tilePool
		}
	},
	mounted() {
		this.$nextTick(() => {
			// Code that will run only after the entire view has been rendered.
			if (!game.initialized) {
				this.$router.push('/');
			}
			else if (!game.optionalRules.allowPoolDetailView) {
				this.$router.push('/play');
			}
		});
	},
	template: `<div id="view-pool" v-if="game.initialized">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.remaining_tiles' | trans(['lab']) }} {{ Pool.tiles.length }}</h1>
				</div>
				<div class="panel">
					<router-link to="/play" class="btn btn--big">
						<img src="images/icons/toggle-left.svg" alt="" class="icon-sm" />
						{{ 'c.main.back_to_game' | trans }}
					</router-link>
				</div>
				<div class="panel tiles" v-if="Pool.tiles.length">
					<div class="panel-body">
						<tc-tile-thumbnail v-for="tile in Pool.tiles" :key="tile.id" :tile="tile" :showCode="true"></tc-tile-thumbnail>
					</div>
				</div>
			</div>
		</div>`
}