/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import '../services/all-for-game.mjs';
import { I18n } from '../services/core.mjs';
import { ModelsLoader } from '../services/models/loader.mjs';
import { Connection } from '../services/connection.mjs';
import home from '../views/home.mjs';
import about from '../views/about.mjs';
import setup from '../views/setup.mjs';
import play from '../views/play.mjs';
import pool from '../views/pool.mjs';
import extensions from '../views/extensions.mjs';
import reconnect from '../views/reconnect.mjs';
import { game } from "../services/game.mjs";
import { System } from "../services/system.mjs";

let Vue = window.Vue;
let VueRouter = window.VueRouter;

I18n.init('fr_FR', () => {
	ModelsLoader.load(['core', 'animals', 'messengers', 'mountains', 'deserts', 'ramparts', 'castles', 'chappaai', 'markets', 'warehouses', 'graveyards', 'runestones', 'cataclysms'], () => {
		let routes = [
			{ path: '/', component: home },
			{ path: '/about', component: about },
			{ path: '/setup', component: setup },
			{ path: '/play', component: play },
			{ path: '/pool', component: pool },
			{ path: '/extensions', component: extensions },
			{ path: '/reconnect/:serverId/:selfId', component: reconnect }
		];

		let router = new VueRouter({
			routes: routes
		});
		System.router = router;

		let app = new Vue({
			router: router,
			data: {
				initialized: true,
				Connection: Connection,
				I18n: I18n,
				copyServerIdentifierToClipboard() {
					document.getElementById('server_identifier').select();
					document.execCommand('copy');
				}
			}
		});

		app.$mount('#app');

		//region Event listeners.
		document.addEventListener('tcConnectionRemovePlayer', (event) => {
			let player = event.detail.data;
			if (player.peerId === Connection.id) {
				alert(I18n.trans('c.main.excluded_from_game'));
				router.push('/');
				window.location.reload();
			}
		});

		// Confirm before quitting the page when a game is running.
		window.addEventListener('beforeunload', (event) => {
			if (game.initialized && !game.ended) {
				event.returnValue = true;
			}
		});
		//endregion
	});
});