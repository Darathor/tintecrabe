/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { game } from '../services/game.mjs';
import { Connection } from "../services/connection.mjs";
import { Sounds } from "../services/sounds.mjs";
import { I18n } from "../services/core.mjs";

export default {
	data() {
		return {
			game: game,
			server: Connection.isServer(),
			client: Connection.isClient(),
			peerId: Connection.id,
			Preferences: game.preferences,
			I18n: I18n
		}
	},
	mounted() {
		this.$nextTick(() => {
			// Code that will run only after the entire view has been rendered.
			if (!game.initialized) {
				this.$router.push('/');
			}
			else if (game.preferences.playSounds && !game.announced) {
				game.announced = true;
				Sounds.startGame.play();
			}
		});
	},
	watch: {
		'game.currentPlayer': {
			handler: () => {
				if (game.preferences.playSounds && Connection.id && Connection.id === game.currentPlayer.peerId) {
					Sounds.startTurn.play();
				}
			}
		},
		'game.ended': {
			handler: () => {
				if (game.preferences.playSounds && game.ended) {
					Sounds.endGame.play();
				}
			}
		}
	},
	template: `<div id="view-play" class="flex-horizontal" v-if="game.initialized">
			<aside class="sidebar sidebar-left" :class="Preferences.collapseLeftSidebar ? 'collapsed': ''">
				<div class="sidebar-toggle">
					<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseLeftSidebar" v-tooltip="I18n.trans('c.main.collapse')"
						@click="Preferences.collapseLeftSidebar = true">
						<img src="images/icons/toggle-left.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseLeftSidebar" v-tooltip="I18n.trans('c.main.expand')"
						@click="Preferences.collapseLeftSidebar = false">
						<img src="images/icons/toggle-right.svg" alt="" class="icon-sm" />
					</button>
				</div>
				<tc-current-tile></tc-current-tile>
				<tc-statistics></tc-statistics>
				<tc-players></tc-players>
			</aside>
			<div class="board">
				<tc-center-on-coordinates></tc-center-on-coordinates>
				<tc-player-actions></tc-player-actions>
				<tc-board></tc-board>
			</div>
			<aside class="sidebar sidebar-right" :class="Preferences.collapseRightSidebar ? 'collapsed': ''">
				<div class="sidebar-toggle">
					<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseRightSidebar" v-tooltip="I18n.trans('c.main.collapse')"
						@click="Preferences.collapseRightSidebar = true">
						<img src="images/icons/toggle-right.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseRightSidebar" v-tooltip="I18n.trans('c.main.expand')"
						@click="Preferences.collapseRightSidebar = false">
						<img src="images/icons/toggle-left.svg" alt="" class="icon-sm" />
					</button>
				</div>
				<tc-game-admin :client="client" :server="server"></tc-game-admin>
				<tc-preferences></tc-preferences>
				<tc-game-debug :client="client" :server="server"></tc-game-debug>
				<tc-ranking></tc-ranking>
				<tc-timeline></tc-timeline>
			</aside>
			<tc-choose-pawn-modal v-if="game.modals.choosePawn"></tc-choose-pawn-modal>
			<tc-timeline-modal v-if="game.modals.timeline"></tc-timeline-modal>
		</div>`
}


