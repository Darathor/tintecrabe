/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { Models } from '../services/models.mjs';

export default {
	data: function() {
		return {
			Models: Models
		}
	},
	template: `<div id="view-extensions">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.the_extensions' | trans }}</h1>
				</div>
				<div v-for="extension in Models.extensionsArray" class="extension panel">
					<div class="panel-header">
						<div class="extension-symbol-container" v-if="extension.symbolSrc">
							<img :src="extension.symbolSrc" class="extension-symbol" alt="" />
						</div>
						<h2>
							{{ extension.name }} <small class="text-muted">{{ extension.code }}</small>
						</h2>
						<p>{{ extension.description }}</p>
						<p v-if="extension.about">{{ 'c.main.about' | trans(['lab']) }} {{ extension.about }}</p>
					</div>
					<div class="panel-body flex-container">
						<div class="w250p">
							<div v-if="extension.pawns.length">
								<h3>{{ 'c.main.pawns' | trans }} <span class="badge">{{ extension.pawns.length }}</span></h3>
								<ul class="little-items pawns">
									<li class="item" v-for="pawn in extension.pawns">
										<tc-pawn-model-summary :model="pawn"></tc-pawn-model-summary>
									</li>
								</ul>
							</div>
							<div v-if="extension.neutralPawns.length">
								<h3>{{ 'c.main.neutral_pawns' | trans }} <span class="badge">{{ extension.neutralPawns.length }}</span></h3>
								<ul class="little-items pawns">
									<li class="item" v-for="pawn in extension.neutralPawns">
										<tc-neutral-pawn-model-summary :model="pawn"></tc-neutral-pawn-model-summary>
									</li>
								</ul>
							</div>
							<div v-if="extension.tokens.length">
								<h3>{{ 'c.main.tokens' | trans }} <span class="badge">{{ extension.tokens.length }}</span></h3>
								<ul class="little-items tokens">
									<li class="item" v-for="token in extension.tokens">
										<tc-token-model-summary :model="token"></tc-token-model-summary>
									</li>
								</ul>
							</div>
							<div v-if="extension.features.length">
								<h3>{{ 'c.main.elements' | trans }} <span class="badge">{{ extension.features.length }}</span></h3>
								<ul class="little-items features">
									<li class="item" v-for="feature in extension.features">
										<tc-feature-model-summary :model="feature"></tc-feature-model-summary>
									</li>
								</ul>
							</div>
						</div>
						<div class="w5"></div>
						<div class="item-fluid">
							<div v-if="extension.tiles.length"> 
								<h3>{{ 'c.main.tiles' | trans }} <span class="badge">{{ extension.tiles.length }}</span></h3>
								<div class="tiles">
									<div class="items">
										<tc-tile-thumbnail v-for="tile in extension.tiles" :tile="tile" :showDetails="true"
											:key="tile.code"></tc-tile-thumbnail>
									</div>
								</div>
							</div>
							<div v-if="extension.messages.length">
								<h3>{{ 'c.main.messages' | trans }} <span class="badge">{{ extension.messages.length }}</span></h3>
								<div class="messages">
									<div class="items">
										<tc-message-thumbnail v-for="message in extension.messages" :message="message" :showDetails="true"
											:key="message.code"></tc-message-thumbnail>
									</div>
								</div>
							</div>
							<div v-if="extension.boardEvents.length">
								<h3>{{ 'c.main.board_events' | trans }} <span class="badge">{{ extension.boardEvents.length }}</span></h3>
								<div class="board-events">
									<div class="items">
										<tc-board-event-thumbnail v-for="event in extension.boardEvents" :event="event" :showDetails="true"
											:key="event.code"></tc-board-event-thumbnail>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>`
}