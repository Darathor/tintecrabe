/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { debounce } from '../services/core.mjs';
import { Setup } from '../services/setup.mjs';
import { game } from '../services/game.mjs';
import { Connection } from '../services/connection.mjs';
import { Preferences } from "../services/preferences.mjs";

let onDataUpdated = debounce(() => {
	if (Connection.isServer() && Setup.isValid) {
		Connection.sendMessage('SetupData', Setup.toData());
	}
	else if (Connection.isClient() && Setup.isValid) {
		let player = Setup.getPlayerByPeerId(Connection.id);
		if (player) {
			Connection.sendMessage('SetupUpdatePlayer', player.data);
		}
	}
});

export default {
	data() {
		Setup.init(Connection.isClient());
		return {
			Setup: Setup,
			Preferences: Preferences.getInstance(),
			server: Connection.isServer(),
			client: Connection.isClient(),
			peerId: Connection.id,
			start() {
				game.init();
				Connection.sendMessage('StartGame', game.data);
				this.$router.push('/play');
			}
		}
	},
	watch: {
		Setup: {
			deep: true,
			handler: onDataUpdated
		}
	},
	mounted() {
		let recursiveCheckInitialized = () => {
			if (this.$router.currentRoute.path === '/setup') {
				if (game.initialized) {
					this.$router.push('/play');
				}
				else {
					setTimeout(recursiveCheckInitialized, 100);
				}
			}
		};

		// Code that will run only after the entire view has been rendered.
		this.$nextTick(recursiveCheckInitialized);
	},
	template: `<div id="view-setup">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.setup.new_game' | trans }}</h1>
				</div>
				<div class="view-body" :class="'tiles-' + Preferences.setupTileSize">
					<div>
						<tc-setup-setup :client="client"></tc-setup-setup>
						<tc-players-setup :peer-id="peerId" :server="server"></tc-players-setup>
						<tc-pawns-setup :client="client"></tc-pawns-setup>
						<tc-neutral-pawns-setup :client="client"></tc-neutral-pawns-setup>
						<tc-tokens-setup :client="client"></tc-tokens-setup>
						<tc-multiple-start-setup :client="client"></tc-multiple-start-setup>
						<tc-pool-setup :client="client"></tc-pool-setup>
						<tc-message-pool-setup :client="client"></tc-message-pool-setup>
						<tc-board-events-pool-setup :client="client"></tc-board-events-pool-setup>
						<tc-starting-hand-setup :client="client"></tc-starting-hand-setup>
						<tc-board-setup :client="client"></tc-board-setup>
						<tc-optional-rules-setup :client="client"></tc-optional-rules-setup>
					</div>
					<div>
						<tc-first-tile-setup :client="client"></tc-first-tile-setup>
						<tc-additional-start-tiles-setup :client="client"></tc-additional-start-tiles-setup>
						<tc-tiles-list-setup :client="client"></tc-tiles-list-setup>
						<tc-messages-list-setup :client="client"></tc-messages-list-setup>
						<tc-board-events-list-setup :client="client"></tc-board-events-list-setup>
						<tc-starting-hand-list-setup :client="client"></tc-starting-hand-list-setup>
					</div>
				</div>
				<div class="view-footer txtcenter" v-if="!client">
					<button type="button" class="btn btn--big" :disabled="!Setup.isValid" @click="start()">{{ 'c.setup.start_game' | trans }}</button>
				</div>
			</div>
		</div>`
}