/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils, I18n } from '../services/core.mjs';
import { ModelsLoader } from '../services/models/loader.mjs';
import { Models } from '../services/models.mjs';

let Vue = window.Vue;

let Data = {
	ready: false,
	Models: Models,
	errors: [],
	total: {},
	features: {},
	sides: {},
	sideFeatures: {},
	configurations: {},
	configurationsFilter: {
		value: [],
		mode: 'inclusion',
		matches: (configuration) => {
			let result;
			if (Data.configurationsFilter.mode === 'inclusion') {
				result = false;
				Data.configurationsFilter.value.forEach((feature) => {
					if (configuration.features[feature.code]) {
						result = true;
					}
				});
			}
			else {
				result = true;
				Data.configurationsFilter.value.forEach((feature) => {
					if (configuration.features[feature.code]) {
						result = false;
					}
				});
			}
			return result;
		},
		clear: () => {
			ArrayUtils.clear(Data.configurationsFilter.value);
			return true;
		},
		selectAll: () => {
			ArrayUtils.clear(Data.configurationsFilter.value);
			Object.keys(Data.sideFeatures).forEach((code) => {
				Data.configurationsFilter.value.push(Data.sideFeatures[code]);
			});
			return true;
		}
	},
};

new Vue({
	el: '#configurator-view',
	data: Data
});

/**
 * @typedef {Object} FeatureStat
 * @property {FeatureModel} model
 * @property {Extension} extension
 * @property {number} tileCount
 * @property {number} occurrencesInTiles
 * @property {number} messageCount
 */

I18n.init('fr_FR', () => {
	ModelsLoader.load(['core', 'animals', 'messengers', 'mountains', 'deserts', 'ramparts', 'castles', 'chappaai', 'markets', 'warehouses', 'graveyards', 'runestones', 'cataclysms'], () => {
		let errors = [];
		let features = {};
		let sideFeatures = {};
		let featureCodes = [];
		let sides = {};
		let configurations = [];
		let configurationsByCode = {};
		let total = {
			tiles: Models.tilesArray.length,
			featuresOccurrences: 0,
			messages: Models.messagesArray.length
		}

		let sideScore = (side) => {
			let score = featureCodes.indexOf(side[0]);
			score *= featureCodes.length;
			score += featureCodes.indexOf(side[1]);
			score *= featureCodes.length;
			score += featureCodes.indexOf(side[2]);
			return score;
		}

		let positionScore = (sides) => {
			let score = sideScore(sides[0]);
			for (let i = 1; i < 4; i++) {
				score *= featureCodes.length * featureCodes.length * featureCodes.length;
				score += sideScore(sides[i]);
			}
			return score;
		}

		let compareScore = (a, b) => {
			if (a.score > b.score) {
				return 1;
			}
			if (a.score < b.score) {
				return -1;
			}
			return 0;
		};

		let getSideFeature = (code) => {
			if (!sideFeatures[code]) {
				sideFeatures[code] = Models.getFeatureByCode(code);
			}
			return sideFeatures[code];
		};

		Models.extensionsArray.forEach((extension) => {
			extension.features.forEach((feature) => {
				featureCodes.push(feature.code);
				features[feature.code] = {
					model: feature,
					extension: extension,
					tileCount: 0,
					occurrencesInTiles: 0,
					messageCount: 0,
				};
			});
		});
		featureCodes = featureCodes.sort();

		Models.tilesArray.forEach((tile) => {
			// Features in tiles.
			let foundFeatures = {};
			tile.featuresData.forEach((featureData) => {
				let code = featureData[0];
				/** @var {FeatureStat} feature */
				let feature = features[code];
				if (!feature) {
					errors.push({
						message: 'Code feature introuvable : ' + code + ' (tuile ' + tile.code + ')',
						context: {
							code: tile.code,
							model: tile,
							featureData: featureData
						}
					});
					return;
				}

				if (!foundFeatures[code]) {
					foundFeatures[code] = true;
					feature.tileCount++;
				}
				feature.occurrencesInTiles++;
				total.featuresOccurrences++;
			});

			// Tile sides.
			let foundSides = {};
			let tileSides = [];
			tile.sides.forEach((side) => {
				let sideCodes = [];
				side.forEach((featureIndex) => {
					sideCodes.push(tile.featuresData[featureIndex][0]);
				});

				let sideCode = sideCodes.join('/');
				tileSides.push(sideCodes);
				if (!sides[sideCode]) {
					sides[sideCode] = {
						codes: sideCodes,
						models: [getSideFeature(sideCodes[0]), getSideFeature(sideCodes[1]), getSideFeature(sideCodes[2])],
						tileCount: 0,
						occurrencesInTiles: 0
					};
				}
				let sideData = sides[sideCode];

				if (!foundSides[sideCode]) {
					foundSides[sideCode] = true;
					sideData.tileCount++;
				}
				sideData.occurrencesInTiles++;
			});

			// Tile configurations.
			let positions = [];
			for (let p = 0; p < 4; p++) {
				let sides = [];
				let sideCodes = [];
				for (let i = p; i < 4; i++) {
					sides.push(tileSides[i]);
					sideCodes.push(tileSides[i].join('/'));
				}
				for (let i = 0; i < p; i++) {
					sides.push(tileSides[i]);
					sideCodes.push(tileSides[i].join('/'));
				}

				positions.push({
					sides: sides,
					code: sideCodes.join('/'),
					score: positionScore(sides)
				});
			}

			let position = positions.sort(compareScore)[0];

			if (!configurationsByCode[position.code]) {
				let allSideCodes = [];
				let allSideModels = [];
				let features = {};
				position.sides.forEach((side) => {
					let sideCodes = [];
					let sideModels = [];
					side.forEach((code) => {
						sideCodes.push(code);
						sideModels.push(getSideFeature(code));
						if (!features[code]) {
							features[code] = getSideFeature(code);
						}
					});
					allSideCodes.push(sideCodes);
					allSideModels.push(sideModels);
				});

				let configuration = {
					codes: allSideCodes,
					models: allSideModels,
					tiles: [],
					features: features,
					score: position.score
				};
				configurationsByCode[position.code] = configuration;
				configurations.push(configuration);
			}

			configurationsByCode[position.code].tiles.push(tile);
		});

		Models.messagesArray.forEach((message) => {
			// Features in messages.
			message.featureReferences.forEach((code) => {
				/** @var {FeatureStat} feature */
				let feature = features[code];
				if (!feature) {
					errors.push({
						message: 'Code feature introuvable : ' + code + ' (message ' + message.code + ')',
						context: {
							code: message.code,
							model: message
						}
					});
					return;
				}

				feature.messageCount++;
			});
		});

		Data.errors = errors;
		Data.total = total;
		Data.features = features;
		Data.sides = sides;
		Data.sideFeatures = sideFeatures;
		Data.configurations = configurations.sort(compareScore);
		Data.ready = true;

		Data.configurationsFilter.selectAll();
	});
});