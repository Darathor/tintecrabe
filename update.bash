#!/bin/bash
peerjsURL=$1
peerjsPort=$2
websiteRoot=$3

# Update code.
git checkout www/index.html
git pull;

# Inject configuration.
rm www/configuration.mjs
cp templates/configuration.mjs.tpl www/configuration.mjs
sed -i "s#%%WEBSITE_ROOT%%#$websiteRoot#" www/configuration.mjs
sed -i "s#%%PEERJS_HOST%%#$peerjsURL#" www/configuration.mjs
sed -i "s#%%PEERJS_PORT%%#$peerjsPort#" www/configuration.mjs

sed -i "s#%%WEBSITE_ROOT%%#$websiteRoot#" www/index.html
sed -i "s#%%PEERJS_HOST%%#$peerjsURL#" www/index.html
sed -i "s#%%PEERJS_PORT%%#$peerjsPort#" www/index.html
